<html>

    <head>
        <title>
            Admin
        </title>
        <link rel="stylesheet" href="views/css/font-awesome.min.css">
        <link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="views/css/bootstrap.min_1.css">
        <link rel="stylesheet" href="views/css/style.css">
        <script src="views/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="views/js/sweetalert.min.js"></script>
        <link rel="stylesheet" href="views/css/sweetalert.css">
        <script>
            function showLocation() {
                var x = document.getElementById("admin-main");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
            function showKinsView() {
                var x = document.getElementById("admin-view");
                if (x.style.display === "none") {
                    x.style.display = "block";
                }
                else {
                    x.style.display = "none";
                }
            }
        </script>
    </head>

    <body>
        <div class="container pr-0 pl-0">
             <div class="pt-2 main pb-2">
                <div class="admin-main" id="admin-main">
                    <div class="row">
                        <div class="col-lg-12 " style="padding-left: 30px;padding-right: 30px;">
                            <div class="d-flex flex-row mb-4">
                                <div class="col-lg-auto mt-auto " style="vertical-align: middle;">
                                    <input class="btn btn-sm " type="button" id="addAdmin" value="Add User">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table
						class="table table-bordered table-striped table-light table_id mr-0 ml-0"
						id="admin_id"
						style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif;">

                                    <thead id="tabelHead">
                                        <tr>
                                            <th>USERID</th>
                                            <th>USER NAME</th>
                                            <th>LOCN/LEXT</th>
                                            <th>DPID</th>
                                            <th>DPTV</th>
                                            <th>DEPT/DEXT</th>
                                            <th>STAT</th>
                                            <th>REAL</th>
                                            <th>BANK</th>
                                            <th>KINS</th>
                                            <th>ADMIN</th>
                                            <th>DELETE</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="admin-create" id="admin-create">
                <div class="d-flex flex-row">
                    <div class="col-lg-auto mt-auto " style="vertical-align: middle;">
                        <label>
                            <b>USER ID</b>
                        </label>
                    </div>
                    <div class="col-lg-auto">
                        <input type="text" class="form-control form-control-sm" id="admin-user-name" value="">
                        <input type="hidden" class="form-control form-control-sm" id="admin_userid_update" value="">
                    </div>
                </div>
                <div class="row mt-1">
                        <div class="col-lg-12 " style="padding-left: 30px;padding-right: 30px;">
                            
                            <div class="table-responsive">
                                <table
						class="table table-bordered table-striped table-light table_id mr-0 ml-0"
						id="table_id_admincreate"
						style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif;">

                            <thead id="tabelHead">
                                <tr>
                                    <th scope="col">Segment</th>
                                    <th scope="col">LOCN/LEXT</th>
                                    <th scope="col">DPID</th>
                                    <th scope="col">DPTV</th>
                                    <th scope="col">DEPT/DEXT</th>
                                    <th scope="col">STAT</th>
                                    <th scope="col">REAL</th>
                                    <th scope="col">BANK</th>
                                    <th scope="col">KINS</th>
                                    <th scope="col">ADMIN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Access</th>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="locn-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="dpid-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="dptv-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="dept-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="stat-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="real-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="bank-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="kins-access">
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control form-control-sm" style="font-size: 9px;font-family: Arial, Helvetica, sans-serif;vertical-align: middle" id="admin-access">
                                                <option value="YES">YES</option>
                                                <option value="NO">NO</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                <div class="d-flex flex-row justify-content-end mt-1">
                    <div class="col-lg-auto " style="vertical-align: middle;">
                        <input class="btn  btn-sm " type="button" id="add" value="Add">
                        <input class="btn  btn-sm btnCancel" type="button" id="cancel" value="Cancel">
                    </div>
                </div>
            </div>
            
            <div class="admin-update" id="admin-update">
                <div class="d-flex flex-row mt-1">
                    <div class="col-sm-12">
                        <table class="table table-bordered mt-2 m-table hirar-table table-striped"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
                           
                            <tbody>
                                <tr>
                                    <th >User Name</th>
                                    <td>
                                    <label id="admin_userName_update"></label>
                                    </td>
                                    <th scope="row">LOCN/LEXT</th>
                                    <td>
                                    <select  class="form-control form-control-sm editText" id="admin_locn_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th >DPID</th>
                                    <td>
                                        <select  class="form-control form-control-sm editText" id="admin_dpid_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                    <th >DPTV</th>
                                    <td>
                                    <select  class="form-control form-control-sm editText" id="admin_dptv_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th >DEPT/DEXT</th>
                                    <td>
                                        <select  class="form-control form-control-sm editText" id="admin_dept_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                    <th >STAT</th>
                                    <td>
                                    <select  class="form-control form-control-sm editText" id="admin_stat_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th >REAL</th>
                                    <td>
                                        <select  class="form-control form-control-sm editText" id="admin_real_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                    <th >BANK</th>
                                    <td>
                                    <select  class="form-control form-control-sm editText" id="admin_bank_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th >KINS</th>
                                    <td>
                                        <select  class="form-control form-control-sm editText" id="admin_kins_update" disabled>
                                                <option value="BROWSE">BROWSE</option>
                                                <option value="FULL">FULL</option>
                                                <option value="NONE">NONE</option>
                                     </select>
                                    </td>
                                    <th >ADMIN</th>
                                    <td>
                                    <select  class="form-control form-control-sm editText" id="admin_update" disabled>
                                                <option value="YES">YES</option>
                                                <option value="NO">NO</option>
                                                
                                     </select>
                                    </td>
                                </tr>
                                    
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-end mt-1">
                    <div class="col-lg-auto " style="vertical-align: middle;">
                        <input class="btn  btn-sm btnCancel" type="button" id="cancel" value="Cancel" >
                        <input class="btn  btn-sm " type="button" id="edit" value="Edit">
                        <input class="btn  btn-sm " type="button" id="update" value="Update">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
    <script>
$(document).ready(function(){
$("#admin-main").show();
$("#admin-create").hide();
$("#admin-update").hide();
$("#update").hide();
        	
$.ajax({
	url : "data/admin/getAllUsers/1/2",
	dataType : 'json',
	success : function(result) {
		$("#admin_id").DataTable({
				"scrollY" : 320,
				"scrollX" : true,
				data : result,
				"columns" : [
						{
						"data" : "userId",
						"render" : function(data, type, row, meta) {
									data = '<a href="#" onclick="updateAdmin('+ data+ ')">' + data + '</a>';
											return data;
									}
						},
						{
							"data" : "userName"
						},
						{
							"data" : "locationSegmentAccess"
						},
						{
							"data" : "dpidSegmentAccess"
						},
						{
							"data" : "dptvSegmentAccess"
						},
						{
							"data" : "deptDextSegmentAccess"
						},
						{
							"data" : "statSegmentAccess"
						},
						{
							"data" : "realSegmentAccess"
						},
						{
							"data" : "bankSegmentAccess"
						},
						{
							"data" : "kinsSegmentAccess"
						},
						{
							"data" : "adminSegmentAccess"
						},
						{
							"data" : "userId",
							"render" : function(data,type,row,meta) {
								data = '<input class="btn  btn-sm " type="button" id="delete" value="Delete" data-toggle="modal" data-target="#exampleModal" onclick="onDelete('+ data +')">'
								return data;
							}
						}
				]
		});
	}
});

        	/* show create and hide update and main */
        	
        	$("#addAdmin").click(function() {
        		
        		$("#admin-main").hide();
        		$("#admin-create").show();
        		$("#admin-update").hide();
        		
        		$("#table_id_admincreate").DataTable();
        	});
        	
        	$("#edit").click(function(){
        		$("#update").show();
        		$("#edit").hide();
        		$(".form-control").prop("disabled", false);
        	});
        	
        	/* On click of cnacel back to admin main page  */
        	$(".btnCancel").click(function() {
        		$("#admin-main").show();
        		$("#admin-create").hide();
        		$("#admin-update").hide();
        		$("#update").hide();
        		$("#edit").show();
        		$(".form-control").prop("disabled", true);
        	});
        	
        	$("#update").click(function(){
                swal({
                    title: "Are you sure?",
                    text: "Your want to update this data",
                    type: "info",
                    showCancelButton: true,

                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                },
                function(){
                	$.ajax({
    					dataType : 'json',
    					type : "post",
    					contentType : 'application/json',
    					url : 'data/admin/updateUser',
    					data : JSON.stringify({
    						"userId" : $("#admin_userid_update").val(),
    						"locationSegmentAccess" : $("#admin_locn_update").val(),
    						"dpidSegmentAccess" : $("#admin_dpid_update").val(),
    						"dptvSegmentAccess" : $("#admin_dptv_update").val(),
    						"deptDextSegmentAccess" : $("#admin_dept_update").val(),
    						"statSegmentAccess" : $("#admin_stat_update").val(),
    						"realSegmentAccess" : $("#admin_real_update").val(),
    						"bankSegmentAccess" : $("#admin_bank_update").val(),
    						"kinsSegmentAccess" : $("#admin_kins_update").val(),
    						"adminSegmentAccess" : $("#admin_update").val()
    					}),
    					success : function(data, status) {
    						if (data.validationMessages.length > 0) {
								swal("Error",data.validationMessages,"error");
							}else{
	    						swal("Success","User record is updated successfully for user "	+ $("#admin-user-name").val(), "success");
	    						location.reload();
							}
    					},
    					error : function(request, textStatus, errorThrown) {
    						swal("Error",
    								"User record is not updated successfully for user - "
    										+ $("#admin-user-name").val(), "error");
    					}
    				});
                });
            });
        	
        	$("#add").click(function(){
                swal({
                    title: "Are you sure?",
                    text: "Your want to add this data",
                    type: "info",
                    showCancelButton: true,

                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                },
                function(){
                	$.ajax({
    					dataType : 'json',
    					type : "post",
    					contentType : 'application/json',
    					url : 'data/admin/createUser',
    					data : JSON.stringify({
    						"userId" : "",
    						"userName" : $("#admin-user-name").val(),
    						"locationSegmentAccess" : $("#locn-access").val(),
    						"dpidSegmentAccess" : $("#dpid-access").val(),
    						"dptvSegmentAccess" : $("#dptv-access").val(),
    						"deptDextSegmentAccess" : $("#dept-access").val(),
    						"statSegmentAccess" : $("#stat-access").val(),
    						"realSegmentAccess" : $("#real-access").val(),
    						"bankSegmentAccess" : $("#bank-access").val(),
    						"kinsSegmentAccess" : $("#kins-access").val(),
    						"adminSegmentAccess" : $("#admin-access").val()
    					}),
    					success : function(data, status) {
    						if (data.validationMessages.length > 0) {
								swal("Error",data.validationMessages,"error");
							}else{
	    						swal("Success","User record is added successfully for user "	+ $("#admin-user-name").val(), "success");
	    						location.reload();
							}
    					},
    					error : function(request, textStatus, errorThrown) {
    						swal("Error",
    								"User record is not added successfully for user - "
    										+ $("#admin-user-name").val(), "error");
    					}
    				});
                });
            });
        	
        });

        function onDelete(id){
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,

                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
            	$.ajax({
					dataType : 'json',
					type : "post",
					contentType : 'application/json',
					url : 'data/admin/deleteUser',
					data : JSON.stringify({
						"userId" : id,
						"userName" : $("#admin-user-name").val(),
						"locationSegmentAccess" : $("#locn-access").val(),
						"dpidSegmentAccess" : $("#dpid-access").val(),
						"dptvSegmentAccess" : $("#dptv-access").val(),
						"deptDextSegmentAccess" : $("#dept-access").val(),
						"statSegmentAccess" : $("#stat-access").val(),
						"realSegmentAccess" : $("#real-access").val(),
						"bankSegmentAccess" : $("#bank-access").val(),
						"kinsSegmentAccess" : $("#kins-access").val(),
						"adminSegmentAccess" : $("#admin-access").val()
					}),
					success : function(data, status) {
						swal("Success",
								"User record is deleted successfully for user - "
										+ $("#admin-user-name").val(), "success");
						$("#admin-main").show();
						$("#admin-create").hide();
						$("#admin-update").hide();
						location.reload();
					},
					error : function(request, textStatus, errorThrown) {
						swal("Error",
								"User record is not deleted successfully for user - "
										+ $("#admin-user-name").val(), "error");
					}
				});
            });
        }
        
        function updateAdmin(id){
        	$.ajax({
				url : "data/admin/getUser/" + id,
				dataType : 'json',
				success : function(result) {
					$("#admin_userid_update").val(result.userId);
					$("#admin_userName_update").text(result.userName);
					$("#admin_locn_update").val(result.locationSegmentAccess);
					$("#admin_dpid_update").val(result.dpidSegmentAccess);
					$("#admin_dptv_update").val(result.dptvSegmentAccess);
					$("#admin_dept_update").val(result.deptDextSegmentAccess);
					$("#admin_stat_update").val(result.statSegmentAccess);
					$("#admin_real_update").val(result.realSegmentAccess);
					$("#admin_bank_update").val(result.bankSegmentAccess);
					$("#admin_kins_update").val(result.kinsSegmentAccess);
					$("#admin_update").val(result.adminSegmentAccess);
				},
				error : function(request, textStatus, errorThrown) {
					swal("Error", "No Record found for Location - "
							+ locationNumber, "error");
				}
			});

        	$("#admin-main").hide();
    		$("#admin-create").hide();
    		$("#admin-update").show();
    		
        }
    </script>

</html>