<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>Location</title>
<link rel="stylesheet"
	href="views/css/font-awesome.min.css">
<link rel="stylesheet"
	href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet"
	href="views/css/bootstrap.min_1.css">
<link rel="stylesheet"
	href="views/css/style.css">
<script
	src="views/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="views/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="views/js/sweetalert.min.js"></script>
<script type="text/javascript"
	src="views/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="views/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet"
	href="views/css/jquery-ui.css">
<script type="text/javascript"
	src="views/js/jquery-ui.js"></script>
<link rel="stylesheet"
	href="views/css/sweetalert.css">
<script type="text/javascript"
	src="views/js/Validation.js"></script>

<%
User user =  (User) pageContext.getAttribute("user");   
%>

<script>
	function showLocation() {
		var x = document.getElementById("location-main");
		var y = document.getElementById("location-view");
		var z = document.getElementById("table-row");
		var v = document.getElementById("create-location-view");

		if (x.style.display === "none") {
			x.style.display = "block";
			y.style.display = "none";
			z.style.display = "none";
			v.style.display = "none";
		} else {
			x.style.display = "none";
		}
	}

	function showLocationView() {
		var x = document.getElementById("location-view");
		var y = document.getElementById("table-row");
		var z = document.getElementById("create-location-view");
		var location_number = document.getElementById("loc_num").value;
		var warningMessage = document.getElementById("alert-div");

		if (location_number.length <= 0 || location_number.length == -1) {
			warningMessage.style.display = "block";
			document.getElementById("loc_num").style.borderColor = "crimson";
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"
			return false;
		}
		if (location_number.length > 0) {
			warningMessage.style.display = "none";
			document.getElementById("loc_num").style.borderColor = "initial";
			if (x.style.display === "none") {
				x.style.display = "block";
				y.style.display = "none";
				z.style.display = "none"
			} else {
				x.style.display = "none";
			}
			//return false;
		}
		var locationNumber = $("#loc_num").val();
		$
				.ajax({
					url : "data/location/getLocation/" + locationNumber,
					dataType : 'json',
					success : function(result) {
						$("#locn-id").val(result.locationId);
						$("#loc_num").val(result.locationNumber);
						$("#locn-lext-id").val(result.locationLextId);
						$("#locn-view-streetAddress").val(result.streetAddress);
						$("#locn-view-city").val(result.city);
						$("#locn-view-state").val(result.state);
						$("#locn-view-zip").val(result.zipCode);
						$("#locn-view-zip-extension").val(
								result.zipCodeExtension);
						$("#locn-view-store-phone-no").val(result.storePhone);
						$("#locn-view-county").val(result.county);
						$("#locn-view-shopping-center").val(
								result.shoppingCenter);
						$("#locn-view-shipping-addr-flg").val(
								result.shippingAddressFlag);
						$("#locn-view-overhead").val(result.overHeadName);
						$("#locn-view-district-lvl-1").val(
								result.districtLevel1);
						$("#locn-view-district-lvl-2").val(
								result.districtLevel2);
						$("#locn-view-district-lvl-3")
								.val(result.summaryLevel3);
						$("#locn-view-manager-name").val(result.managerName);
						$("#locn-view-regional-manager").val(
								result.regionalManager);
						$("#locn-view-merch-code").val(result.areaMerchCoord);
						$("#locn-view-org-locn").val(result.originalNumber);
						$("#locn-view-nai-nbr").val(result.naiNumber);
						$("#locn-view-sears-unit-nbr").val(
								result.searUnitNumber);
						$("#locn-view-orig-kmart-nbr").val(
								result.originalKmartNumber);
						$("#locn-view-orig-facility").val(
								result.originalFacility);
						$("#locn-view-rpting-locn-nbr").val(
								result.rptingLocationNumber);
						$("#locn-view-bt-ft-nbr")
								.val(result.btftLocationNumber);
						$("#locn-view-receiving-store").val(
								result.recievingStoreNumber);
						$("#locn-view-division-code").val(result.devisionCode);
						$("#locn-view-type-code").val(result.typeCode);
						$("#locn-view-region-code").val(result.regionCode);
						$("#locn-view-format-type").val(result.formatType);
						$("#locn-view-format-sub-type").val(
								result.formatSubType);
						$("#locn-view-format-modifier").val(
								result.formatModifier);
						$("#locn-view-company-code").val(result.companyCode);
						$("#locn-view-climatic-zone").val(result.climaticZone);
						$("#locn-view-mdse-area-zone").val(
								result.mdseAreaRotoZone);
						$("#locn-view-ups-zone").val(result.upsZone);
						$("#locn-view-shipping-street-addr").val(
								result.shippingStreetAddress);
						$("#locn-view-shipping-city").val(result.shipCity);
						$("#locn-view-shipping-state").val(result.shipState);
						$("#locn-view-shipping-zip").val(result.shipZipCode);
						$("#locn-view-shipping-zip-ext").val(
								result.shipZipCodeExtension);
						$("#locn-view-shipping-fax-number").val(
								result.faxNumber);
						$("#locn-view-shipping-time-zone").val(result.timeZone);
						$("#locn-view-lattitude").val(result.latitude);
						$("#locn-view-longitude").val(result.langitude);
						$("#locn-view-state-nbr").val(result.stateNumber);
						$("#locn-view-city-nbr").val(result.cityNumber);
						$("#locn-view-county-nbr").val(result.countyNumber);
						$("#locn-view-planned-open-date").val(
								result.planedOpenDate);
						$("#locn-view-actual-open-date").val(
								result.actualOpenDate);
						$("#locn-view-closed-date").val(result.closeDate);
						$("#locn-view-temp-close-date").val(
								result.temporaryCloseDate);
						$("#locn-view-re-open-date").val(result.reOpenDate);
						$("#locn-view-planned-close-date").val(
								result.plannedCloseDate);
						$("#locn-view-holiday-close-day_1").val(
								result.holidayCloseDate1);
						$("#locn-view-holiday-close-day_2").val(
								result.holidayCloseDate2);
						$("#locn-view-holiday-close-day_3").val(
								result.holidayCloseDate3);
						$("#locn-view-holiday-close-day_4").val(
								result.holidayCloseDate4);
						$("#locn-view-holiday-close-day_5").val(
								result.holidayCloseDate5);
						$("#locn-view-holiday-close-day_6").val(
								result.holidayCloseDate6);
						$("#locn-view-holiday-close-day_7").val(
								result.holidayCloseDate7);
						$("#locn-view-holiday-close-day_8").val(
								result.holidayCloseDate8);
						$("#locn-view-holiday-close-day_9").val(
								result.holidayCloseDate9);
						$("#locn-view-holiday-close-day_10").val(
								result.holidayCloseDate10);
						$("#locn-view-total-sq-footage").val(
								result.totalSQFootage);
						$("#locn-view-selling-area-sq-foot").val(
								result.sellingAreaSqFoot);
						$("#locn-view-terminal-code").val(result.terminalCode);
						$("#locn-view-terminal-date").val(result.terminalDate);
					},
					error : function(request, textStatus, errorThrown) {
						swal("Error", "No Record found for Location - "
								+ locationNumber, "error");
					}
				});
	}

	function showCreateLocationView() {
		var x = document.getElementById("create-location-view");
		var y = document.getElementById("table-row");
		var z = document.getElementById("location-view");

		if (x.style.display === "none") {
			x.style.display = "block";
			y.style.display = "none"
			z.style.display = "none"
		} else {
			x.style.display = "none";
		}
		$("#loc_num").val("");
		$("#locn-id").val();
		$("#loc_num").val();
		$("#locn-lext-id").val();
		$("#locn-view-streetAddress").val();
		$("#locn-view-city").val();
		$("#locn-view-state").val();
		$("#locn-view-zip").val();
		$("#locn-view-zip-extension").val();
		$("#locn-view-store-phone-no").val();
		$("#locn-view-county").val();
		$("#locn-view-shopping-center").val();
		$("#locn-view-shipping-addr-flg").val();
		$("#locn-view-overhead").val();
		$("#locn-view-district-lvl-1").val();
		$("#locn-view-district-lvl-2").val();
		$("#locn-view-district-lvl-3").val();
		$("#locn-view-manager-name").val();
		$("#locn-view-regional-manager").val();
		$("#locn-view-merch-code").val();
		$("#locn-view-org-locn").val();
		$("#locn-view-nai-nbr").val();
		$("#locn-view-sears-unit-nbr").val();
		$("#locn-view-orig-kmart-nbr").val();
		$("#locn-view-orig-facility").val();
		$("#locn-view-rpting-locn-nbr").val();
		$("#locn-view-bt-ft-nbr").val();
		$("#locn-view-receiving-store").val();
		$("#locn-view-division-code").val();
		$("#locn-view-type-code").val();
		$("#locn-view-region-code").val();
		$("#locn-view-format-type").val();
		$("#locn-view-format-sub-type").val();
		$("#locn-view-format-modifier").val();
		$("#locn-view-company-code").val();
		$("#locn-view-climatic-zone").val();
		$("#locn-view-mdse-area-zone").val();
		$("#locn-view-ups-zone").val();
		$("#locn-view-shipping-street-addr").val();
		$("#locn-view-shipping-city").val();
		$("#locn-view-shipping-state").val();
		$("#locn-view-shipping-zip").val();
		$("#locn-view-shipping-zip-ext").val();
		$("#locn-view-shipping-fax-number").val();
		$("#locn-view-shipping-time-zone").val();
		$("#locn-view-lattitude").val();
		$("#locn-view-longitude").val();
		$("#locn-view-state-nbr").val();
		$("#locn-view-city-nbr").val();
		$("#locn-view-county-nbr").val();
		$("#locn-view-planned-open-date").val();
		$("#locn-view-actual-open-date").val();
		$("#locn-view-closed-date").val();
		$("#locn-view-temp-close-date").val();
		$("#locn-view-re-open-date").val();
		$("#locn-view-planned-close-date").val();
		$("#locn-view-holiday-close-day_1").val();
		$("#locn-view-holiday-close-day_2").val();
		$("#locn-view-holiday-close-day_3").val();
		$("#locn-view-holiday-close-day_4").val();
		$("#locn-view-holiday-close-day_5").val();
		$("#locn-view-holiday-close-day_6").val();
		$("#locn-view-holiday-close-day_7").val();
		$("#locn-view-holiday-close-day_8").val();
		$("#locn-view-holiday-close-day_9").val();
		$("#locn-view-holiday-close-day_10").val();
		$("#locn-view-total-sq-footage").val();
		$("#locn-view-selling-area-sq-foot").val();
		$("#locn-view-terminal-code").val();
		$("#locn-view-terminal-date").val();
		
	}

	$(document).ready(
			function() {
				$("#edit").click(
						function() {
							if (this.value == "Edit") {
								$("#update").show();
								$('.editText').removeAttr("readonly");
								$('.editText').removeClass(
										"form-control-plaintext").addClass(
										"form-control");
								this.value = "Cancel";

							} else {
								$("#update").hide();
								$('.editText').attr("readonly", true);
								$('.editText').removeClass("form-control").addClass("form-control-plaintext");
								this.value = "Edit";
								$("#location-view").hide();
								$("#create-location-view").hide();
								$("#table-row").hide();
							}
							
						});

				$(function() {
					$(".datepicker").datepicker({
						minDate : new Date()
					});
				});
			})
</script>
</head>

<body>
	<div class="container pr-0 pl-0">

		<div class="main">
			<div class="alert alert-danger" role="alert" id="alert-div">Please
				fill in the required fields</div>
			<div class="location-main pt-1" id="location-main">
				<div class="d-flex flex-row location-row">
					<div class="col-lg-auto mt-auto " style="vertical-align: middle;">
						<label> <b>LOCATION NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="loc_num" placeholder=""> <input
							type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="locn-id" placeholder=""> <input
							type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="locn-lext-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							onclick="showLocationView()">GO</button>
					</div>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1 pr-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" id='btnAll'>ALL</button>
					</div>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<%
						if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
					%>
					<div class="col-lg-auto pl-1">
						<button type="button" class="btn btn-sm m-0"
							style="font-size: 9px" onclick="showCreateLocationView()">ADD</button>
					</div>
					<%
						}
					%>
				</div>
			</div>


			<!-- Create Location View -->
			<div class="mt-2" id="create-location-view">
				<div class="container">
					<div class="d-flex justify-content-end">
						<button class="btn btn-outline-info btn-sm ml-2 mt-2"
							type="button" id="save" value="Save">Save</button>
						<input class="btn btn-outline-dark btn-sm ml-2 mt-2" type="button"
							value="Cancel" id="cancelFromAddPage">
					</div>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td>STREET ADDRESS</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-streetAddress" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
											<tr>
												<td>CITY</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-city" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>STATE</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-state" value=""
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td style="width: 78px">ZIP CODE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-zipCode" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td style="width: 150px">ZIP CODE EXTENSION</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-zipCode-Extension" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>STORE PHONE NO</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-store-Phone" value=""
													onkeydown="maxvalidation.call(this,11,event)"></td>
											</tr>
											<tr>
												<td>COUNTY</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-county" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>SHOPPING CENTER</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-shopping-center" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
											<tr>
												<td>SHIPPING ADDRESS FLAG</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-shipping-address-flag" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>OVERHEAD NAME</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-overhead-name" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
										</tbody>
									</table>
									<h6>
										<b>LOCATION HIERARCHIES</b>
									</h6>
									<table class="table table-bordered mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">DISTRICT LEVEL 1</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-district-lvl-1" value=""></td>
											</tr>
											<tr>
												<td>DISTRICT LEVEL 2</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-district-lvl-2" value=""></td>
											</tr>
											<tr>
												<td>SUMMARY LEVEL 3</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-district-lvl-3" value=""></td>
											</tr>
											<tr>
												<td>MANAGER NAME</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-manager-name" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>REGIONAL MANAGER</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-regional-manager" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>AREA MERCH COORD</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-area-merch-cord" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">ORIG LOCN</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-original-location" value=""></td>
											</tr>
											<tr>
												<td>NAI NBR</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-nai-nbr" value=""></td>
											</tr>
											<tr>
												<td>SEARS UNIT NBR</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-sears-unit-nbr" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>ORIG KMART NBR</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-orig-kmart-nbr" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>ORIG FACILITY</td>
												<td><input type="text"
													class="form-control form-control-sm alphabetValidation"
													id="locn-create-orig-facility" value=""
													onkeydown="maxvalidation.call(this,7,event)"></td>
											</tr>
											<tr>
												<td>RPTING LOCN NBR</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-rpting-locn-nbr" value=""></td>
											</tr>
											<tr>
												<td>BT/FT LOCN NBR</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-bt-ft-location-nbr" value=""></td>
											</tr>
											<tr>
												<td>RECEIVING STORE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-recieving-store" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>DIVISION CODE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-division-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>TYPE CODE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-type-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>REGION CODE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-region-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>FORMAT TYPE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-format-type" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>FORMAT SUB TYPE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-format-sub-type" value=""
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td>FORMAT MODIFIER</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-format-modifier" value="" value=""
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td>COMPANY CODE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-company-code" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>CLIMATIC ZONE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-climatic-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>MDSE AREA/ROTO ZN</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-mdse-area-roto-zn" value=""></td>
											</tr>
											<tr>
												<td>UPS ZONE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-ups-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td>SHIPPING STREET ADDRESS</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-shipping-street-address" value="">
												</td>
											</tr>
											<tr>
												<td>SHIPPING CITY</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-shipping-city" value=""></td>
											</tr>
											<tr>
												<td>SHIPPING STATE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-shipping-state" value=""></td>
											</tr>
											<tr>
												<td>SHIPPING ZIP</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-shipping-zip" value=""></td>
											</tr>
											<tr>
												<td>SHIPPING ZIP EXT</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-shipping-zip-ext" value=""></td>
											</tr>
											<tr>
												<td>FAX NUMBER</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-fax-number" value=""></td>
											</tr>
											<tr>
												<td>TIME ZONE</td>
												<td><input type="text"
													class="form-control form-control-sm numericValidation"
													id="locn-create-time-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>LATITUDE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-latitude" value=""></td>
											</tr>
											<tr>
												<td>LONGITUDE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-longitude" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">STATE NO</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-state-no" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>CITY NO</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-city-no" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>COUNTY NO</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-county-no" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>PLANNED OPEN DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-planned-open-date" value=""></td>
											</tr>
											<tr>
												<td>ACTUAL OPEN DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-actual-open-date" value=""></td>
											</tr>
											<tr>
												<td>CLOSE DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-close-date" value=""></td>
											</tr>
											<tr>
												<td>TEMP CLOSE DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-temp-close-date" value=""></td>
											</tr>
											<tr>
												<td>REOPEN DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-reopen-date" value=""></td>
											</tr>
											<tr>
												<td>PLANNED CLOSE DATE</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-planned-close-date" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>HOLIDAY CLOSE DATE 1</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-1" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 2</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-2" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 3</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker "
													id="locn-create-holiday-close-date-3" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 4</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker "
													id="locn-create-holiday-close-date-4" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 5</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-5" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 6</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-6" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 7</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-7" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 8</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-8" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 9</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-9" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 10</td>
												<td><input type="text"
													class="form-control form-control-sm datepicker"
													id="locn-create-holiday-close-date-10" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>TOTAL SQ FOOTAGE</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-total-sq-footage" value=""
													onkeydown="maxvalidation.call(this,7,event)"></td>
											</tr>
											<tr>
												<td>SELLING AREA SQ FOOT</td>
												<td><input type="text"
													class="form-control form-control-sm"
													id="locn-create-selling-area-sq-foot" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>TERMINAL CODE</td>
												<td><input type="email"
													class="form-control form-control-sm"
													id="locn-create-terminal-code" value="" disabled="disabled"></td>
											</tr>
											<tr>
												<td>TERMINAL DATE</td>
												<td><input type="email"
													class="form-control form-control-sm"
													id="locn-create-terminal-date" value="" disabled="disabled"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="mt-2" id="location-view">
				<div class="container">
					<div class="d-flex justify-content-end">
						<%
							if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
						%>
						<input class="btn btn-outline-info btn-sm ml-2 mt-2" type="button"
							id="edit" value="Edit"> <input
							class="btn btn-outline-primary btn-sm ml-2 mt-2" type="button"
							id="update" value="Update">
						<%
							}
						%>
					</div>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td>STREET ADDRESS</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-streetAddress" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
											<tr>
												<td>CITY</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-city" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>STATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-state" value=""
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td style="width: 78px">ZIP CODE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-zip" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td style="width: 150px">ZIP CODE EXTENSION</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-zip-extension" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>STORE PHONE NO</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-store-phone-no" value=""
													onkeydown="maxvalidation.call(this,11,event)"></td>
											</tr>
											<tr>
												<td>COUNTY</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-county" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>SHOPPING CENTER</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-shopping-center" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
											<tr>
												<td>SHIPPING ADDRESS FLAG</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-shipping-addr-flg" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>OVERHEAD NAME</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-overhead" value=""
													onkeydown="maxvalidation.call(this,25,event)"></td>
											</tr>
										</tbody>
									</table>
									<h6>
										<b>Location Hirarchies</b>
									</h6>
									<table
										class="table table-bordered table-striped mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">DISTRICT LEVEL 1</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-district-lvl-1" value=""></td>
											</tr>
											<tr>
												<td>DISTRICT LEVEL 2</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-district-lvl-2" value=""></td>
											</tr>
											<tr>
												<td>SUMMARY LEVEL 3</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-district-lvl-3" value=""></td>
											</tr>
											<tr>
												<td>MANAGER NAME</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-manager-name" value=""
													onkeydown="maxvalidation.call(this,20,event)"></td>
											</tr>
											<tr>
												<td>REGIONAL MANAGER</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-regional-manager" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>AREA MERCH COORD</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-merch-code" value=""></td>
											</tr>
										</tbody>
									</table>
									<table
										class="table table-bordered table-striped mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">ORIG LOCN</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-org-locn" value=""></td>
											</tr>
											<tr>
												<td>NAI NBR</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-nai-nbr" value=""></td>
											</tr>
											<tr>
												<td>SEARS UNIT NBR</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-sears-unit-nbr" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>ORIG KMART NBR</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-orig-kmart-nbr" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>ORIG FACILITY</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-orig-facility" value=""
													onkeydown="maxvalidation.call(this,7,event)"></td>
											</tr>
											<tr>
												<td>RPTING LOCN NBR</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-rpting-locn-nbr" value=""></td>
											</tr>
											<tr>
												<td>BT/FT LOCN NBR</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-bt-ft-nbr" value=""></td>
											</tr>
											<tr>
												<td>RECEIVING STORE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-receiving-store" value=""></td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>DIVISION CODE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-division-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>TYPE CODE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-type-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>REGION CODE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-region-code" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>FORMAT TYPE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-format-type" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>FORMAT SUB TYPE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-format-sub-type" value=""
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td>FORMAT MODIFIER</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText alphabetValidation"
													id="locn-view-format-modifier" value="" 
													onkeydown="maxvalidation.call(this,2,event)"></td>
											</tr>
											<tr>
												<td>COMPANY CODE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-company-code" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>CLIMATIC ZONE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-climatic-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>MDSE AREA/ROTO ZN</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-mdse-area-zone" value=""></td>
											</tr>
											<tr>
												<td>UPS ZONE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-ups-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td>SHIPPING STREET ADDRESS</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-street-addr"
													value=""></td>
											</tr>
											<tr>
												<td>SHIPPING CITY</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-city" value="">
												</td>
											</tr>
											<tr>
												<td>SHIPPING STATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-state" value=""></td>
											</tr>
											<tr>
												<td>SHIPPING ZIP</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-zip" value=""></td>
											</tr>
											<tr>
												<td>SHIPPING ZIP EXT</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-zip-ext" value=""></td>
											</tr>
											<tr>
												<td>FAX NUMBER</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-shipping-fax-number" value="">
												</td>
											</tr>
											<tr>
												<td>TIME ZONE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText numericValidation"
													id="locn-view-shipping-time-zone" value=""
													onkeydown="maxvalidation.call(this,1,event)"></td>
											</tr>
											<tr>
												<td>LATITUDE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-lattitude" value=""></td>
											</tr>
											<tr>
												<td>LONGITUDE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-longitude" value=""></td>
											</tr>
										</tbody>
									</table>
									<table
										class="table table-bordered table-striped mt-2 m-table hirar-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tbody>
											<tr>
												<td style="width: 71px">STATE NO</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-state-nbr" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>CITY NO</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText "
													id="locn-view-city-nbr" value=""
													onkeydown="maxvalidation.call(this,5,event)"></td>
											</tr>
											<tr>
												<td>COUNTY NO</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-county-nbr" value=""
													onkeydown="maxvalidation.call(this,3,event)"></td>
											</tr>
											<tr>
												<td>PLANNED OPEN DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText "
													id="locn-view-planned-open-date" value="">
												</td>
											</tr>
											<tr>
												<td>ACTUAL OPEN DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-actual-open-date" value="">
												</td>
											</tr>
											<tr>
												<td>CLOSE DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-closed-date" value=""></td>
											</tr>
											<tr>
												<td>TEMP CLOSE DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-temp-close-date" value=""></td>
											</tr>
											<tr>
												<td>REOPEN DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-re-open-date" value=""></td>
											</tr>
											<tr>
												<td>PLANNED CLOSE DATE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-planned-close-date" value="">
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>HOLIDAY CLOSE DATE 1</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_1" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 2</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_2" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 3</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_3" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 4</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_4" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 5</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_5" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 6</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_6" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 7</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_7" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 8</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_8" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 9</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_9" value=""></td>
											</tr>
											<tr>
												<td>HOLIDAY CLOSE DATE 10</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-holiday-close-day_10" value="">
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>TOTAL SQ FOOTAGE</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-total-sq-footage" value=""
													onkeydown="maxvalidation.call(this,7,event)"></td>
											</tr>
											<tr>
												<td>SELLING AREA SQ FOOT</td>
												<td><input type="text" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-selling-area-sq-foot" value="">
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif">
										<tbody>
											<tr>
												<td>TERMINAL CODE</td>
												<td><input type="email" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-terminal-code" value="" disabled="disabled" ></td>
											</tr>
											<tr>
												<td>TERMINAL DATE</td>
												<td><input type="email" readonly=""
													class="form-control-plaintext form-control-sm editText"
													id="locn-view-terminal-date" value="" disabled="disabled"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="table-row mt-2 ml-2 mr-2" id="table-row">
				<div class="table-responsive">
					<table
						class="table table-bordered table-striped table-light table_id mr-0 ml-0"
						id="table_id_locn"
						style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif;">
						<thead id="tabelHead">
							<tr>
								<th>LOCATION</th>
								<th>FACILITY #</th>
								<th>CITY</th>
								<th>STATE</th>
								<th>OPEN-DATE</th>
								<th>CLOSE-DATE</th>
								<th>FORMAT-TYPE</th>
								<th>FORMAT-SUB-TYPE</th>
								<th>TIME-ZONE</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document)
				.ready(
						function() {
							$("#location-main").show();
							$("#update").hide();

							$("#btnAll")
									.click(
											function() {

												$("#location-view").hide();
												$("#table-row").show();
												$("#create-location-view")
														.hide();
												$
														.ajax({
															url : "data/location/getAllLocations/1/1",
															dataType : 'json',
															success : function(
																	result) {
																$(
																		"#table_id_locn")
																		.DataTable(
																				{
																					"destroy" : true,
																					"scrollY" : 350,
																					"scrollX" : true,
																					data : result,
																					"columns" : [
																							{
																								"data" : "locationNumber",
																								"render" : function(
																										data,
																										type,
																										row,
																										meta) {
																									data = '<a href="#" onclick="getDataByLocatioID('
																											+ data
																											+ ')">'
																											+ data
																											+ '</a>';
																									return data;

																								}
																							},

																							{
																								"data" : "originalFacility"
																							},
																							{
																								"data" : "city"
																							},
																							{
																								"data" : "state"
																							},
																							{
																								"data" : "actualOpenDate"
																							},
																							{
																								"data" : "closeDate"
																							},
																							{
																								"data" : "formatType"
																							},
																							{
																								"data" : "subType"
																							},
																							{
																								"data" : "timeZone"
																							} ]
																				});
															}
														});
											});

							$("#save")
									.click(
											function() {
												var warningMessage = document
														.getElementById("alert-div")
												var createLocationNodes = document
														.querySelectorAll("#create-location-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < createLocationNodes.length; i++) {
													if (createLocationNodes[i].value.length <= 0
															|| createLocationNodes[i].value.length == -1) {
														warningMessage.style.display = "block";
														createLocationNodes[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (createLocationNodes[i].value.length > 0) {
														warningMessage.style.display = "none";
														createLocationNodes[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to add this data",
																type : "info",
																showCancelButton : true,
																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/location/createLocation',
																			data : JSON
																					.stringify({
																						"locationId" : "",
																						"locationLextId" : "",
																						"locationNumber" : $(
																								"#loc_num")
																								.val(),
																						"streetAddress" : $(
																								"#locn-create-streetAddress")
																								.val(),
																						"city" : $(
																								"#locn-create-city")
																								.val(),
																						"state" : $(
																								"#locn-create-state")
																								.val(),
																						"zipCode" : $(
																								"#locn-create-zipCode")
																								.val(),
																						"zipCodeExtension" : $(
																								"#locn-create-zipCode-Extension")
																								.val(),
																						"storePhone" : $(
																								"#locn-create-store-Phone")
																								.val(),
																						"county" : $(
																								"#locn-create-county")
																								.val(),
																						"shoppingCenter" : $(
																								"#locn-create-shopping-center")
																								.val(),
																						"shippingAddressFlag" : $(
																								"#locn-create-shipping-address-flag")
																								.val(),
																						"overHeadName" : $(
																								"#locn-create-overhead-name")
																								.val(),
																						"shippingStreetAddress" : $(
																								"#locn-create-shipping-street-address")
																								.val(),
																						"shipCity" : $(
																								"#locn-create-shipping-city")
																								.val(),
																						"shipState" : $(
																								"#locn-create-shipping-state")
																								.val(),
																						"shipZipCode" : $(
																								"#locn-create-shipping-zip")
																								.val(),
																						"shipZipCodeExtension" : $(
																								"#locn-create-shipping-zip-ext")
																								.val(),
																						"faxNumber" : $(
																								"#locn-create-fax-number")
																								.val(),
																						"timeZone" : $(
																								"#locn-create-time-zone")
																								.val(),
																						"latitude" : $(
																								"#locn-create-latitude")
																								.val(),
																						"langitude" : $(
																								"#locn-create-longitude")
																								.val(),
																						"districtLevel1" : $(
																								"#locn-create-district-lvl-1")
																								.val(),
																						"districtLevel2" : $(
																								"#locn-create-district-lvl-2")
																								.val(),
																						"summaryLevel3" : $(
																								"#locn-create-district-lvl-3")
																								.val(),
																						"managerName" : $(
																								"#locn-create-manager-name")
																								.val(),
																						"regionalManager" : $(
																								"#locn-create-regional-manager")
																								.val(),
																						"areaMerchCoord" : $(
																								"#locn-create-area-merch-cord")
																								.val(),
																						"originalNumber" : $(
																								"#locn-create-original-location")
																								.val(),
																						"naiNumber" : $(
																								"#locn-create-nai-nbr")
																								.val(),
																						"searUnitNumber" : $(
																								"#locn-create-sears-unit-nbr")
																								.val(),
																						"originalKmartNumber" : $(
																								"#locn-create-orig-kmart-nbr")
																								.val(),
																						"originalFacility" : $(
																								"#locn-create-orig-facility")
																								.val(),
																						"rptingLocationNumber" : $(
																								"#locn-create-rpting-locn-nbr")
																								.val(),
																						"btftLocationNumber" : $(
																								"#locn-create-bt-ft-location-nbr")
																								.val(),
																						"recievingStoreNumber" : $(
																								"#locn-create-recieving-store")
																								.val(),
																						"devisionCode" : $(
																								"#locn-create-division-code")
																								.val(),
																						"typeCode" : $(
																								"#locn-create-type-code")
																								.val(),
																						"regionCode" : $(
																								"#locn-create-region-code")
																								.val(),
																						"formatType" : $(
																								"#locn-create-format-type")
																								.val(),
																						"formatSubType" : $(
																								"#locn-create-format-sub-type")
																								.val(),
																						"formatModifier" : $(
																								"#locn-create-format-modifier")
																								.val(),
																						"companyCode" : $(
																								"#locn-create-company-code")
																								.val(),
																						"climaticZone" : $(
																								"#locn-create-climatic-zone")
																								.val(),
																						"mdseAreaRotoZone" : $(
																								"#locn-create-mdse-area-roto-zn")
																								.val(),
																						"upsZone" : $(
																								"#locn-create-ups-zone")
																								.val(),
																						"stateNumber" : $(
																								"#locn-create-state-no")
																								.val(),
																						"cityNumber" : $(
																								"#locn-create-city-no")
																								.val(),
																						"countyNumber" : $(
																								"#locn-create-county-no")
																								.val(),
																						"planedOpenDate" : $(
																								"#locn-create-planned-open-date")
																								.val(),
																						"actualOpenDate" : $(
																								"#locn-create-actual-open-date")
																								.val(),
																						"closeDate" : $(
																								"#locn-create-close-date")
																								.val(),
																						"temporaryCloseDate" : $(
																								"#locn-create-close-date")
																								.val(),
																						"reOpenDate" : $(
																								"#locn-create-reopen-date")
																								.val(),
																						"plannedCloseDate" : $(
																								"#locn-create-planned-close-date")
																								.val(),
																						"holidayCloseDate1" : $(
																								"#locn-create-holiday-close-date-1")
																								.val(),
																						"holidayCloseDate2" : $(
																								"#locn-create-holiday-close-date-2")
																								.val(),
																						"holidayCloseDate3" : $(
																								"#locn-create-holiday-close-date-3")
																								.val(),
																						"holidayCloseDate4" : $(
																								"#locn-create-holiday-close-date-4")
																								.val(),
																						"holidayCloseDate5" : $(
																								"#locn-create-holiday-close-date-5")
																								.val(),
																						"holidayCloseDate6" : $(
																								"#locn-create-holiday-close-date-6")
																								.val(),
																						"holidayCloseDate7" : $(
																								"#locn-create-holiday-close-date-7")
																								.val(),
																						"holidayCloseDate8" : $(
																								"#locn-create-holiday-close-date-8")
																								.val(),
																						"holidayCloseDate9" : $(
																								"#locn-create-holiday-close-date-9")
																								.val(),
																						"holidayCloseDate10" : $(
																								"#locn-create-holiday-close-date-10")
																								.val(),
																						"totalSQFootage" : $(
																								"#locn-create-total-sq-footage")
																								.val(),
																						"sellingAreaSqFoot" : $(
																								"#locn-create-selling-area-sq-foot")
																								.val(),
																						"terminalCode" : $(
																								"#locn-create-terminal-code")
																								.val(),
																						"terminalDate" : $(
																								"#locn-create-terminal-date")
																								.val()
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error",data.validationMessages,"error");
																					$('input:text').css("border-color", "");
																				} else {
																					swal("Success", "Location data is added successfully for location - "+ $("#loc_num").val(), "success");
																					$('input:text').prop("readonly",true);
																					$('input:text').css("border-color","");
																					$('input:text').removeClass("form-control").addClass("form-control-plaintext");
																				}
																			},
																			error : function(data, status) {
																				swal("Error", "Location data is not added successfully for location - " + $("#loc_num")	.val(), "error");
																			}
																		});


															})
												}
												;

											}

									);

							$("#cancel").click(function() {

								$("#location-view").hide();
								$("#create-location-view").hide();
								$("#table-row").hide();

							});

							$("#cancelFromAddPage").click(function() {
								$("#location-view").hide();
								$("#create-location-view").hide();
								$("#table-row").hide();

							})

							$("#update")
									.click(
											function() {
												var warningMessage = document
														.getElementById("alert-div");
												var createLocationNodes = document
														.querySelectorAll("#location-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < createLocationNodes.length; i++) {
													if (createLocationNodes[i].value.length <= 0
															|| createLocationNodes[i].value.length == -1) {
														warningMessage.style.display = "block";
														createLocationNodes[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (createLocationNodes[i].value.length > 0) {
														warningMessage.style.display = "none";
														createLocationNodes[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/location/updateLocation',
																			data : JSON
																					.stringify({
																						"locationId" : $(
																								"#locn-id")
																								.val(),
																						"locationLextId" : document.getElementById("locn-lext-id").value,
																						"locationNumber" : $(
																								"#loc_num")
																								.val(),
																						"streetAddress" : $(
																								"#locn-view-streetAddress")
																								.val(),
																						"city" : $(
																								"#locn-view-city")
																								.val(),
																						"state" : $(
																								"#locn-view-state")
																								.val(),
																						"zipCode" : $(
																								"#locn-view-zip")
																								.val(),
																						"zipCodeExtension" : $(
																								"#locn-view-zip-extension")
																								.val(),
																						"storePhone" : $(
																								"#locn-view-store-phone-no")
																								.val(),
																						"county" : $(
																								"#locn-view-county")
																								.val(),
																						"shoppingCenter" : $(
																								"#locn-view-shopping-center")
																								.val(),
																						"shippingAddressFlag" : $(
																								"#locn-view-shipping-addr-flg")
																								.val(),
																						"overHeadName" : $(
																								"#locn-view-overhead")
																								.val(),
																						"shippingStreetAddress" : $(
																								"#locn-view-shipping-street-addr")
																								.val(),
																						"shipCity" : $(
																								"#locn-view-shipping-city")
																								.val(),
																						"shipState" : $(
																								"#locn-view-shipping-state")
																								.val(),
																						"shipZipCode" : $(
																								"#locn-view-shipping-zip")
																								.val(),
																						"shipZipCodeExtension" : $(
																								"#locn-view-shipping-zip-ext")
																								.val(),
																						"faxNumber" : $(
																								"#locn-view-shipping-fax-number")
																								.val(),
																						"timeZone" : $(
																								"#locn-view-shipping-time-zone")
																								.val(),
																						"latitude" : $(
																								"#locn-view-lattitude")
																								.val(),
																						"langitude" : $(
																								"#locn-view-longitude")
																								.val(),
																						"districtLevel1" : $(
																								"#locn-view-district-lvl-1")
																								.val(),
																						"districtLevel2" : $(
																								"#locn-view-district-lvl-2")
																								.val(),
																						"summaryLevel3" : $(
																								"#locn-view-district-lvl-3")
																								.val(),
																						"managerName" : $(
																								"#locn-view-manager-name")
																								.val(),
																						"regionalManager" : $(
																								"#locn-view-regional-manager")
																								.val(),
																						"areaMerchCoord" : $(
																								"#locn-view-merch-code")
																								.val(),
																						"originalNumber" : $(
																								"#locn-view-org-locn")
																								.val(),
																						"naiNumber" : $(
																								"#locn-view-nai-nbr")
																								.val(),
																						"searUnitNumber" : $(
																								"#locn-view-sears-unit-nbr")
																								.val(),
																						"originalKmartNumber" : $(
																								"#locn-view-orig-kmart-nbr")
																								.val(),
																						"originalFacility" : $(
																								"#locn-view-orig-facility")
																								.val(),
																						"rptingLocationNumber" : $(
																								"#locn-view-rpting-locn-nbr")
																								.val(),
																						"btftLocationNumber" : $(
																								"#locn-view-bt-ft-nbr")
																								.val(),
																						"recievingStoreNumber" : $(
																								"#locn-view-receiving-store")
																								.val(),
																						"devisionCode" : $(
																								"#locn-view-division-code")
																								.val(),
																						"typeCode" : $(
																								"#locn-view-type-code")
																								.val(),
																						"regionCode" : $(
																								"#locn-view-region-code")
																								.val(),
																						"formatType" : $(
																								"#locn-view-format-type")
																								.val(),
																						"formatSubType" : $(
																								"#locn-view-format-sub-type")
																								.val(),
																						"formatModifier" : $(
																								"#locn-view-format-modifier")
																								.val(),
																						"companyCode" : $(
																								"#locn-view-company-code")
																								.val(),
																						"climaticZone" : $(
																								"#locn-view-climatic-zone")
																								.val(),
																						"mdseAreaRotoZone" : $(
																								"#locn-view-mdse-area-zone")
																								.val(),
																						"upsZone" : $(
																								"#locn-view-ups-zone")
																								.val(),
																						"stateNumber" : $(
																								"#locn-view-state-nbr")
																								.val(),
																						"cityNumber" : $(
																								"#locn-view-city-nbr")
																								.val(),
																						"countyNumber" : $(
																								"#locn-view-county-nbr")
																								.val(),
																						"planedOpenDate" : $(
																								"#locn-view-planned-open-date")
																								.val(),
																						"actualOpenDate" : $(
																								"#locn-view-actual-open-date")
																								.val(),
																						"closeDate" : $(
																								"#locn-view-closed-date")
																								.val(),
																						"temporaryCloseDate" : $(
																								"#locn-view-temp-close-date")
																								.val(),
																						"reOpenDate" : $(
																								"#locn-view-re-open-date")
																								.val(),
																						"plannedCloseDate" : $(
																								"#locn-view-planned-close-date")
																								.val(),
																						"holidayCloseDate1" : $(
																								"#locn-view-holiday-close-day_1")
																								.val(),
																						"holidayCloseDate2" : $(
																								"#locn-view-holiday-close-day_2")
																								.val(),
																						"holidayCloseDate3" : $(
																								"#locn-view-holiday-close-day_3")
																								.val(),
																						"holidayCloseDate4" : $(
																								"#locn-view-holiday-close-day_4")
																								.val(),
																						"holidayCloseDate5" : $(
																								"#locn-view-holiday-close-day_5")
																								.val(),
																						"holidayCloseDate6" : $(
																								"#locn-view-holiday-close-day_6")
																								.val(),
																						"holidayCloseDate7" : $(
																								"#locn-view-holiday-close-day_7")
																								.val(),
																						"holidayCloseDate8" : $(
																								"#locn-view-holiday-close-day_8")
																								.val(),
																						"holidayCloseDate9" : $(
																								"#locn-view-holiday-close-day_9")
																								.val(),
																						"holidayCloseDate10" : $(
																								"#locn-view-holiday-close-day_10")
																								.val(),
																						"totalSQFootage" : $(
																								"#locn-view-total-sq-footage")
																								.val(),
																						"sellingAreaSqFoot" : $(
																								"#locn-view-selling-area-sq-foot")
																								.val(),
																						"terminalCode" : $(
																								"#locn-view-terminal-code")
																								.val(),
																						"terminalDate" : $(
																								"#locn-view-terminal-date")
																								.val()
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error", data.validationMessages, "error");
																					$('input:text').css("border-color", "");
																				} else {
																					swal("Success","Location data is updated successfully for location - "+ $("#loc_num").val(),"success");
																					$("#update").hide();
																					$('.editText').prop("readonly",true);
																					$('input:text').css("border-color","");
																					$('.editText').removeClass("form-control").addClass("form-control-plaintext");
																					$("#edit").val("Edit");
																				}
																			},
																			error : function(
																					data,
																					status) {
																				swal(
																						"Error",
																						"Location data is not updated successfully for location - "
																								+ $(
																										"#loc_num")
																										.val(),
																						"error");
																			}
																		});
																
															})
												}
												;
											});
						});

		function getDataByLocatioID(locationId) {
			$("#location-view").show();
			$("#table-row").hide();
			$("#create-location-view").hide();

			$
					.ajax({
						url : "data/location/getLocation/" + locationId,
						dataType : 'json',
						success : function(result) {
							$("#locn-id").val(result.locationId);
							$("#loc_num").val(result.locationNumber);
							$("#locn-lext-id").val(result.locationLextId);
							$("#locn-view-streetAddress").val(
									result.streetAddress);
							$("#locn-view-city").val(result.city);
							$("#locn-view-state").val(result.state);
							$("#locn-view-zip").val(result.zipCode);
							$("#locn-view-zip-extension").val(
									result.zipCodeExtension);
							$("#locn-view-store-phone-no").val(
									result.storePhone);
							$("#locn-view-county").val(result.county);
							$("#locn-view-shopping-center").val(
									result.shoppingCenter);
							$("#locn-view-shipping-addr-flg").val(
									result.shippingAddressFlag);
							$("#locn-view-overhead").val(result.overHeadName);
							$("#locn-view-district-lvl-1").val(
									result.districtLevel1);
							$("#locn-view-district-lvl-2").val(
									result.districtLevel2);
							$("#locn-view-district-lvl-3").val(
									result.summaryLevel3);
							$("#locn-view-manager-name")
									.val(result.managerName);
							$("#locn-view-regional-manager").val(
									result.regionalManager);
							$("#locn-view-merch-code").val(
									result.areaMerchCoord);
							$("#locn-view-org-locn").val(result.originalNumber);
							$("#locn-view-nai-nbr").val(result.naiNumber);
							$("#locn-view-sears-unit-nbr").val(
									result.searUnitNumber);
							$("#locn-view-orig-kmart-nbr").val(
									result.originalKmartNumber);
							$("#locn-view-orig-facility").val(
									result.originalFacility);
							$("#locn-view-rpting-locn-nbr").val(
									result.rptingLocationNumber);
							$("#locn-view-bt-ft-nbr").val(
									result.btftLocationNumber);
							$("#locn-view-receiving-store").val(
									result.recievingStoreNumber);
							$("#locn-view-division-code").val(
									result.devisionCode);
							$("#locn-view-type-code").val(result.typeCode);
							$("#locn-view-region-code").val(result.regionCode);
							$("#locn-view-format-type").val(result.formatType);
							$("#locn-view-format-sub-type").val(
									result.formatSubType);
							$("#locn-view-format-modifier").val(
									result.formatModifier);
							$("#locn-view-company-code")
									.val(result.companyCode);
							$("#locn-view-climatic-zone").val(
									result.climaticZone);
							$("#locn-view-mdse-area-zone").val(
									result.mdseAreaRotoZone);
							$("#locn-view-ups-zone").val(result.upsZone);
							$("#locn-view-shipping-street-addr").val(
									result.shippingStreetAddress);
							$("#locn-view-shipping-city").val(result.shipCity);
							$("#locn-view-shipping-state")
									.val(result.shipState);
							$("#locn-view-shipping-zip")
									.val(result.shipZipCode);
							$("#locn-view-shipping-zip-ext").val(
									result.shipZipCodeExtension);
							$("#locn-view-shipping-fax-number").val(
									result.faxNumber);
							$("#locn-view-shipping-time-zone").val(
									result.timeZone);
							$("#locn-view-lattitude").val(result.latitude);
							$("#locn-view-longitude").val(result.langitude);
							$("#locn-view-state-nbr").val(result.stateNumber);
							$("#locn-view-city-nbr").val(result.cityNumber);
							$("#locn-view-county-nbr").val(result.countyNumber);
							$("#locn-view-planned-open-date").val(
									result.planedOpenDate);
							$("#locn-view-actual-open-date").val(
									result.actualOpenDate);
							$("#locn-view-closed-date").val(result.closeDate);
							$("#locn-view-temp-close-date").val(
									result.temporaryCloseDate);
							$("#locn-view-re-open-date").val(result.reOpenDate);
							$("#locn-view-planned-close-date").val(
									result.plannedCloseDate);
							$("#locn-view-holiday-close-day_1").val(
									result.holidayCloseDate1);
							$("#locn-view-holiday-close-day_2").val(
									result.holidayCloseDate2);
							$("#locn-view-holiday-close-day_3").val(
									result.holidayCloseDate3);
							$("#locn-view-holiday-close-day_4").val(
									result.holidayCloseDate4);
							$("#locn-view-holiday-close-day_5").val(
									result.holidayCloseDate5);
							$("#locn-view-holiday-close-day_6").val(
									result.holidayCloseDate6);
							$("#locn-view-holiday-close-day_7").val(
									result.holidayCloseDate7);
							$("#locn-view-holiday-close-day_8").val(
									result.holidayCloseDate8);
							$("#locn-view-holiday-close-day_9").val(
									result.holidayCloseDate9);
							$("#locn-view-holiday-close-day_10").val(
									result.holidayCloseDate10);
							$("#locn-view-total-sq-footage").val(
									result.totalSQFootage);
							$("#locn-view-selling-area-sq-foot").val(
									result.sellingAreaSqFoot);
							$("#locn-view-terminal-code").val(
									result.terminalCode);
							$("#locn-view-terminal-date").val(
									result.terminalDate);
						},
						error : function(request, textStatus, errorThrown) {
							swal("Error", "No Record found for Location - "
									+ locationId, "error");
						}
					});
		}
	</script>
</body>

</html>