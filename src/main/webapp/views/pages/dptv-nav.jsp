<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>DPTV</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<link rel="stylesheet" href="views/css/sweetalert.css">
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="views/js/sweetalert.min.js"></script>
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<script>
	function showLocation() {
		var x = document.getElementById("dpid-main");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
	$(function() {
		$(".datepicker").datepicker({
			minDate : new Date()
		});
	});
</script>
</head>
<%
User user =  (User) pageContext.getAttribute("user");   
%>
<body>
	<div class="container pr-0 pl-0">
		<div class="main">
			<div class="alert alert-danger" role="alert" id="dptv-alert-div">Please
				fill in the required fields</div>
			<div class="dpid-main pt-1" id="dptv-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
						<label> <b>LOCATION NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="location-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="location-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							id="btngo">GO</button>
					</div>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1 pr-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" id="btnAll">ALL</button>
					</div>
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
					
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" id="add">ADD</button>
					</div>
					<%} %>
				</div>
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-2 " style="vertical-align: middle;">
						<label> <b>DEPARTMENT NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,3,event)"
							style="font-size: 9px" id="deparment-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="deparment-id" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="dptv-id" placeholder="">
					</div>

				</div>

				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-2 " style="vertical-align: middle;">
						<label> <b>OPEN DATE</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm datepicker"
							style="font-size: 9px" id="opnDate" placeholder="">
					</div>

				</div>
			</div>


			<!-- DPTV Show details on GO -->

			<div class="mt-2 ml-2 mr-2" id="dptv-view">
				<div class="container">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
					
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="save" value="Save" onclick="saveFunction()"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel" value="Cancel" onclick="cancelFunction()"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="update" value="Update">
					<%} %>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="location-form">
								<div class="form-group">
									<table class="table table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">

										<tr style="vertical-align: middle">
											<td>CHANGE INDICATOR</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,1,event)"
												id="dptv-view-change-indicator" value=""></td>
										</tr>

										<tr style="vertical-align: middle">
											<td>VENDOR DEPARTMENT</td>
											<td><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,3,event)"
												id="dptv-view-vendor-department" value=""></td>
										</tr>

										<tr style="vertical-align: middle">
											<td>VENDOR OPEN DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="dptv-view-vendor-open-date" value=""></td>
										</tr>
										<tr>
											<td>VENDOR OPEN DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="dptv-view-vendor-close-date" value=""></td>
										</tr>
										<tr>
											<td>VENDOR DUNS NUMBER</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,11,event)"
												id="dptv-view-vendor-duns-nbr" value=""></td>
										</tr>
										<tr>
											<td>VENDOR ACCOUNT NUMBER</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-view-vendor-acct-number" value=""></td>
										</tr>
										<tr>
											<td>COMBINED FEE PERCENT</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-view-combined-fee-percent" value=""></td>
										</tr>
										<tr>
											<td>SPLIT FEE PERCENT</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-view-split-fee-percent" value=""></td>
										</tr>
										<tr>
											<td>PERCENT OWNED BY KMART</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,4,event)"
												id="dptv-view-percent-owned-by-kmart" value=""></td>
										</tr>
										<tr>
											<td>ADVERTISING FEE PERCENT</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-view-advertising-fee-percent" value=""></td>
										</tr>
										<tr>
											<td>WEEKLY FLORIDA TAX</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,3,event)"
												id="dptv-view-florida-tax" value=""></td>

										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="mt-2 ml-3 mr-3" id="dptv-create-view">
				<div class="container">
					<div class="d-flex justify-content-end">
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button"
							id="save" value="Save" onclick="saveFunction()"> <input
							class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel"
							value="Cancel" onclick="cancelFunction()">
						<!-- <button type="button" class="btn btn-outline-dark btn-sm ml-2 mb-2">Delete</button> -->
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr style="vertical-align: middle">
											<td>CHANGE INDICATOR</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,1,event)"
												id="dptv-create-change-indicator" value=""></td>
										</tr>
										<tr style="vertical-align: middle">
											<td>VENDOR DEPARTMENT NUMBER</td>
											<td><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,3,event)"
												id="dptv-create-vendor-department-number" value="">
											</td>
										</tr>
										<tr>
											<td>VENDOR OPEN DATE</td>
											<td><input type="text"
												class="form-control form-control-sm datepicker "
												id="dptv-create-vendor-open-date" value=""></td>
										</tr>
										<tr>
											<td>VENDOR CLOSE DATE</td>
											<td><input type="text"
												class="form-control form-control-sm datepicker"
												id="dptv-create-vendor-close-date" value=""></td>
										</tr>
										<tr>
											<td>VENDOR DUNS NUMBER</td>
											<td><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,11,event)"
												id="dptv-create-vendor-duns-number" value=""></td>
										</tr>
										<tr>
											<td>VENDOR ACCOUNT NUMBER</td>
											<td><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-create-vendor-account-number" value=""></td>
										</tr>
										<tr>
											<td>COMBINED FEE PERCENT</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-create-combined-fee-recent" value=""></td>
										</tr>
										<tr>
											<td>SPLIT FEE PERCENT</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-create-split-fee-percent" value=""></td>
										</tr>
										<tr>
											<td>PERCENT OWNED BY KMART</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,4,event)"
												id="dptv-create-owned-by-kmart" value=""></td>
										</tr>
										<tr>
											<td>ADVERTISING FEE PERCENT</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dptv-create-advertising-fee-percent" value=""></td>
										</tr>
										<tr>
											<td>WEEKLY FLORIDA TAX</td>
											<td><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,3,event)"
												id="dptv-create-weekly-florida-tax" value=""></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="table-row mt-2 ml-2 mr-2" id="table-row">
				<div class="table-responsive">
					<table
						class="table table-bordered table-striped table-light table_id mr-0 ml-0"
						id="table_id_dptv"
						style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif">
						<thead id="tabelHead">
							<tr>
								<th>DPTV-ID</th>
								<th>CONTROL-KEY</th>
								<th>VENDOR-OPEN-DATE</th>
								<th>VENDOR-CLOSE-DATE</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>

	<script>
		$(document)
				.ready(
						function() {

							$("#save").hide();
							$("#cancel").hide();
							$("#dptv-view").hide();
							$("#dptv-create-view").hide();
							$("#update").hide();

							$("#table-row").hide();

							$("#btnDPTV").click(function() {
								$("#dptv-main").show();
								$("#dptv-view").hide();
								$("#dptv-create-view").hide();
								$("#table-row").hide();
							});

							$("#btngo")
									.click(
											function() {
												var warningMessage = document
														.getElementById("dptv-alert-div")
												var dptvView = document
														.querySelectorAll("#dptv-main input[type=email]");
												var showFlag = false;
												for (var i = 0; i < dptvView.length; i++) {
													if (dptvView[i].value.length <= 0
															|| dptvView[i].value.length == -1) {
														warningMessage.style.display = "block";
														dptvView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (dptvView[i].value.length > 0) {
														warningMessage.style.display = "none";
														dptvView[i].style.borderColor = "initial";
														showFlag = true;
													}
												}
												if (showFlag == true) {
													$("#dptv-view").show();
													$("#table-row").hide();
													$("#dptv-create-view")
															.hide();
													$("#save").hide();
													$("#cancel").hide();
													$("#edit").show();

													var locationNumber = $("#location-number").val();
													var departmentNumber = $("#deparment-number").val();
													var openDate = $("#opnDate").val();
													$
															.ajax({
																url : "data/dptv/getDptv/"+ locationNumber+ "/"+ departmentNumber + "/"+openDate,
																dataType : 'json',
																success : function(
																		result) {
																	$(
																			"#location-number")
																			.val(
																					result.locationNumber);
																	$(
																			"#location-id")
																			.val(
																					result.locationId);
																	$(
																			"#deparment-number")
																			.val(
																					result.departmentNumber);
																	$(
																			"#deparment-id")
																			.val(
																					result.dpid);
																	$(
																			"#dptv-id")
																			.val(
																					result.dptvId);
																	$(
																			"#opnDate")
																			.val(
																					result.openDate);
																	$(
																			"#dptv-view-change-indicator")
																			.val(
																					result.changeIndicator);
																	$(
																			"#dptv-view-vendor-department")
																			.val(
																					result.vendorDepartment);
																	$(
																			"#dptv-view-vendor-open-date")
																			.val(
																					result.vendorOpenDate);
																	$(
																			"#dptv-view-vendor-close-date")
																			.val(
																					result.vendorCloseDate);
																	$(
																			"#dptv-view-vendor-duns-nbr")
																			.val(
																					result.vendorDunsNumber);
																	$(
																			"#dptv-view-vendor-acct-number")
																			.val(
																					result.vendorAccountNumber);
																	$(
																			"#dptv-view-combined-fee-percent")
																			.val(
																					result.combinedFeePercent);
																	$(
																			"#dptv-view-split-fee-percent")
																			.val(
																					result.splitFeePercent);
																	$(
																			"#dptv-view-percent-owned-by-kmart")
																			.val(
																					result.percentageOwnedByKMART);
																	$(
																			"#dptv-view-advertising-fee-percent")
																			.val(
																					result.advertisingFeePercentage);
																	$(
																			"#dptv-view-florida-tax")
																			.val(
																					result.weeklyFloridaTax);
																},
																error : function(
																		request,
																		textStatus,
																		errorThrown) {
																	swal(
																			"Error",
																			"No Record found for Location Number - "
																					+ locationNumber
																					+ " Department Number - "
																					+ departmentNumber,
																			"error");
																}
															});

													$('input:text').attr(
															"readonly");
													$('input:text')
															.removeClass(
																	"form-control")
															.addClass(
																	"form-control-plaintext");
												} else {
													$("#dptv-view").hide();
													$("#table-row").hide();
													$("#dptv-create-view")
															.hide();
													$("#save").hide();
													$("#cancel").hide();
													$("#edit").hide();
												}
											});
							$("#btnAll")
									.click(
											function() {
												$("#dptv-view").hide();
												$("#table-row").show();
												$("#dptv-create-view").hide();
												var locationNumber = $("#location-number").val();
												var departmentNumber = $("#deparment-number").val();
												$
														.ajax({
															url : "data/dptv/getAllDptv/"+locationNumber+"/"+departmentNumber,
															dataType : 'json',
															success : function(
																	result) {
																$(
																		"#table_id_dptv")
																		.DataTable(
																				{
																					"destroy" : true,
																					"scrollY" : 320,
																					"scrollX" : true,
																					data : result,
																					"columns" : [
																							{
																								"data" : "dptvId",
																								"render" : function(
																										data,
																										type,
																										row,
																										meta) {
																									data = '<a href="#" onclick="getDataById('
																											+ data
																											+ ')">'
																											+ data
																											+ '</a>';
																									return data;

																								}
																							},
																							{
																								"data" : "keyControl"
																							},
																							{
																								"data" : "vendorOpenDate"
																							},
																							{
																								"data" : "vendorCloseDate"
																							} ]
																				});
																	}
														});
											});

							$("#add").click(
									function() {
										$("#dptv-view").hide();
										$("#table-row").hide();
										$("#dptv-create-view").show();

										$("#save").show();
										$("#cancel").show();
										$("#edit").hide();
										$("#update").hide();

										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

										$("#location-number").val("");
										$("#location-id").val("");
										$("#deparment-number").val("");
										$("#deparment-id").val("");
										$("#dptv-id").val("");
										$("#opnDate").val("");

									});

							$(".btnedit").click(
									function() {
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

										$("#update").show();
										$("#cancel").show();
										$("#edit").hide();

									});

							$("#cancel").click(function() {
								$("#dptv-view").show();
								$("#table-row").hide();
								$("#dptv-create-view").hide();
								$("#update").hide();

							});

							$("#save")
									.click(
											function() {
												var warningMessage = document
														.getElementById("dptv-alert-div")
												var createdptv = document
														.querySelectorAll("#dptv-create-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < createdptv.length; i++) {
													if (createdptv[i].value.length <= 0
															|| createdptv[i].value.length == -1) {
														warningMessage.style.display = "block";
														createdptv[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (createdptv[i].value.length > 0) {
														warningMessage.style.display = "none";
														createdptv[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															}, function() {

															})

													;
													$('.form-control')
															.addClass(
																	'form-control-plaintext')
															.removeClass(
																	'form-control');
													$(".form-control-plaintext")
															.prop('readonly',
																	true);

													$("#save").hide();
													$("#cancel").hide();
													$("#edit").show();
													$("#update").show();
													$('input:text').prop(
															"readonly", true);
													$('input:text').css(
															"border-color", "");
													$('#locNo')
															.addClass(
																	'form-control')
															.removeClass(
																	'form-control-plaintext');
													$("#locNo").removeAttr(
															"readonly");
													$('#dptNo')
															.addClass(
																	'form-control')
															.removeClass(
																	'form-control-plaintext');
													$("#dptNo").removeAttr(
															"readonly");
													$('#opnDate')
															.addClass(
																	'form-control')
															.removeClass(
																	'form-control-plaintext');
													$("#opnDate").removeAttr(
															"readonly");

												}
											});

							$("#update")
									.click(
											function() {
												var warningMessage = document
														.getElementById("dptv-alert-div")
												var createdptv = document
														.querySelectorAll("#dptv-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < createdptv.length; i++) {
													if (createdptv[i].value.length <= 0
															|| createdptv[i].value.length == -1) {
														warningMessage.style.display = "block";
														createdptv[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (createdptv[i].value.length > 0) {
														warningMessage.style.display = "none";
														createdptv[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/dptv/updateDptv',
																			data : JSON
																					.stringify({
																						"dpid" : $(
																								"#deparment-id")
																								.val(),
																						"locationNumber" : $(
																								"#location-number")
																								.val(),
																						"departmentNumber" : $(
																								"#deparment-number")
																								.val(),
																						"dptvId" : $(
																								"#dptv-id")
																								.val(),
																						"changeIndicator" : $(
																								"#dptv-view-change-indicator")
																								.val(),
																						"vendorDepartment" : $(
																								"#dptv-view-vendor-department")
																								.val(),
																						"vendorOpenDate" : $(
																								"#dptv-view-vendor-open-date")
																								.val(),
																						"vendorCloseDate" : $(
																								"#dptv-view-vendor-close-date")
																								.val(),
																						"vendorDunsNumber" : $(
																								"#dptv-view-vendor-duns-nbr")
																								.val(),
																						"vendorAccountNumber" : $(
																								"#dptv-view-vendor-acct-number")
																								.val(),
																						"combinedFeePercent" : $(
																								"#dptv-view-combined-fee-percent")
																								.val(),
																						"splitFeePercent" : $(
																								"#dptv-view-split-fee-percent")
																								.val(),
																						"percentageOwnedByKMART" : $(
																								"#dptv-view-percent-owned-by-kmart")
																								.val(),
																						"advertisingFeePercentage" : $(
																								"#dptv-view-advertising-fee-percent")
																								.val(),
																						"weeklyFloridaTax" : $(
																								"#dptv-view-florida-tax")
																								.val()
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error",data.validationMessages,"error");
																				}else{
																					swal("Success", "DPTV record for location - "+ $("#location-number").val() + " and Department - "+ $("#deparment-number").val()	+ " is updated successfully","success");
																				}
																			},
																			error : function(
																					data,
																					status) {
																				swal(
																						"Error",
																						"DPTV record for location - "
																								+ $(
																										"#location-number")
																										.val()
																								+ " and Department - "
																								+ $(
																										"#deparment-number")
																										.val()
																								+ " is not updated successfully",
																						"error");
																			},
																		});

																$('input:text')
																		.addClass(
																				'form-control-plaintext')
																		.removeClass(
																				'form-control');
																$(
																		".form-control-plaintext")
																		.prop(
																				'readonly',
																				true);
																$('input:text')
																		.css(
																				"border-color",
																				"");

																$("#save")
																		.hide();
																$("#cancel")
																		.hide();
																$("#edit")
																		.show();
																$("#update")
																		.hide();
																$('#locNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#locNo")
																		.removeAttr(
																				"readonly");
																$('#dptNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#dptNo")
																		.removeAttr(
																				"readonly");
																$('#opnDate')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#opnDate")
																		.removeAttr(
																				"readonly");
															})
												}
												;
											});
						});

		function getDataById(dtpvId) {
			$("#dptv-view").show();
			$("#table-row").hide();
			$("#dptv-create-view").hide();
			$("#save").hide();
			$("#cancel").hide();
			$("#edit").show();

			$
					.ajax({
						url : "data/dptv/getDptv/"	+ dtpvId,
						dataType : 'json',
						success : function(result) {
							$("#location-number").val(result.locationNumber);
							$("#location-id").val(result.locationId);
							$("#deparment-number").val(result.departmentNumber);
							$("#deparment-id").val(result.dpid);
							$("#dptv-id").val(result.dptvId);
							$("#opnDate").val(result.vendorOpenDate);
							$("#dptv-view-change-indicator").val(
									result.changeIndicator);
							$("#dptv-view-vendor-department").val(
									result.vendorDepartment);
							$("#dptv-view-vendor-open-date").val(
									result.vendorOpenDate);
							$("#dptv-view-vendor-close-date").val(
									result.vendorCloseDate);
							$("#dptv-view-vendor-duns-nbr").val(
									result.vendorDunsNumber);
							$("#dptv-view-vendor-acct-number").val(
									result.vendorAccountNumber);
							$("#dptv-view-combined-fee-percent").val(
									result.combinedFeePercent);
							$("#dptv-view-split-fee-percent").val(
									result.splitFeePercent);
							$("#dptv-view-percent-owned-by-kmart").val(
									result.percentageOwnedByKMART);
							$("#dptv-view-advertising-fee-percent").val(
									result.advertisingFeePercentage);
							$("#dptv-view-florida-tax").val(
									result.weeklyFloridaTax);
						},
						error : function(request, textStatus, errorThrown) {
							swal("Error", "No Record found for DPTV - "
									+ dtpvId, "error");
						}
					});
		}

		function cancelFunction() {
			$("#dptv-view").show();
			$("#table-row").hide();
			$("#dptv-create-view").hide();
			$('.form-control').addClass('form-control-plaintext').removeClass(
					'form-control');
			$(".form-control-plaintext").prop('readonly', true);
			;

			$("#save").hide();
			$("#cancel").hide();
			$("#edit").show();
			$("#update").hide();
			$('#locNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#locNo").removeAttr("readonly");
			$('#dptNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#dptNo").removeAttr("readonly");
			$('#opnDate').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#opnDate").removeAttr("readonly");
		}

		function saveFunction() {

			var warningMessage = document.getElementById("dptv-alert-div")
			var createdptv = document
					.querySelectorAll("#dptv-create-view input[type=text]");
			var showFlag = false;
			for (var i = 0; i < createdptv.length; i++) {
				if (createdptv[i].value.length <= 0
						|| createdptv[i].value.length == -1) {
					warningMessage.style.display = "block";
					createdptv[i].style.borderColor = "crimson";
					showFlag = false;
					break;
				}
				if (createdptv[i].value.length > 0) {
					warningMessage.style.display = "none";
					createdptv[i].style.borderColor = "initial";
					showFlag = true;
				}

			}
			if (showFlag == true) {
				swal(
						{
							title : "Are you sure?",
							text : "Your want to add this data",
							type : "info",
							showCancelButton : true,

							confirmButtonText : "Yes",
							closeOnConfirm : false
						},

						function() {
							$
									.ajax({
										dataType : 'json',
										type : "post",
										contentType : 'application/json',
										url : 'data/dptv/createDptv',
										data : JSON
												.stringify({
													"dpid" : $("#deparment-id")
															.val(),
													"locationNumber" : $(
															"#location-number")
															.val(),
													"departmentNumber" : $(
															"#deparment-number")
															.val(),
													"dptvId" : "",
													"changeIndicator" : $(
															"#dptv-create-change-indicator")
															.val(),
													"vendorDepartment" : $(
															"#dptv-create-vendor-department-number")
															.val(),
													"vendorOpenDate" : $(
															"#dptv-create-vendor-open-date")
															.val(),
													"vendorCloseDate" : $(
															"#dptv-create-vendor-close-date")
															.val(),
													"vendorDunsNumber" : $(
															"#dptv-create-vendor-duns-number")
															.val(),
													"vendorAccountNumber" : $(
															"#dptv-create-vendor-account-number")
															.val(),
													"combinedFeePercent" : $(
															"#dptv-create-combined-fee-recent")
															.val(),
													"splitFeePercent" : $(
															"#dptv-create-split-fee-percent")
															.val(),
													"percentageOwnedByKMART" : $(
															"#dptv-create-owned-by-kmart")
															.val(),
													"advertisingFeePercentage" : $(
															"#dptv-create-advertising-fee-percent")
															.val(),
													"weeklyFloridaTax" : $(
															"#dptv-create-weekly-florida-tax")
															.val()
												}),
										success : function(data, status) {
											if (data.validationMessages.length > 0) {
												swal("Error",data.validationMessages,"error");
											}else{
												swal("Success", "DPTV record for location - "+ $("#location-number").val() + " and Department - "+ $("#deparment-number").val()	+ " is added successfully","success");
											}
										},
										error : function(data, status) {
											swal(
													"Error",
													"DPTV record for location - "
															+ $(
																	"#location-number")
																	.val()
															+ " and Department - "
															+ $(
																	"#deparment-number")
																	.val()
															+ " is not added successfully",
													"error");
										},
									});

							$('input:text').prop("readonly", true);
							$('input:text').css("border-color", "");
							$('input:text').removeClass("form-control")
									.addClass("form-control-plaintext");

							$("#save").hide();
							$("#cancel").hide();
							$("#edit").show();
							$("#update").show();

						})
				}
			;

		}
	</script>
</body>

</html>