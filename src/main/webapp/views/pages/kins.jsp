<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>KINS</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<script src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="views/js/sweetalert.min.js"></script>
<link rel="stylesheet" href="views/css/sweetalert.css">
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<%
User user =  (User) pageContext.getAttribute("user");   
%>
<script>
	function showLocation() {
		var x = document.getElementById("kins-main");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
	function showKinsView() {
		var x = document.getElementById("kins-view");
		//var y = document.getElementById("dpid-create-view");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
	$(document).ready(
			function() {
				$("#edit").click(
						function() {
							if (this.value == "Edit") {
								$("#update").show();
								$('input:text').removeAttr("readonly");
								$('input:text').removeClass(
										"form-control-plaintext").addClass(
										"form-control");
								this.value = "Cancel";
							} else {
								$("#update").hide();
								$('input:text').attr("readonly", true);
								$('input:text').removeClass("form-control")
										.addClass("form-control-plaintext");
								this.value = "Edit";
							}
						});

				$(function() {
					$(".datepicker").datepicker({
						minDate : new Date()
					});
				});
			})
</script>
</head>

<body>
	<div class="container pr-0 pl-0">
		<div class="main pb-2">
			<div class="alert alert-danger" role="alert" id="kins-alert-div">Please
				fill in the required fields</div>
			<div class="kins-main pt-1" id="kins-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-auto mt-auto " style="vertical-align: middle;">
						<label> <b>LOCATION NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="kins-location-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="kins-location-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							onclick="" id="btnGo">GO</button>
					</div>


				</div>
			</div>


			<!-- DPID Show details on GO -->

			<div class="mt-2 ml-1 mr-1" id="kins-view">
				<div class="container">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
						<input class="btn  btn-sm ml-2 mt-2" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2" type="button" id="update" value="Update">
					<%} %>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr style="vertical-align: middle">
											<td>TERMINAL CODE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,1,event)"
												id="kins-view-terminal-code" value=""></td>
										</tr>
										<tr style="vertical-align: middle">
											<td>TERMINAL DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm datepicker"
												id="kins-view-terminal-date" value=""></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>




		</div>
	</div>
</body>
<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
<script>
	$(document)
			.ready(
					function() {
						$("#update").hide();
						$("#kins-main").show();
						$("#table_id").DataTable();
						$("#btnAll").click(function() {

							$("#kins-view").hide();
							$("#table-row").show();
						});
						$("#btnGo")
								.click(
										function() {
											var warningMessage = document
													.getElementById("kins-alert-div")
											var kinsView = document
													.querySelectorAll("#kins-main input[type=email]");
											var showFlag = false;
											for (var i = 0; i < kinsView.length; i++) {
												if (kinsView[i].value.length <= 0
														|| kinsView[i].value.length == -1) {
													warningMessage.style.display = "block";
													kinsView[i].style.borderColor = "crimson";
													showFlag = false;
													break;
												}
												if (kinsView[i].value.length > 0) {
													warningMessage.style.display = "none";
													kinsView[i].style.borderColor = "initial";
													showFlag = true;
												}

											}
											if (showFlag == true) {
												$("#kins-view").show();
												$("#table-row").hide();

												var locationNumber = $(
														"#kins-location-number")
														.val();

												$
														.ajax({
															url : "data/kins/getKins/"+ locationNumber,
															dataType : 'json',
															success : function(
																	result) {
																$(
																		"#kins-location-id")
																		.val(
																				result.locationId);
																$(
																		"#kins-location-number")
																		.val(
																				result.locationNumber);
																$(
																		"#kins-view-terminal-code")
																		.val(
																				result.terminalCode);
																$(
																		"#kins-view-terminal-date")
																		.val(
																				result.terminalDate);
															},
															error : function(
																	request,
																	textStatus,
																	errorThrown) {
																swal(
																		"Error",
																		"No Record found for Location - "
																				+ locationNumber,
																		"error");
															}
														});
											}
										});

						$("#update")
								.click(
										function() {
											var warningMessage = document
													.getElementById("kins-alert-div")
											var kinsView = document
													.querySelectorAll("#kins-view input[type=text]");
											var showFlag = false;
											for (var i = 0; i < kinsView.length; i++) {
												if (kinsView[i].value.length <= 0
														|| kinsView[i].value.length == -1) {
													warningMessage.style.display = "block";
													kinsView[i].style.borderColor = "crimson";
													showFlag = false;
													break;
												}
												if (kinsView[i].value.length > 0) {
													warningMessage.style.display = "none";
													kinsView[i].style.borderColor = "initial";
													showFlag = true;
												}

											}
											if (showFlag == true) {
												swal(
														{
															title : "Are you sure?",
															text : "Your want to update this data",
															type : "info",
															showCancelButton : true,
															confirmButtonText : "Yes",
															closeOnConfirm : false
														},
														function() {
															$
																	.ajax({
																		dataType : 'json',
																		type : "post",
																		contentType : 'application/json',
																		url : 'data/kins/updateKins',
																		data : JSON
																				.stringify({
																					"locationId" : $(
																							"#kins-location-id")
																							.val(),
																					"locationNumber" : $(
																							"#kins-location-number")
																							.val(),
																					"terminalCode" : $(
																							"#kins-view-terminal-code")
																							.val(),
																					"terminalDate" : $(
																							"#kins-view-terminal-date")
																							.val()
																				}),
																		success : function(data,status) {
																			if (data.validationMessages.length > 0) {
																				swal("Error",data.validationMessages,"error");
																			}else{
																				swal("Success","Kins data is updated successfully for location - "+ $("#kins-location-number").val(),"success");
																			}
																		},
																		error : function(
																				data,
																				status) {
																			swal(
																					"Success",
																					"Kins data is not updated successfully for location - "
																							+ $(
																									"#kins-location-number")
																									.val(),
																					"error");
																		}
																	});

															$("#update").hide();
															$('input:text')
																	.prop(
																			"readonly",
																			true);
															$('input:text')
																	.css(
																			"border-color",
																			"");
															$('input:text')
																	.removeClass(
																			"form-control")
																	.addClass(
																			"form-control-plaintext");
															$("#edit").val(
																	"Edit");
														})
											}
											;

										});
					});
</script>

</html>