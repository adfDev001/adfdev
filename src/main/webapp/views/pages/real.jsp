<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>REAL</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<link rel="stylesheet" href="views/css/sweetalert.css">
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script src="views/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<script>
	function showLocation() {
		var x = document.getElementById("dpid-main");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>
</head>
<%
User user =  (User) pageContext.getAttribute("user");   
%>

<body>
	<div class="container pr-0 pl-0">
		<div class="main">
			<div class="alert alert-danger" role="alert" id="reaL-alert-div">Please
				fill in the required fields</div>
			<div class="dpid-main pt-1" id="real-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
						<label> <b>Location Number</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="locNo" placeholder=""> <input
							type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="real-occ-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							id="btngo">GO</button>
					</div>
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" id="add">ADD</button>
					</div>
					<% } %>
				</div>
			</div>


			<!-- REAL Show details on GO -->

			<div class="mt-2 ml-3 mr-3" id="real-view">
				<div class="container">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="save" value="Save" onclick="saveFunction()"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel" value="Cancel" onclick="cancelFunction()"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="update" value="Update">
					<%} %>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="location-form">
								<div class="form-group">
									<table class="table table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr>
											<td>LEASED OWNED CODE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,1,event)"
												id="real-view-lease-owned-date" value=""></td>
										</tr>
										<tr style="vertical-align: middle">
											<td>ORIGINAL LEASE DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="real-view-original-lease-date" value=""></td>
										</tr>
										<tr>
											<td>FIRST LEASE RENEWAL DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="real-view-1-lease-renewal-date" value=""></td>
										</tr>
										<tr>
											<td>SECOND LEASE RENEWAL DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="real-view-2-lease-renewal-date" value=""></td>
										</tr>
										<tr>
											<td>THIRD LEASE RENEWAL DATE</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												id="real-view-3-lease-renewal-date" value=""></td>
										</tr>

									</table>
								</div>
							</form>
						</div>
					</div>



				</div>
			</div>

			<div class="mt-2 ml-3 mr-3" id="real-create-view">
				<div class="container">
					<div class="d-flex justify-content-end">
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button"
							id="save" value="Save" onclick="saveFunction()"> <input
							class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel"
							value="Cancel" onclick="cancelFunction()">
						<!-- <button type="button" class="btn btn-outline-dark btn-sm ml-2 mb-2">Delete</button> -->
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr>
											<td>LEASED OWNED CODE</td>
											<td><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,1,event)"
												id="real-create-lease-owned-date" value=""></td>
										</tr>

										<tr style="vertical-align: middle">
											<td>ORIGINAL LEASE DATE</td>
											<td><input type="text"
												class="form-control form-control-sm"
												id="real-create-original-lease-date" value=""></td>
										</tr>
										<tr>
											<td>FIRST LEASE RENEWAL DATE</td>
											<td><input type="text"
												class="form-control form-control-sm"
												id="real-create-1-lease-renewal-date" value=""></td>
										</tr>
										<tr>
											<td>SECOND LEASE RENEWAL DATE</td>
											<td><input type="text"
												class="form-control form-control-sm"
												id="real-create-2-lease-renewal-date" value=""></td>
										</tr>
										<tr>
											<td>THIRD LEASE RENEWAL DATE</td>
											<td><input type="text"
												class="form-control form-control-sm"
												id="real-create-3-lease-renewal-date" value=""></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>


				</div>
			</div>

		</div>
	</div>



	<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="views/js/sweetalert.min.js"></script>

	<script>
		$(document)
				.ready(
						function() {
							$(function() {
								$(".datepicker").datepicker({
									minDate : new Date()
								});
							});

							$("#save").hide();
							$("#cancel").hide();
							$("#real-view").hide();
							$("#real-create-view").hide();

							$("#btnDPTV").click(function() {
								$("#real-main").show();
								$("#real-view").hide();
								$("#real-create-view").hide();
								<!--$("#table-row").hide();
								-->
							});

							$("#btngo")
									.click(
											function() {
												var warningMessage = document
														.getElementById("reaL-alert-div")
												var realView = document
														.querySelectorAll("#real-main input[type=email]");
												var showFlag = false;
												for (var i = 0; i < realView.length; i++) {
													if (realView[i].value.length <= 0
															|| realView[i].value.length == -1) {
														warningMessage.style.display = "block";
														realView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (realView[i].value.length > 0) {
														warningMessage.style.display = "none";
														realView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													$("#real-view").show();
													<!--$("#table-row").hide();
													-->
													$("#real-create-view")
															.hide();
													$("#save").hide();
													$("#cancel").hide();
													$("#edit").show();
													$("#update").hide();

													$('input:text').attr(
															"readonly");
													$('input:text')
															.removeClass(
																	"form-control")
															.addClass(
																	"form-control-plaintext");

													var locationNumber = $(
															"#locNo").val();
													$
															.ajax({
																url : "data/real/getReal/"+ locationNumber,
																dataType : 'json',
																success : function(
																		result) {
																	$(
																			"#real-occ-id")
																			.val(
																					result.realId);
																	$(
																			"#real-view-lease-owned-date")
																			.val(
																					result.leasedOwnedCode);
																	$(
																			"#real-view-original-lease-date")
																			.val(
																					result.originalLeasedDate);
																	$(
																			"#real-view-1-lease-renewal-date")
																			.val(
																					result.firstLeaseRenewalDate);
																	$(
																			"#real-view-2-lease-renewal-date")
																			.val(
																					result.secondLeaseRenewalDate);
																	$(
																			"#real-view-3-lease-renewal-date")
																			.val(
																					result.thirdLeaseRenewalDate);
																},
																error : function(
																		request,
																		textStatus,
																		errorThrown) {
																	swal(
																			"Error",
																			"No Record found for Location Number"
																					+ locationNumber,
																			"error");
																}

															});

												}
											});
							$("#btnAll").click(function() {

								$("#real-view").hide();
								$("#table-row").show();
								$("#real-create-view").hide();
							});

							$("#add").click(
									function() {
										$("#real-view").hide();
										<!--$("#table-row").hide();
										-->
										$("#real-create-view").show();
										$("#locNo").val("");

										$("#save").show();
										$("#cancel").show();
										$("#edit").hide();
										$("#update").hide();

										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

									});

							$(".btnedit").click(
									function() {
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");
										$("#update").show();

										$("#cancel").show();
										$("#edit").hide();

									});

							$("#cancel").click(function() {
								$("#real-view").show();
								<!--$("#table-row").hide();
								-->
								$("#real-create-view").hide();
								$("#update").hide();

							});

							$("#save")
									.click(
											function() {
												swal(
														{
															title : "Are you sure?",
															text : "Your want to update this data",
															type : "info",
															showCancelButton : true,

															confirmButtonText : "Yes",
															closeOnConfirm : false
														},
														function() {
															swal(
																	"Success",
																	"Your data updated successfully",
																	"success");
														});
												$('.form-control')
														.addClass(
																'form-control-plaintext')
														.removeClass(
																'form-control');
												$(".form-control-plaintext")
														.prop('readonly', true);

												$("#save").hide();
												$("#cancel").hide();
												$("#edit").show();
												$("#update").hide();
												$('#locNo')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#locNo").removeAttr(
														"readonly");
												$('#dptNo')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#dptNo").removeAttr(
														"readonly");
												$('#opnDate')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#opnDate").removeAttr(
														"readonly");

											});

							$("#update")
									.click(
											function() {
												var warningMessage = document
														.getElementById("reaL-alert-div")
												var realView = document
														.querySelectorAll("#real-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < realView.length; i++) {
													if (realView[i].value.length <= 0
															|| realView[i].value.length == -1) {
														warningMessage.style.display = "block";
														realView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (realView[i].value.length > 0) {
														warningMessage.style.display = "none";
														realView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/real/updateReal',
																			data : JSON
																					.stringify({
																						"realId" : $(
																								"#real-occ-id")
																								.val(),
																						"locationNumber" : $(
																								"#locNo")
																								.val(),
																						"leasedOwnedCode" : $(
																								"#real-view-lease-owned-date")
																								.val(),
																						"originalLeasedDate" : $(
																								"#real-view-original-lease-date")
																								.val(),
																						"firstLeaseRenewalDate" : $(
																								"#real-view-1-lease-renewal-date")
																								.val(),
																						"secondLeaseRenewalDate" : $(
																								"#real-view-1-lease-renewal-date")
																								.val(),
																						"thirdLeaseRenewalDate" : $(
																								"#real-view-1-lease-renewal-date")
																								.val()
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error",data.validationMessages,"error");
																				}else{
																					swal("Success","Real record is updated successfully for location - "+ $("#locNo").val(), "success");
																				}
																			},
																			error : function(
																					request,
																					textStatus,
																					errorThrown) {
																				swal(
																						"Error",
																						"Real record is not updated successfully for location - "
																								+ $(
																										"#locNo")
																										.val(),
																						"error");
																			}
																		});
																$(
																		'.form-control')
																		.addClass(
																				'form-control-plaintext')
																		.removeClass(
																				'form-control');
																$(
																		".form-control-plaintext")
																		.prop(
																				'readonly',
																				true);
																$('input:text').css("border-color", "");
																$("#save")
																		.hide();
																$("#cancel")
																		.hide();
																$("#edit")
																		.show();
																$("#update")
																		.show();
																$('#locNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#locNo")
																		.removeAttr(
																				"readonly");
																$('#dptNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#dptNo")
																		.removeAttr(
																				"readonly");
																$('#opnDate')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#opnDate")
																		.removeAttr(
																				"readonly");
															})
												}
												;
											});
						});

		function cancelFunction() {
			$("#real-view").show();
			<!--
			$("#table-row").hide();
			-->
			$("#real-create-view").hide();
			$('.form-control').addClass('form-control-plaintext').removeClass(
					'form-control');
			$(".form-control-plaintext").prop('readonly', true);
			;

			$("#save").hide();
			$("#cancel").hide();
			$("#edit").show();
			$("#update").hide();
			$('#locNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#locNo").removeAttr("readonly");
			$('#dptNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#dptNo").removeAttr("readonly");
			$('#opnDate').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#opnDate").removeAttr("readonly");
		}

		function saveFunction() {
			swal({
				title : "Are you sure?",
				text : "Your want to add this data",
				type : "info",
				showCancelButton : true,

				confirmButtonText : "Yes",
				closeOnConfirm : false
			}, function() {
				$.ajax({
					dataType : 'json',
					type : "post",
					contentType : 'application/json',
					url : 'data/real/createReal',
					data : JSON.stringify({
						"departmentId" : "",
						"locationNumber" : $("#locNo").val(),
						"leasedOwnedCode" : $("#real-create-lease-owned-date")
								.val(),
						"originalLeasedDate" : $(
								"#real-create-original-lease-date").val(),
						"firstLeaseRenewalDate" : $(
								"#real-create-1-lease-renewal-date").val(),
						"secondLeaseRenewalDate" : $(
								"#real-create-2-lease-renewal-date").val(),
						"thirdLeaseRenewalDate" : $(
								"#real-create-3-lease-renewal-date").val()
					}),
					success : function(data, status) {
						if (data.validationMessages.length > 0) {
							swal("Error",data.validationMessages,"error");
						}else{
							swal("Success","Real record is created successfully for location - "+ $("#locNo").val(), "success");
						}
					},
					error : function(request, textStatus, errorThrown) {
						swal("Error",
								"Real record is not added successfully for location - "
										+ $("#locNo").val(), "error");
					}
				});
				$('input:text').attr("readonly");
				$('input:text').removeClass("form-control").addClass(
						"form-control-plaintext");

				$("#save").hide();
				$("#cancel").hide();
				$("#edit").show();
				$("#update").show();

			});

		}
	</script>
</body>

</html>