<link href="views/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="views/css/font-awesome.min.css" rel="stylesheet">
<link href="views/css/Login.css" rel="stylesheet">
<script src="views/js/bootstrap.min.js"></script>
<script src="views/js/jquery-3.3.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<script>
function reset(){
	$("#username").val("");
	$("#password").val("");
}

function validateDetails(){
	var username = $("#username").val();
	var password = $("#password").val();
	var isSubmit = true;
	if( username === undefined || username === ""){
		isSubmit = false;
		alert("Please provide the username");
	}
	
	if(password === undefined || password === ""){
		isSubmit = false;
		alert("Please provide the password");
	}
	if(isSubmit){
		document.getElementById("loginForm").submit();
		
	}	
}

</script>

<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="panel panel-default" style="width: 452px; background-color: #f2f2f2;">
                <div class="panel-heading" style="background-color:#337ab7; color:white">
                    <strong>Login - LOCATION DATABASE</strong>
                </div>
                <div class="panel-body" style="height:160px !important;">
                    <form method="post" action="login" id="loginForm">
                        <fieldset>

                            <div class="row">
                                <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input class="form-control" placeholder="username" id= "username" name="username" type="text" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock"></i>
                                            </span>
                                            <input class="form-control" placeholder="password"  id="password" name="password" type="password" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <button type="button" class="btn btn-lg btn-primary btn-sm" onclick="validateDetails();">Submit  <i class="fa fa-refresh"></i></button>
                                        <button type="button" class="btn btn-lg btn-primary btn-sm" onclick="reset();">Reset  <i class="fa fa-refresh"></i></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>