<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>DEPT/DEXT</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<link rel="stylesheet" href="views/css/sweetalert.css">
<script src="views/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>

</head>
<%
User user =  (User) pageContext.getAttribute("user");   
%>

<body>
	<div class="container pr-0 pl-0">
		<div class="main">
			<div class="alert alert-danger" role="alert" id="dept-alert-div">Please
				fill in the required fields</div>
			<div class="dpid-main pt-1" id="dept-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
						<label> <b>DEPARTMENT NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="text"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,3,event)"
							style="font-size: 9px" id="dptNo" placeholder=""> <input
							type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="deptId" placeholder=""> <input
							type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="dextId" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn o btn-sm" style="font-size: 9px"
							id="btngo">GO</button>
					</div>

				</div>

			</div>

			<!-- DPTV Show details on GO -->

			<div class="mt-2 ml-2 mr-2" id="dept-view">
				<div class="container ">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="save" value="Save"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel" value="Cancel"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="update" value="Update">
					<% } %>
					</div>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>DEPARTMENT NAME</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm alphabetValidation"
											onkeydown="maxvalidation.call(this,20,event)"
											id="dept-department-name" value=""></td>
									</tr>
									<tr>
										<td>DEPARTMENT PRODUCT CODE</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm alphabetValidation"
											onkeydown="maxvalidation.call(this,1,event)"
											id="dept-product-code" value=""></td>
									</tr>
								</tbody>
							</table>
							<h6>
								<b>MERCHANDISE LEVEL</b>
							</h6>
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>TERM ENTRY LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm numericValidation"
											onkeydown="maxvalidation.call(this,3,event)"
											id="dept-term-entry-lvl" value=""></td>
									</tr>
									<tr>
										<td>SHC REPORT LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-shc-rpt-lvl" value=""></td>
									</tr>
									<tr>
										<td>CORP SUMM LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-corp-summ-lvl" value=""></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>DATE DEPARTMENT ADDED</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-date-department-added" value=""></td>
									</tr>
									<tr>
										<td>DEPARTMENT PHASE OUT DATE</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-deparment-phaseout-date" value=""></td>
									</tr>
								</tbody>
							</table>
							<h6>
								<b>SALES MERCHANDISE LEVEL</b>
							</h6>
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>SALES TERM ENTRY LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-sales-term-entry-lvl" value=""></td>
									</tr>
									<tr>
										<td>SALES SHC REPORT LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-sales-shc-rpt-lvl" value=""></td>
									</tr>
									<tr>
										<td>SALES CORP SUMM LEVEL</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-sales-corp-summ-lvl" value=""></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row justify-content-center">
						<h6>
							<b>SALES MERCHANDISE LEVEL</b>
						</h6>
					</div>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>STORE USE CODE</td>
										<td>
											<form>
												<input disabled="" class="radio" type="radio" name="gender"
													value="Yes" style="vertical-align: middle"
													id="store-use-code-yes"> Yes <input disabled=""
													class="radio" type="radio" name="gender" value="No"
													style="vertical-align: middle" id="store-use-code-no">
												No
											</form>
										</td>
									</tr>
									<tr>
										<td>SHC OPER CODE</td>
										<td>
											<form>
												<input id="shc-oper-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="shc-oper-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>OTHER OPER CODE</td>
										<td>
											<form>
												<input id="other-oper-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="other-oper-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr>
										<td>ASSOC OPER CODE</td>
										<td>
											<form>
												<input id="assoc-oper-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="assoc-oper-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>SPECIALTY CODE</td>
										<td>
											<form>
												<input id="speciality-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="speciality-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr>
										<td>CASH SALES CODE</td>
										<td>
											<form>
												<input id="cash-sales-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="cash-sales-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>PAYROLL CODE</td>
										<td>
											<form>
												<input id="payroll-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="payroll-code-no" disabled="" class="radio" type="radio"
													name="gender" value="No" style="vertical-align: middle">
												No
											</form>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>RETAIL CODE</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-retail-code" value=""></td>
									</tr>
									<tr>
										<td>OWNER CODE</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-owner-code" value=""></td>
									</tr>
									<tr style="vertical-align: middle">
										<td>GROUP CODE</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-group-code" value=""></td>
									</tr>
									<tr>
										<td>OVERHEAD CODE</td>
										<td>
											<form>
												<input id="overhead-code-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="overhead-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr>
										<td>INVENTORIED DEPT</td>
										<td>
											<form>
												<input id="inventoried-dept-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="inventoried-dept-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr>
										<td>ACCOUNT PAYABLE CODE</td>
										<td>
											<form>
												<input id="account-payable-code-yes" disabled=""
													class="radio" type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="account-payable-code-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-6 col-lg-6">
							<table class="table table-bordered table-striped mt-2 m-table"
								style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
								<tbody>
									<tr style="vertical-align: middle">
										<td>REPORTING DEPT</td>
										<td><input type="text" readonly=""
											class="form-control-plaintext form-control-sm"
											id="dept-reporting" value=""></td>
									</tr>
									<tr>
										<td>COST ONLY DEPT</td>
										<td>
											<form>
												<input id="cost-only-dept-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="cost-only-dept-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>ACCOUNT REQUIRED</td>
										<td>
											<form>
												<input id="account-req-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="account-req-no" disabled="" class="radio" type="radio"
													name="gender" value="No" style="vertical-align: middle">
												No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>ASSOCIATE DISCOUNT</td>
										<td>
											<form>
												<input id="associate-discount-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="associate-discount-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>FOOD STAMPS/EBT</td>
										<td>
											<form>
												<input id="food-stamps-ebt-yes" disabled="" class="radio"
													type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="food-stamps-ebt-no" disabled="" class="radio"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
									<tr style="vertical-align: middle">
										<td>PURCHASE ORDER CODE</td>
										<td>
											<form>
												<input id="purchase-order-code-yes" disabled=""
													class="radio rdbtn" type="radio" name="gender" value="Yes"
													style="vertical-align: middle"> Yes <input
													id="purchase-order-code-no" disabled="" class="radio rdbtn"
													type="radio" name="gender" value="No"
													style="vertical-align: middle"> No
											</form>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="views/js/sweetalert.min.js"></script>
	<script>
		$(document)
				.ready(
						function() {

							$(function() {
								$(".datepicker").datepicker({
									minDate : new Date()
								});
							});

							$("#save").hide();
							$("#update").hide();
							$("#cancel").hide();
							$("#dept-view").hide();
							$("#dept-create-view").hide();

							<!--$("#table-row").hide();
							-->

							$("#table_id").DataTable();

							$("#add").click(
									function() {
										$("#dept-view").hide();
										$("#table-row").hide();
										$("#dept-create-view").show();
										$("#save").show();
										$("#cancel").show();
										$("#edit").hide();
										$("#update").hide();
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");
									});

							$(".btnedit").click(
									function() {
										$("#update").show();
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");
										$(".radio").prop("disabled", false);

										$("#cancel").show();
										$("#edit").hide();
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

									});

							$("#cancel")
									.click(
											function() {
												$("#update").hide();
												$("#dept-view").show();
												$("#table-row").hide();
												$("#dept-create-view").hide();
												$('.form-control')
														.addClass(
																'form-control-plaintext')
														.removeClass(
																'form-control');
												$(".form-control-plaintext")
														.prop('readonly', true);
												$(".radio").prop("disabled",
														true);

												$("#save").hide();
												$("#cancel").hide();
												$("#edit").show();

												$('#dptNo')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#dptNo").removeAttr(
														"readonly");

											});

							$("#update")
									.click(
											function() {
												var warningMessage = document
														.getElementById("dept-alert-div")
												var dptvView = document
														.querySelectorAll("#dept-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < dptvView.length; i++) {
													if (dptvView[i].value.length <= 0
															|| dptvView[i].value.length == -1) {
														warningMessage.style.display = "block";
														dptvView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (dptvView[i].value.length > 0) {
														warningMessage.style.display = "none";
														dptvView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/deptdext/updateDeptDext',
																			data : JSON
																					.stringify({
																						"departmentNumber" : $("#dptNo").val(),
																						"deptId" : $(
																								"#deptId")
																								.val(),
																						"dextId" : $(
																								"#dextId")
																								.val(),
																						"departmentName" : $(
																								"#dept-department-name")
																								.val(),
																						"departmentProductCode" : $(
																								"#dept-product-code")
																								.val(),
																						"termEntryLevel" : $(
																								"#dept-term-entry-lvl")
																								.val(),
																						"shcReportLevel" : $(
																								"#dept-shc-rpt-lvl")
																								.val(),
																						"corpSummLevel" : $(
																								"#dept-corp-summ-lvl")
																								.val(),
																						"dateDepartmentAdded" : $(
																								"#dept-date-department-added")
																								.val(),
																						"departmentPhaseOutDate" : $(
																								"#dept-deparment-phaseout-date")
																								.val(),
																						"salesTermEntryLevel" : $(
																								"#dept-sales-term-entry-lvl")
																								.val(),
																						"salesShcReportLevel" : $(
																								"#dept-sales-shc-rpt-lvl")
																								.val(),
																						"salesCorpSummLevel" : $(
																								"#dept-sales-corp-summ-lvl")
																								.val(),
																						"retailCode" : $(
																								"#dept-retail-code")
																								.val(),
																						"ownerCode" : $(
																								"#dept-owner-code")
																								.val(),
																						"groupCode" : $(
																								"#dept-group-code")
																								.val(),
																						"reportingDept" : $(
																								"#dept-reporting")
																								.val(),
																						"storeUseCode" : "1",
																						"shcOperCode" : "1",
																						"otherOperCode" : "1",
																						"assocOperCode" : "1",
																						"specialityCode" : "1",
																						"cashSalesCode" : "1",
																						"payrollCode" : "1",
																						"overHeadCode" : "1",
																						"inventoriedDepartment" : "1",
																						"accountPayableCode" : "1",
																						"costOnlyDept" : "1",
																						"accountRequired" : "1",
																						"associateDiscount" : "1",
																						"foodStampsEbt" : "1",
																						"purchaseOrderCode" : "1"
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error",data.validationMessages,"error");
																				}else{
																					swal("Success","DEPT and DEXT record for Department - "+ $("#dptNo").val() + " is updated successfully","success");
																				}
																			},
																			error : function(
																					data,
																					status) {
																				swal(
																						"Success",
																						"DEPT and DEXT record for Department - "
																								+ $(
																										"#deparment-number")
																										.val()
																								+ " is updated successfully",
																						"success");
																			},
																		});

																$(
																		'.form-control')
																		.addClass(
																				'form-control-plaintext')
																		.removeClass(
																				'form-control');
																$(
																		".form-control-plaintext")
																		.prop(
																				'readonly',
																				true);
																$('input:text').css("border-color", "");
																$(".radio")
																		.prop(
																				"disabled",
																				true);

																$("#save")
																		.hide();
																$("#cancel")
																		.hide();
																$("#edit")
																		.show();
																$("#update")
																		.hide();

																$('#dptNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#dptNo")
																		.removeAttr(
																				"readonly");
															})
												}
												;

											});

							$("#btnDept").click(function() {
								$("#dept-main").show();
							});

							$("#btngo")
									.click(
											function() {
												var warningMessage = document
														.getElementById("dept-alert-div")
												var dptvView = document
														.querySelectorAll("#dept-main input[type=text]");
												var showFlag = false;
												for (var i = 0; i < dptvView.length; i++) {
													if (dptvView[i].value.length <= 0
															|| dptvView[i].value.length == -1) {
														warningMessage.style.display = "block";
														dptvView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (dptvView[i].value.length > 0) {
														warningMessage.style.display = "none";
														dptvView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													$("#dept-view").show();
													$("#table-row").hide();
													$("#dept-create-view")
															.hide();

													$("#save").hide();
													$("#cancel").hide();
													$("#edit").show();
													$("#update").hide();

													var departmentNumber = $(
															"#dptNo").val();
													$
															.ajax({
																url : "data/deptdext/getDeptDext/"+ departmentNumber,
																dataType : 'json',
																success : function(
																		result) {
																	$("#deptId")
																			.val(
																					result.deptId);
																	$("#dextId")
																			.val(
																					result.dextId);
																	$(
																			"#dept-department-name")
																			.val(
																					result.departmentName);
																	$(
																			"#dept-product-code")
																			.val(
																					result.departmentProductCode);
																	$(
																			"#dept-term-entry-lvl")
																			.val(
																					result.termEntryLevel);
																	$(
																			"#dept-shc-rpt-lvl")
																			.val(
																					result.shcReportLevel);
																	$(
																			"#dept-corp-summ-lvl")
																			.val(
																					result.corpSummLevel);
																	$(
																			"#dept-date-department-added")
																			.val(
																					result.dateDepartmentAdded);
																	$(
																			"#dept-deparment-phaseout-date")
																			.val(
																					result.departmentPhaseOutDate);
																	$(
																			"#dept-sales-term-entry-lvl")
																			.val(
																					result.salesTermEntryLevel);
																	$(
																			"#dept-sales-shc-rpt-lvl")
																			.val(
																					result.salesShcReportLevel);
																	$(
																			"#dept-sales-corp-summ-lvl")
																			.val(
																					result.salesCorpSummLevel);
																	$(
																			"#dept-retail-code")
																			.val(
																					result.retailCode);
																	$(
																			"#dept-owner-code")
																			.val(
																					result.ownerCode);
																	$(
																			"#dept-group-code")
																			.val(
																					result.groupCode);
																	$(
																			"#dept-reporting")
																			.val(
																					result.reportingDept);
																	if (result.storeUseCode == "1") {
																		$(
																				"#store-use-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#store-use-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.shcOperCode == "1") {
																		$(
																				"#shc-oper-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#shc-oper-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.otherOperCode == "1") {
																		$(
																				"#other-oper-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#other-oper-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.assocOperCode == "1") {
																		$(
																				"#assoc-oper-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#assoc-oper-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.specialityCode == "1") {
																		$(
																				"#speciality-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#speciality-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.cashSalesCode == "1") {
																		$(
																				"#cash-sales-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#cash-sales-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.payrollCode == "1") {
																		$(
																				"#payroll-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#payroll-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.overHeadCode == "1") {
																		$(
																				"#overhead-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#overhead-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.inventoriedDepartment == "1") {
																		$(
																				"#inventoried-dept-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#inventoried-dept-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.accountPayableCode == "1") {
																		$(
																				"#account-payable-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#account-payable-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.costOnlyDept == "1") {
																		$(
																				"#cost-only-dept-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#cost-only-dept-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.accountRequired == "1") {
																		$(
																				"#account-req-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#account-req-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.associateDiscount == "1") {
																		$(
																				"#associate-discount-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#associate-discount-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.foodStampsEbt == "1") {
																		$(
																				"#food-stamps-ebt-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#food-stamps-ebt-yes")
																				.prop(
																						'checked',
																						true);
																	}
																	if (result.purchaseOrderCode == "1") {
																		$(
																				"#purchase-order-code-no")
																				.prop(
																						'checked',
																						true);
																	} else {
																		$(
																				"#purchase-order-code-yes")
																				.prop(
																						'checked',
																						true);
																	}
																},
																error : function(
																		request,
																		textStatus,
																		errorThrown) {
																	swal(
																			"Error",
																			"No Record found for Deparment Number - "
																					+ departmentNumber,
																			"error");
																}

															})

													;

													$('input:text').attr(
															"readonly", true);
													$('input:text').css(
															"border-color", "");
													$('input:text')
															.removeClass(
																	"form-control")
															.addClass(
																	"form-control-plaintext");
												}
											});
							$("#btnAll").click(function() {

								$("#dept-view").hide();
								$("#table-row").show();
								$("#dept-create-view").hide();
							});

						});

		function saveFunction() {
			swal({
				title : "Are you sure?",
				text : "Your want to add this data",
				type : "info",
				showCancelButton : true,

				confirmButtonText : "Yes",
				closeOnConfirm : false
			}, function() {
				swal("Success", "Your data added successfully", "success");
				$('.form-control').addClass('form-control-plaintext')
						.removeClass('form-control');
				$(".form-control-plaintext").prop('readonly', true);

				$("#save").hide();
				$("#cancel").hide();
				$("#edit").show();

				$('#dptNo').addClass('form-control').removeClass(
						'form-control-plaintext');
				$("#dptNo").removeAttr("readonly");

			});

		}

		function cancelFunction() {

			$("#dept-view").show();
			$("#table-row").hide();
			$("#dept-create-view").hide();
			$('.form-control').addClass('form-control-plaintext').removeClass(
					'form-control');
			$(".form-control-plaintext").prop('readonly', true);

			$("#update").hide();
			$("#save").hide();
			$("#cancel").hide();
			$("#edit").show();

			$('#dptNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#dptNo").removeAttr("readonly");
			$(".rdbtn").prop("disabled", true);

		}
	</script>
</body>

</html>