<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<html>
<%@page import="com.sears.model.User"%>
<head>
<title>BANK</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<link rel="stylesheet" href="views/css/sweetalert.css">
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script src="views/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<script>
	function showLocation() {
		var x = document.getElementById("dpid-main");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	$(function() {
		$(".datepicker").datepicker({
			minDate : new Date()
		});
	});
</script>
</head>

<%
User user =  (User) pageContext.getAttribute("user");   
%>

<body>
	<div class="container pr-0 pl-0">
		<div class="main">
			<div class="alert alert-danger" role="alert" id="bank-alert-div">Please
				fill in the required fields</div>
			<div class="dpid-main pt-1" id="bank-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
						<label> <b>Location Number</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="bank-location-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="bank-occ-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							id="btngo">GO</button>
					</div>

				</div>
			</div>


			<!-- bank Show details on GO -->

			<div class="mt-2 ml-3 mr-3" id="bank-view">
				<div class="container">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2 btnedit" type="button" id="save" value="Save" onclick="saveFunction()"> 
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="cancel" value="Cancel" onclick="cancelFunction()">
						<input class="btn  btn-sm ml-2 mt-2 btnupdate" type="button" id="update" value="Update">
					<%} %>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="location-form">
								<div class="form-group">
									<table class="table table-striped mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">

										<tr style="vertical-align: middle">
											<td>MULTIBANK STORE NO</td>
											<td><input type="text" readonly
												class="form-control-plaintext form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="bank-view-multi-store-no" value=""></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="views/js/sweetalert.min.js"></script>

	<script>
		$(document)
				.ready(
						function() {
							$("#save").hide();
							$("#cancel").hide();
							$("#bank-view").hide();
							<!--$("#dptv-create-view").hide();
							-->

							<!--$("#table-row").hide();
							-->
							<!--$("#table_id").DataTable();
							-->

							$("#btnDPTV").click(function() {
								$("#bank-main").show();
								$("#bank-view").hide();
								<!--$("#dptv-create-view").hide();
								-->
							});

							$("#btngo")
									.click(
											function() {
												var warningMessage = document
														.getElementById("bank-alert-div")
												var bankView = document
														.querySelectorAll("#bank-main input[type=email]");
												var showFlag = false;
												for (var i = 0; i < bankView.length; i++) {
													if (bankView[i].value.length <= 0
															|| bankView[i].value.length == -1) {
														warningMessage.style.display = "block";
														bankView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (bankView[i].value.length > 0) {
														warningMessage.style.display = "none";
														bankView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													$("#bank-view").show();
													$("#save").hide();
													$("#cancel").hide();
													$("#edit").show();
													$("#update").hide();

													var locationNumber = $(
															"#bank-location-number")
															.val();
													$
															.ajax({
																url : "data/bank/getBank/"+ locationNumber,
																dataType : 'json',
																success : function(
																		result) {
																	$(
																			"#bank-location-number")
																			.val(
																					result.locationNumber);
																	$(
																			"#bank-occ-id")
																			.val(
																					result.bankId);
																	$(
																			"#bank-view-multi-store-no")
																			.val(
																					result.multibankStoreNumber);
																},
																error : function(
																		request,
																		textStatus,
																		errorThrown) {
																	swal(
																			"Error",
																			"No Record found for Location Number"
																					+ locationNumber,
																			"error");
																}
															});

													$('input:text').attr(
															"readonly");
													$('input:text')
															.removeClass(
																	"form-control")
															.addClass(
																	"form-control-plaintext");
												}
											});
							$("#btnAll").click(function() {

								$("#bank-view").hide();
							});

							$("#add").click(
									function() {
										$("#bank-view").hide();
										<!--$("#table-row").hide();
										-->

										$("#save").show();
										$("#cancel").show();
										$("#edit").hide();
										$("#update").hide();

										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

									});

							$(".btnedit").click(
									function() {
										$('.form-control-plaintext').addClass(
												'form-control').removeClass(
												'form-control-plaintext');
										$(".form-control").removeAttr(
												"readonly");

										$("#update").show();
										$("#cancel").show();
										$("#edit").hide();

									});

							$("#cancel").click(function() {
								$("#bank-view").show();
								$("#update").hide();
								<!--
								$("#table-row").hide();
								-->
								<!--$("#dptv-create-view").hide();
								-->

							});

							$("#save")
									.click(
											function() {
												swal(
														{
															title : "Are you sure?",
															text : "Your want to update this data",
															type : "info",
															showCancelButton : true,

															confirmButtonText : "Yes",
															closeOnConfirm : false
														},
														function() {
															swal(
																	"Success",
																	"Your data updated successfully",
																	"success");
														});
												$('.form-control')
														.addClass(
																'form-control-plaintext')
														.removeClass(
																'form-control');
												$(".form-control-plaintext")
														.prop('readonly', true);

												$("#save").hide();
												$("#cancel").hide();
												$("#edit").show();
												$("#update").hide();
												$('#locNo')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#locNo").removeAttr(
														"readonly");
												$('#dptNo')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#dptNo").removeAttr(
														"readonly");
												$('#opnDate')
														.addClass(
																'form-control')
														.removeClass(
																'form-control-plaintext');
												$("#opnDate").removeAttr(
														"readonly");

											});

							$("#update")
									.click(
											function() {
												var warningMessage = document
														.getElementById("bank-alert-div")
												var bankView = document
														.querySelectorAll("#bank-view input[type=text]");
												var showFlag = false;
												for (var i = 0; i < bankView.length; i++) {
													if (bankView[i].value.length <= 0
															|| bankView[i].value.length == -1) {
														warningMessage.style.display = "block";
														bankView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (bankView[i].value.length > 0) {
														warningMessage.style.display = "none";
														bankView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													swal(
															{
																title : "Are you sure?",
																text : "Your want to update this data",
																type : "info",
																showCancelButton : true,

																confirmButtonText : "Yes",
																closeOnConfirm : false
															},
															function() {
																$
																		.ajax({
																			dataType : 'json',
																			type : "post",
																			contentType : 'application/json',
																			url : 'data/bank/updateBank',
																			data : JSON
																					.stringify({
																						"bankId" : $("#bank-occ-id").val(),
																						"locationNumber" : $("#bank-location-number").val(),
																						"multibankStoreNumber" : $(
																								"#bank-view-multi-store-no")
																								.val()
																					}),
																			success : function(data,status) {
																				if (data.validationMessages.length > 0) {
																					swal("Error",data.validationMessages,"error");
																				}else{
																					swal("Success","Bank record is updated successfully for location - "+ $("#bank-location-number").val(), "success");
																				}
																			},
																			error : function(
																					data,
																					status) {
																				swal(
																						"Success",
																						"Bank record is not updated successfully for location - "
																								+ $(
																										"#bank-location-number")
																										.val(),
																						"success");
																			}
																		});

																$('input:text')
																		.addClass(
																				'form-control-plaintext')
																		.removeClass(
																				'form-control');
																$('input:text')
																		.attr(
																				'readonly',
																				true);
																$('input:text')
																		.prop(
																				'readonly',
																				true);
																$('input:text')
																		.css(
																				"border-color",
																				"");

																$("#save")
																		.hide();
																$("#cancel")
																		.hide();
																$("#edit")
																		.show();
																$("#update")
																		.show();
																$('#locNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#locNo")
																		.removeAttr(
																				"readonly");
																$('#dptNo')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#dptNo")
																		.removeAttr(
																				"readonly");
																$('#opnDate')
																		.addClass(
																				'form-control')
																		.removeClass(
																				'form-control-plaintext');
																$("#opnDate")
																		.removeAttr(
																				"readonly");
															})
												}
												;

											});

						});

		function cancelFunction() {
			$("#bank-view").show();
			<!--$("#table-row").hide();
			-->
			<!--$("#dptv-create-view").hide();
			-->
			$('.form-control').addClass('form-control-plaintext').removeClass(
					'form-control');
			$(".form-control-plaintext").prop('readonly', true);
			;

			$("#save").hide();
			$("#cancel").hide();
			$("#edit").show();
			$("#update").hide();
			$('#locNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#locNo").removeAttr("readonly");
			$('#dptNo').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#dptNo").removeAttr("readonly");
			$('#opnDate').addClass('form-control').removeClass(
					'form-control-plaintext');
			$("#opnDate").removeAttr("readonly");
		}

		function saveFunction() {
			swal({
				title : "Are you sure?",
				text : "Your want to add this data",
				type : "info",
				showCancelButton : true,

				confirmButtonText : "Yes",
				closeOnConfirm : false
			}, function() {
				swal("Success", "Your data added successfully", "success");
				$('input:text').attr("readonly");
				$('input:text').removeClass("form-control").addClass(
						"form-control-plaintext");

				$("#save").hide();
				$("#cancel").hide();
				$("#edit").show();
				$("#update").show();

			});

		}
	</script>
</body>

</html>