<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<%@page import="com.sears.model.User"%>
<html>
<head>
<title>DPID</title>
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/style.css">
<link rel="stylesheet" href="views/css/jquery-ui.css">
<script src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="views/js/sweetalert.min.js"></script>
<script type="text/javascript" src="views/js/jquery-ui.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<link rel="stylesheet" href="views/css/sweetalert.css">
<script type="text/javascript" src="views/js/Validation.js"></script>

<%
User user =  (User) pageContext.getAttribute("user");   
%>

<script>
	function showLocation() {
		var x = document.getElementById("dpid-main");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	function showDpidView() {
		var x = document.getElementById("dpid-view");
		var y = document.getElementById("dpid-create-view");
		var warningMessage = document.getElementById("alert-div");
		var location_number = document.getElementById("dpid-location-number").value;
		var department_number = document
				.getElementById("dpid-department-number").value;
		var createDPIDNodes = document
				.querySelectorAll("#dpid-main input[type=email]");
		var showFlag = false;
		for (var i = 0; i < createDPIDNodes.length; i++) {
			if (createDPIDNodes[i].value.length <= 0
					|| createDPIDNodes[i].value.length == -1) {
				warningMessage.style.display = "block";
				createDPIDNodes[i].style.borderColor = "crimson";
				showFlag = false;
				break;
			}
			if (createDPIDNodes[i].value.length > 0) {
				warningMessage.style.display = "none";
				createDPIDNodes[i].style.borderColor = "initial";				
				showFlag = true;
			}
		}
		if (showFlag == true) {
			x.style.display = "block";
			y.style.display = "none";
		} else {
			x.style.display = "none";
		}
	}

	function showCreateDpidView() {
		var x = document.getElementById("dpid-create-view");
		var y = document.getElementById("dpid-view");
		if (x.style.display === "none") {
			x.style.display = "block";
			y.style.display = "none";
		} else {
			x.style.display = "none";
		}
		
		$("#dpid-location-number").val("");
		$("#dpid-department-number").val("");
	}
	$(document).ready(
			function() {
				$("#edit").click(
						function() {
							if (this.value == "Edit") {
								$("#update").show();
								$('.editText').removeAttr("readonly");
								$('.editText').removeClass(
										"form-control-plaintext").addClass(
										"form-control");
								this.value = "Cancel";
							} else {
								$("#update").hide();
								$('.editText').attr("readonly", true);
								$('.editText').removeClass("form-control")
										.addClass("form-control-plaintext");
								this.value = "Edit";
							}
						});

				$(function() {
					$(".datepicker").datepicker({
						minDate : new Date()
					});
				});
			})
</script>
</head>

<body>
	<div class="container pr-0 pl-0">
		<div class="main">
			<div class="alert alert-danger" role="alert" id="alert-div">Please
				fill in the required fields</div>
			<div class="dpid-main pt-1" id="dpid-main">
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
						<label> <b>LOCATION NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,5,event)"
							style="font-size: 9px" id="dpid-location-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="dpid-location-id" placeholder="">
					</div>
					<div class="col-lg-auto pl-0 pr-1">
						<button type="button" class="btn  btn-sm" style="font-size: 9px"
							onclick="showDpidView()" id="btnGo">GO</button>
					</div>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1 pr-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" id="btnAll">ALL</button>
					</div>
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
					<div class="col-lg-auto pl-0 pr-0 m-1">
						<label>OR</label>
					</div>
					<div class="col-lg-auto pl-1">
						<button type="button" class="btn  btn-sm m-0"
							style="font-size: 9px" onclick="showCreateDpidView()" id="btnAdd">ADD</button>
					</div>
					<%} %>
				</div>
				<div class="d-flex flex-row" id="search-section">
					<div class="col-lg-2 mt-2 " style="vertical-align: middle;">
						<label> <b>DEPARTMENT NUMBER</b>
						</label>
					</div>
					<div class="col-lg-3">
						<input type="email"
							class="form-control form-control-sm numericValidation"
							onkeydown="maxvalidation.call(this,3,event)"
							style="font-size: 9px" id="dpid-department-number" placeholder="">
						<input type="hidden" class="form-control form-control-sm"
							style="font-size: 9px" id="dpid-department-id" placeholder="">
					</div>

				</div>
			</div>


			<!-- DPID Show details on GO -->

			<div class="mt-2 ml-2 mr-2" id="dpid-view">
				<div class="container">
					<div class="d-flex justify-content-end">
					<%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>						
						<input class="btn  btn-sm ml-2 mt-2" type="button" id="edit" value="Edit"> 
						<input class="btn  btn-sm ml-2 mt-2" type="button" id="update" value="Update">
					<%} %>
						
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr style="vertical-align: middle">
											<td colspan="2" align="right" class="pr-4">DEPT TYPE</td>
											<td colspan="2"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText numericValidation"
												onkeydown="maxvalidation.call(this,1,event)"
												id="dpid-view-dept-type" value=""></td>
										</tr>
										<tr>
											<td colspan="2" align="right" class="pr-4">OPEN DATE</td>
											<td colspan="2"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText"
												id="dpid-view-open-date" value=""></td>
										</tr>
										<tr>

											<td colspan="2" align="right" class="pr-4">CLOSE DATE</td>
											<td colspan="2"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText"
												id="dpid-view-close-date" value=""></td>

										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table table-bordered table-striped mt-2 m-table"
										style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr>
											<td align="center">SPECIAL DIST MGE CODE</td>
											<td align="center"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText numericValidation"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dpid-view-special-dist-mge-code" value="">
											</td>
											<td align="center">SPECIAL REGION MANAGER</td>
											<td align="center"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dpid-view-special-region-manager" value=""></td>
										</tr>

										<tr>
											<td align="center">DEPT IN SQUARE FEET</td>
											<td align="center"><input type="text" readonly
												class="form-control-plaintext form-control-sm editText"
												onkeydown="maxvalidation.call(this,7,event)"
												id="dpid-view-dept-in-sq-feet" value=""></td>
											<td></td>
											<td></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
					<div class="card">
						<div class="card-body mb-2"
							style="color: #337ab7 !important; font-size: 11px">
							<div class="row mb-2">
								<div class="col-lg-2">
									<b>DEPARTMENT TYPES</b>
								</div>
								<div class="col-lg-10">
									<div class="row">
										<div class="col-lg-1">1 =</div>
										<div class="col-lg-2">CONCESSION</div>
										<div class="col-lg-1">2 =</div>
										<div class="col-lg-2">K MART SALES</div>
										<div class="col-lg-1">3 =</div>
										<div class="col-lg-4">K MART MERCHANDISE ONLY</div>
									</div>
									<div class="row">
										<div class="col-lg-1">4 =</div>
										<div class="col-lg-2">K MART OVERHEAD</div>
										<div class="col-lg-1">5 =</div>
										<div class="col-lg-2">VALID FOR SALES AND MERCHANDISE
											REPORTING</div>
										<div class="col-lg-1">6 =</div>
										<div class="col-lg-5">SPECIALTY</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<!-- DPID Show create DPID on Add -->
			<div class="mt-2 ml-3 mr-3" id="dpid-create-view">
				<div class="container">
					<div class="d-flex justify-content-end">
						<input class="btn  btn-sm ml-2 mt-2" type="button" id="save"
							value="Save"> <input class="btn  btn-sm ml-2 mt-2"
							type="button" id="cancel" value="Cancel">
						<!-- <button type="button" class="btn btn-outline-dark btn-sm ml-2 mb-2">Delete</button> -->
					</div>
					<div class="row">
						<div class="col-lg-12">
							<form name="dpid-form">
								<div class="form-group">
									<table class="table mt-2 m-table"
										style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
										<tr style="vertical-align: middle">
											<td colspan="2" align="right" class="pr-4">DEPT TYPE</td>
											<td colspan="2"><input type="text"
												class="form-control form-control-sm lengthValidation numericValidation"
												onkeydown="maxvalidation.call(this,1,event)"
												id="dpid-create-dept-type" value=""></td>
										<tr />
										<tr>
											<td colspan="2" align="right" class="pr-4">OPEN DATE</td>
											<td colspan="2"><input type="text"
												class="form-control form-control-sm datepicker"
												id="dpid-create-open-date" value=""></td>
										</tr>
										<tr>

											<td colspan="2" align="right" class="pr-4">CLOSE DATE</td>
											<td colspan="2"><input type="text"
												class="form-control form-control-sm datepicker"
												id="dpid-create-close-date" value=""></td>

										</tr>
										<tr>
											<td align="center">SPECIAL DIST MGE CODE</td>
											<td align="center"><input type="text"
												class="form-control form-control-sm numericValidation"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dpid-create-special-dist-mge-code" value=""></td>
											<td align="center">SPECIAL REGION MANAGER</td>
											<td align="center"><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,5,event)"
												id="dpid-create-special-region-manager" value=""></td>
										</tr>

										<tr>
											<td align="center">DEPT IN SQUARE FEET</td>
											<td align="center"><input type="text"
												class="form-control form-control-sm"
												onkeydown="maxvalidation.call(this,7,event)"
												id="dpid-create-dept-in-square-feet" value=""></td>
											<td></td>
											<td></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
					<div class="card">
						<div class="card-body mb-2 mt-2 ml-2p-0"
							style="color: #337ab7 !important; font-size: 11px">
							<div class="row mb-2">
								<div class="col-lg-2">
									<b>DEPARTMENT TYPES</b>
								</div>
								<div class="col-lg-10">
									<div class="row">
										<div class="col-lg-auto">1 =</div>
										<div class="col-lg-2">CONCESSION</div>
										<div class="col-lg-auto">2 =</div>
										<div class="col-lg-2">K MART SALES</div>
										<div class="col-lg-auto">3 =</div>
										<div class="col-lg-4">K MART MERCHANDISE ONLY</div>
									</div>
									<div class="row">
										<div class="col-lg-auto">4 =</div>
										<div class="col-lg-2">K MART OVERHEAD</div>
										<div class="col-lg-auto">5 =</div>
										<div class="col-lg-2">VALID FOR SALES AND MERCHANDISE
											REPORTING</div>
										<div class="col-lg-auto">6 =</div>
										<div class="col-lg-5">SPECIALTY</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="table-row mt-2 ml-2 mr-2" id="table-row">
				<div class="table-responsive">
					<table
						class="table table-bordered table-striped table_id mr-0 ml-0"
						id="table_id_dpid"
						style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif;">
						<thead id="tabelHead">
							<tr>
								<th>DEPT-ID</th>
								<th>LOCN-NUMBER</th>
								<th>DEPT-NUMBER</th>
								<th>DEPT-TYPE</th>
								<th>OPEN-DATE</th>
								<th>CLOSE-DATE</th>
								<th>SPECIAL-DIST-MGR-CODE</th>

							</tr>
						</thead>
					</table>
				</div>
			</div>

		</div>
	</div>
</body>
<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>

<script>
	$(document)
			.ready(
					function() {

						$("#dpid-main").show();
						$("#update").hide();
						$("#table-row").hide();
						$("#btnAll")
								.click(
										function() {
											$("#dpid-view").hide();
											$("#table-row").show();
											$("#dpid-create-view").hide();
											var locationNumber = $(
											"#dpid-location-number")
											.val();
											$
													.ajax({
														url : "data/dpid/getAllDepartments/"+locationNumber,
														dataType : 'json',
														success : function(
																result) {
															$("#table_id_dpid")
																	.DataTable(
																			{
																				"destroy" : true,
																				"scrollY" : 320,
																				"scrollX" : true,
																				data : result,
																				"columns" : [
																						{
																							"data" : "departmentId",
																							"render" : function(
																									data,
																									type,
																									row,
																									meta) {
																								data = '<a href="#" onclick="getDataByDeparmentId('
																										+ data
																										+ ')">'
																										+ data
																										+ '</a>';
																								return data;
																							}
																						},
																						{
																							"data" : "locationNumber"
																						},
																						{
																							"data" : "departmentNumber"
																						},
																						{
																							"data" : "departmentType"
																						},
																						{
																							"data" : "departmentOpenDate"
																						},
																						{
																							"data" : "departmentCloseDate"
																						},
																						{
																							"data" : "specialDistrictMGRCode"
																						} ]
																			});
														}
													});
										});
						$("#btnAdd").click(function() {
							$("#dpid-create-view").show();
							$("#table-row").hide();
						});

						$("#btnGo")
								.click(
										function() {
											$("#dpid-create-view").hide();
											$("#table-row").hide();
											$("#dpid-view").show();
											var locationNumber = $(
													"#dpid-location-number")
													.val();
											var departmentNumber = $(
													"#dpid-department-number")
													.val();
											$
													.ajax({
														url : "data/dpid/getDepartment/"+ locationNumber+ "/"+ departmentNumber,
														dataType : 'json',
														success : function(
																result) {
															$(
																	"#dpid-department-id")
																	.val(
																			result.departmentId);
															$(
																	"#dpid-department-number")
																	.val(
																			result.departmentNumber);
															$(
																	"#dpid-location-number")
																	.val(
																			result.locationNumber);
															$(
																	"#dpid-location-id")
																	.val(
																			result.LocationId);
															$(
																	"#dpid-view-dept-type")
																	.val(
																			result.departmentType);
															$(
																	"#dpid-view-open-date")
																	.val(
																			result.departmentOpenDate);
															$(
																	"#dpid-view-close-date")
																	.val(
																			result.departmentCloseDate);
															$(
																	"#dpid-view-special-dist-mge-code")
																	.val(
																			result.specialDistrictMGRCode);
															$(
																	"#dpid-view-special-region-manager")
																	.val(
																			result.specialRegionManager);
															$(
																	"#dpid-view-dept-in-sq-feet")
																	.val(
																			result.deptInSquareFeet);
														},
														error : function(
																request,
																textStatus,
																errorThrown) {
															swal(
																	"Error",
																	"No Record found for Location Number"
																			+ locationNumber,
																	"error");
														}
													});
										});

						$("#save")
								.click(
										function() {
											var warningMessage = document
													.getElementById("alert-div")
											var createdpid = document
													.querySelectorAll("#dpid-create-view input[type=text]");
											var showFlag = false;
											for (var i = 0; i < createdpid.length; i++) {
												if (createdpid[i].value.length <= 0
														|| createdpid[i].value.length == -1) {
													warningMessage.style.display = "block";
													createdpid[i].style.borderColor = "crimson";
													showFlag = false;
													break;
												}
												if (createdpid[i].value.length > 0) {
													warningMessage.style.display = "none";
													createdpid[i].style.borderColor = "initial";
													showFlag = true;
												}

											}
											if (showFlag == true) {
												swal(
														{
															title : "Are you sure?",
															text : "Your want to add this data",
															type : "info",
															showCancelButton : true,
															confirmButtonText : "Yes",
															closeOnConfirm : false
														},
														function() {
															$
																	.ajax({
																		dataType : 'json',
																		type : "post",
																		contentType : 'application/json',
																		url : 'data/dpid/createDpid',
																		data : JSON
																				.stringify({
																					"departmentId" : "",
																					"locationNumber" : $(
																							"#dpid-location-number")
																							.val(),
																					"departmentNumber" : $(
																							"#dpid-department-number")
																							.val(),
																					"departmentType" : $(
																							"#dpid-create-dept-type")
																							.val(),
																					"departmentOpenDate" : $(
																							"#dpid-create-open-date")
																							.val(),
																					"departmentCloseDate" : $(
																							"#dpid-create-close-date")
																							.val(),
																					"specialDistrictMGRCode" : $(
																							"#dpid-create-special-dist-mge-code")
																							.val(),
																					"specialRegionManager" : $(
																							"#dpid-create-special-region-manager")
																							.val(),
																					"deptInSquareFeet" : $(
																							"#dpid-create-dept-in-square-feet")
																							.val()
																				}),
																		success : function(data,status) {
																			if (data.validationMessages.length > 0) {
																				swal("Error",data.validationMessages,"error");
																			} else {
																				swal("Success", "DPID record -" + $("#dpid-department-number").val() + " for location - " + $("#dpid-location-number").val() + " is added successfully.", "success");
																			}
																		},
																		error : function(data,status) {
																			swal("Error","DPID record -" +$("#dpid-department-number").val()	+ " for location - " + $("#dpid-location-number").val() + " is not added successfully.", "error");
																		},
																	});
															$('input:text')
																	.prop(
																			"readonly",
																			true);
															$('input:text')
																	.css(
																			"border-color",
																			"");
															$('input:text')
																	.removeClass(
																			"form-control")
																	.addClass(
																			"form-control-plaintext");
														})
											}
											;
										});

						$("#cancel").click(function() {
							$("#dpid-create-view").hide();
							$("#dpid-view").show();
						});

						$("#update")
								.click(
										function() {
											var warningMessage = document
													.getElementById("alert-div")
											var createdpid = document
													.querySelectorAll("#dpid-view input[type=text]");
											var showFlag = false;
											for (var i = 0; i < createdpid.length; i++) {
												if (createdpid[i].value.length <= 0
														|| createdpid[i].value.length == -1) {
													warningMessage.style.display = "block";
													createdpid[i].style.borderColor = "crimson";
													showFlag = false;
													break;
												}
												if (createdpid[i].value.length > 0) {
													warningMessage.style.display = "none";
													createdpid[i].style.borderColor = "initial";
													showFlag = true;
												}

											}
											if (showFlag == true) {
												swal(
														{
															title : "Are you sure?",
															text : "Your want to update this data",
															type : "info",
															showCancelButton : true,
															confirmButtonText : "Yes",
															closeOnConfirm : false
														},
														function() {
															$
																	.ajax({
																		dataType : 'json',
																		type : "post",
																		contentType : 'application/json',
																		url : 'data/dpid/updateDpid',
																		data : JSON
																				.stringify({
																					"departmentId" : $(
																							"#dpid-department-id")
																							.val(),
																					"LocationId" : $(
																							"#dpid-location-id")
																							.val(),
																					"locationNumber" : $(
																							"#dpid-location-number")
																							.val(),
																					"departmentNumber" : $(
																							"#dpid-department-number")
																							.val(),
																					"departmentType" : $(
																							"#dpid-view-dept-type")
																							.val(),
																					"departmentOpenDate" : $(
																							"#dpid-view-open-date")
																							.val(),
																					"departmentCloseDate" : $(
																							"#dpid-view-close-date")
																							.val(),
																					"specialDistrictMGRCode" : $(
																							"#dpid-view-special-dist-mge-code")
																							.val(),
																					"specialRegionManager" : $(
																							"#dpid-view-special-region-manager")
																							.val(),
																					"deptInSquareFeet" : $(
																							"#dpid-view-dept-in-sq-feet")
																							.val()
																				}),
																		success : function(data, status) {
																			if (data.validationMessages.length > 0) {
																				swal("Error",data.validationMessages,"error");
																			} else {
																				swal("Success", "DPID record -" + $("#dpid-department-number").val() + " for location - " + $("#dpid-location-number").val() + " is updated successfully.", "success");
																			}
																		},
																		error : function(
																				data,
																				status) {
																			swal(
																					"Success",
																					"DPID record -"
																							+ $(
																									"#dpid-department-number")
																									.val()
																							+ " for location - "
																							+ $(
																									"#dpid-location-number")
																									.val()
																							+ " is not updated successfully.",
																					"success");
																		},
																	});
															$("#update").hide();
															$('input:text')
																	.prop(
																			"readonly",
																			true);
															$('input:text')
																	.css(
																			"border-color",
																			"");
															$('input:text')
																	.removeClass(
																			"form-control")
																	.addClass(
																			"form-control-plaintext");
															$("#edit").val(
																	"Edit");
														})
											}
											;
										});
					});

	function getDataByDeparmentId(departmentId) {
		$("#dpid-create-view").hide();
		$("#table-row").hide();
		$("#dpid-view").show();
		$.ajax({
			url : "data/dpid/getDepartment/"+ departmentId,
			dataType : 'json',
			success : function(result) {
				$("#dpid-department-id").val(result.departmentId);
				$("#dpid-department-number").val(result.departmentNumber);
				$("#dpid-location-number").val(result.locationNumber);
				$("#dpid-location-id").val(result.LocationId);
				$("#dpid-view-dept-type").val(result.departmentType);
				$("#dpid-view-open-date").val(result.departmentOpenDate);
				$("#dpid-view-close-date").val(result.departmentCloseDate);
				$("#dpid-view-special-dist-mge-code").val(
						result.specialDistrictMGRCode);
				$("#dpid-view-special-region-manager").val(
						result.specialRegionManager);
				$("#dpid-view-dept-in-sq-feet").val(result.deptInSquareFeet);
			},
			error : function(request, textStatus, errorThrown) {
				swal("Error", "No Record found for DPID - " + departmentId,
						"error");
			}
		});

	}
</script>

</html>