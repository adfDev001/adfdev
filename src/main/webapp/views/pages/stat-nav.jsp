<html>

<head>
<title>STAT</title>
<link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="views/css/bootstrap.min_1.css">
<link rel="stylesheet" href="views/css/font-awesome.min.css">
<script type="text/javascript" src="views/js/sweetalert.min.js"></script>
<link rel="stylesheet" href="views/css/sweetalert.css">
<link rel="stylesheet" href="views/css/style.css">
<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
<script src="views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="views/js/Validation.js"></script>
<script>
	
</script>
</head>

<body>
	<div class="main pr-0 pl-0">
		<div class="alert alert-danger" role="alert" id="stat-alert-div">Please
			fill in the required fields</div>
		<div class="stat-main pt-1" id="stat-main">
			<div class="d-flex flex-row" id="search-section">
				<div class="col-lg-2 mt-auto " style="vertical-align: middle;">
					<label> <b>Location Number</b>
					</label>
				</div>
				<div class="col-lg-3">
					<input type="text"
						class="form-control form-control-sm numericValidation"
						onkeydown="maxvalidation.call(this,5,event)"
						style="font-size: 9px" id="location-number" placeholder="">
					<input type="hidden" class="form-control form-control-sm"
						style="font-size: 9px" id="real-occ-id" placeholder="">
				</div>
				<div class="col-lg-auto pl-0 pr-1">
					<button type="button" class="btn  btn-sm" style="font-size: 9px"
						id="btngo">GO</button>
				</div>

				<div class="col-lg-auto pl-0 pr-0 m-1">
					<label>OR</label>
				</div>
				<div class="col-lg-auto pl-1">
					<button type="button" class="btn  btn-sm m-0"
						style="font-size: 9px" id="btnAll">ALL</button>
				</div>
			</div>
			<div class="d-flex flex-row" id="search-section">
				<div class="col-lg-2 mt-2 " style="vertical-align: middle;">
					<label> <b>Key Fields</b>
					</label>
				</div>
				<div class="col-lg-3">
					<input type="text" class="form-control form-control-sm"
						style="font-size: 9px" id="key-fields" placeholder="">
				</div>

			</div>


		</div>


		<!-- DPTV Show details on GO -->

		<div class="mt-2 ml-3 mr-3" id="stat-view">
			<div class="container">

				<div class="row">
					<div class="col-lg-12">
						<form name="location-form">
							<div class="form-group">
								<table class="table table-striped mt-2 m-table"
									style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
									<tr style="vertical-align: middle">
										<td colspan="2" style="vertical-align: middle">FISCAL
											YEAR</td>
										<td colspan="2" style="vertical-align: middle"><input
											type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-view-fiscal-year" value=""></td>
										<td colspan="2" style="vertical-align: middle">PERIOD</td>
										<td colspan="2" style="vertical-align: middle"><input
											type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-view-period" value=""></td>

									</tr>
								</table>
								<table
									class="table  mt-2 m-table table-bordered text-nowrap mb-1"
									style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle;">
									<tr style="vertical-align: middle">
										<td>STATUS CODES</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_1" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_2" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_3" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_4" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_5" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_6" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_7" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_8" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_9" value=""></td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-code_10" value=""></td>



									</tr>
								</table>
								<div class="row">
									<div class="col-lg-6">
										<table class="table table-striped mt-2 m-table mb-0"
											style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
											<tr>
												<td>CASH :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-cash" value=""></td>
											</tr>
											<tr>
												<td>GEN. ACCT. :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-gen-acct" value=""></td>
											</tr>
											<tr>
												<td>ACCT. PAY. :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-acct-pay" value=""></td>
											</tr>
											<tr>
												<td>PAYROLL :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-payroll" value=""></td>
											</tr>
										</table>
									</div>
								</div>
								<table class="table table-striped mt-2 m-table mb-1"
									style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
									<tr>
										<td colspan="4" align="center">
											<h6>
												<b>CASH INFORMATION</b>
											</h6>
										</td>

										<td colspan="4" align="center" style="font-weight: bold;">
											<h6>
												<b>ACCOUNTS PAYABLE DATA</b>
											</h6>
										</td>
									</tr>

									<tr>
										<td>PREV DAY DATE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-prev-date" value=""></td>
										<td>PREV BALANCE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-prev-balance" value=""></td>
										<td>SP ADJ STATUS CODES</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-spadj-code" value=""></td>
										<td>INVOICE TRAN DATE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-invoice-date" value=""></td>
									</tr>

									<tr>
										<td>WK UNBAL SALES</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-wk-sales" value=""></td>
										<td>WK UNBAL FUNDS</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-wk-funds" value=""></td>
										<td>605/606 TRAN DATE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-tran-date" value=""></td>
										<td>USE TAX PURCHASE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-use-tax-purchase" value=""></td>
									</tr>

									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td>USE TAX PAID</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-tax-paid" value=""></td>
										<td>MERCH. FOR STORE USE</td>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-store-use" value=""></td>
									</tr>

								</table>


								<table
									class="table  mt-2 m-table table-bordered text-nowrap mb-1"
									style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
									<tr>
										<th>GA COMPLETE CODES</th>
										<td><input type="text" readonly
											class="form-control-plaintext form-control-sm"
											id="stat-ga-complete-codes" value=""></td>
									</tr>
								</table>

								<div class="row">
									<div class="col-lg-6">
										<table class="table table-striped mt-2 m-table"
											style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle">
											<tr>
												<td>CASH AND SALES COMPLETE CODES :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-cash-code" value=""></td>
											</tr>
											<tr>
												<td>WEEK COMPLETED FLAG :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-cmplt-flag" value=""></td>
											</tr>
											<tr>
												<td>WEEK PROCESSED FLAG :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-pro-flag" value=""></td>
											</tr>
											<tr>
												<td>WEEK FORCED FLAG :</td>
												<td><input type="text" readonly
													class="form-control-plaintext form-control-sm"
													id="stat-force-flag" value=""></td>
											</tr>
										</table>
									</div>
								</div>



							</div>
						</form>
					</div>
				</div>



			</div>
		</div>
		<div class="table-row mt-2 ml-3 mr-3" id="table-row">
			<div class="table-responsive">
				<table
					class="table table-bordered table-striped table-light my-table table_id mr-0 ml-0 w-auto"
					id="table_id_stat"
					style="font-size: 9px; width: 100%; font-family: Arial, Helvetica, sans-serif">
					<thead id="tabelHead">
						<tr>
							<th>KEY-FIELDS</th>
							<th>FISCAL-YEAR</th>
							<th>PERIOD</th>
							<th>SATUS-CODE</th>
							<th>PREV-BALANCE</th>
							<th>USE-TAX-PURCHASE</th>
							<th>USE-TAX-PAID</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>


	</div>


	<script type="text/javascript" src="views/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="views/js/jquery.dataTables.min.js"></script>
	<script>
		$(document)
				.ready(
						function() {

							$("#stat-view").hide();

							$("#table-row").hide();

							$("#btnSTAT").click(function() {
								$("#stat-main").show();
							});

							$("#btngo")
									.click(
											function() {
												var warningMessage = document
														.getElementById("stat-alert-div")
												var statView = document
														.querySelectorAll("#stat-main input[type=text]");
												var showFlag = false;
												for (var i = 0; i < statView.length; i++) {
													if (statView[i].value.length <= 0
															|| statView[i].value.length == -1) {
														warningMessage.style.display = "block";
														statView[i].style.borderColor = "crimson";
														showFlag = false;
														break;
													}
													if (statView[i].value.length > 0) {
														warningMessage.style.display = "none";
														statView[i].style.borderColor = "initial";
														showFlag = true;
													}

												}
												if (showFlag == true) {
													$("#stat-view").show();
													$("#table-row").hide();

													var locationNumber = $(
															"#location-number")
															.val();
													var keyFields = $(
															"#key-fields")
															.val();

													$
															.ajax({
																url : "data/stat/getStat/"+ locationNumber+ "/"+ keyFields,
																dataType : 'json',
																success : function(
																		result) {
																	$(
																			"#location-number")
																			.val(
																					result.locationNumber);
																	$(
																			"#stat-id")
																			.val(
																					result.statId);
																	$(
																			"#key-fields")
																			.val(
																					result.keyFields);
																	$(
																			"#stat-view-fiscal-year")
																			.val(
																					result.fiscalYear);
																	$(
																			"#stat-view-period")
																			.val(
																					result.period);
																	$(
																			"#stat-code_1")
																			.val(
																					result.statusCode);
																	$(
																			"#stat-code_2")
																			.val(
																					result.statusCode1);
																	$(
																			"#stat-code_3")
																			.val(
																					result.statusCode2);
																	$(
																			"#stat-code_4")
																			.val(
																					result.statusCode3);
																	$(
																			"#stat-code_5")
																			.val(
																					result.statusCode4);
																	$(
																			"#stat-code_6")
																			.val(
																					result.statusCode5);
																	$(
																			"#stat-code_7")
																			.val(
																					result.statusCode6);
																	$(
																			"#stat-code_8")
																			.val(
																					result.statusCode7);
																	$(
																			"#stat-code_9")
																			.val(
																					result.statusCode8);
																	$(
																			"#stat-code_10")
																			.val(
																					result.statusCode9);
																	$(
																			"#stat-cash")
																			.val(
																					result.changeIndicator);
																	$(
																			"#stat-gen-acct")
																			.val(
																					result.vendorDepartment);
																	$(
																			"#stat-acct-pay")
																			.val(
																					result.vendorOpenDate);
																	$(
																			"#stat-payroll")
																			.val(
																					result.vendorCloseDate);
																	$(
																			"#stat-prev-date")
																			.val(
																					result.previousDayDate);
																	$(
																			"#stat-prev-balance")
																			.val(
																					result.previousBalance);
																	$(
																			"#stat-spadj-code")
																			.val(
																					result.combinedFeePercent);
																	$(
																			"#stat-invoice-date")
																			.val(
																					result.invoiceTranDate);
																	$(
																			"#stat-wk-sales")
																			.val(
																					result.weeklyUnBalanceSales);
																	$(
																			"#stat-wk-funds")
																			.val(
																					result.weeklyUnBalanceFunds);
																	$(
																			"#stat-tran-date")
																			.val(
																					result.invoiceTranDate605_606);
																	$(
																			"#stat-use-tax-purchase")
																			.val(
																					result.useTaxPurchase);
																	$(
																			"#stat-tax-paid")
																			.val(
																					result.useTaxPaid);
																	$(
																			"#stat-store-use")
																			.val(
																					result.merchForStoreUse);
																	$(
																			"#stat-cash-code")
																			.val(
																					"");
																	$(
																			"#stat-cmplt-flag")
																			.val(
																					result.weeklyCompletedFlags);
																	$(
																			"#stat-pro-flag")
																			.val(
																					result.weeklyProcessFlag);
																	$(
																			"#stat-force-flag")
																			.val(
																					result.weeklyForceFlag);
																	$(
																			"#stat-ga-complete-codes")
																			.val(
																					result.weeklyFloridaTax);
																}
															});

												}
											});
							$("#btnAll")
									.click(
											function() {

												$("#stat-view").hide();
												$("#table-row").show();
												var locationNumber = $("#location-number").val();
												$
														.ajax({
															url : "data/stat/getAllStat/"+locationNumber,
															dataType : 'json',
															success : function(
																	result) {
																$(
																		"#table_id_stat")
																		.DataTable(
																				{
																					"destroy" : true,
																					"scrollY" : 320,
																					"scrollX" : true,
																					data : result,
																					"columns" : [
																							{
																								"data" : "keyFields",
																								"render" : function(
																										data,
																										type,
																										row,
																										meta) {
																									data = '<a href="#" onclick="getDataByStatId('
																											+ data
																											+ ')">'
																											+ data
																											+ '</a>';
																									return data;

																								}
																							},
																							{
																								"data" : "fiscalYear"
																							},
																							{
																								"data" : "period"
																							},
																							{
																								"data" : "previousDayDate"
																							},
																							{
																								"data" : "previousBalance"
																							},
																							{
																								"data" : "useTaxPurchase"
																							},
																							{
																								"data" : "useTaxPaid"
																							} ]
																				});
															}
														});

											});

						});

		function getDataByStatId(keyFields) {
			$("#stat-view").show();
			$("#table-row").hide();
			var locationNumber = $("#location-number").val();
			$.ajax({
				url : "data/stat/getStat/"+ locationNumber+ "/"+ keyFields,
				dataType : 'json',
				success : function(result) {
					$("#location-number").val(result.locationNumber);
					$("#stat-id").val(result.statId);
					$("#key-fields").val(result.keyFields);
					$("#stat-view-fiscal-year").val(result.fiscalYear);
					$("#stat-view-period").val(result.period);
					$("#stat-code_1").val(result.statusCode);
					$("#stat-code_2").val(result.statusCode1);
					$("#stat-code_3").val(result.statusCode2);
					$("#stat-code_4").val(result.statusCode3);
					$("#stat-code_5").val(result.statusCode4);
					$("#stat-code_6").val(result.statusCode5);
					$("#stat-code_7").val(result.statusCode6);
					$("#stat-code_8").val(result.statusCode7);
					$("#stat-code_9").val(result.statusCode8);
					$("#stat-code_10").val(result.statusCode9);
					$("#stat-cash").val(result.changeIndicator);
					$("#stat-gen-acct").val(result.vendorDepartment);
					$("#stat-acct-pay").val(result.vendorOpenDate);
					$("#stat-payroll").val(result.vendorCloseDate);
					$("#stat-prev-date").val(result.previousDayDate);
					$("#stat-prev-balance").val(result.previousBalance);
					$("#stat-spadj-code").val(result.combinedFeePercent);
					$("#stat-invoice-date").val(result.invoiceTranDate);
					$("#stat-wk-sales").val(result.weeklyUnBalanceSales);
					$("#stat-wk-funds").val(result.weeklyUnBalanceFunds);
					$("#stat-tran-date").val(result.invoiceTranDate605_606);
					$("#stat-use-tax-purchase").val(result.useTaxPurchase);
					$("#stat-tax-paid").val(result.useTaxPaid);
					$("#stat-store-use").val(result.merchForStoreUse);
					$("#stat-cash-code").val("");
					$("#stat-cmplt-flag").val(result.weeklyCompletedFlags);
					$("#stat-pro-flag").val(result.weeklyProcessFlag);
					$("#stat-force-flag").val(result.weeklyForceFlag);
					$("#stat-ga-complete-codes").val(result.weeklyFloridaTax);
				}
			});
		}
	</script>
</body>

</html>