$(document).ready(function () {
	
	   /* var max=4;
    $('.lengthValidation').keydown(function(event){
        if (event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && 
            event.keyCode != 38 && event.keyCode != 39 && event.keyCode != 40 &&
            $(this).val().length >= max) {
            event.preventDefault();
            swal("Maximum Limit Reached...!")
        }
    })*/

    $('.alphabetValidation').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
            e.preventDefault();
            swal("Please Enter Alphabets only...!")
            return false;
        }
    });

    $(".numericValidation").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190 ||
            event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 18 || event.keyCode == 13 || event.keyCode == 17 ||event.keyCode == 16 || event.keyCode == 20 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {  
            return;
        }
        else {    
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)
                && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
                swal("Please Enter Numeric Values only...!")
            }
        }
    });
    

    

   

});

function maxvalidation(len,event){
	
	var max=len;
	var text;
	if (event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 13 && event.keyCode != 18 &&
			event.keyCode != 17 && event.keyCode != 16 && event.keyCode != 20 && event.keyCode != 38 && event.keyCode != 39 && event.keyCode != 9 && event.keyCode != 40 &&
            $(this).val().length >= max) {
		
            event.preventDefault();
           
//            text = "Input not valid"; 
            swal("Maximum "+len+" character allowed")
            $(this).val($(this).val().substr(0,max)); 
        }else{
//        	text="";
        	
        }
	
//		document.getElementById("error").innerHTML = text;
}

