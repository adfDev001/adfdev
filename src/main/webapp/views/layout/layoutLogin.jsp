<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
  <head>
  <link rel="stylesheet" href="<%= getServletConfig().getServletContext().getContextPath() %>/views/css/bootstrap.min_1.css">
  <link rel="stylesheet" href="<%= getServletConfig().getServletContext().getContextPath() %>/views/css/font-awesome.min.css">
    <title><tiles:getAsString name="title"/></title>
  </head>
  <body>
        <table>
      <tr>
        <td>
          <tiles:insertAttribute name="body" />
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <tiles:insertAttribute name="footer" />
        </td>
      </tr>
    </table>
  </body>
</html>