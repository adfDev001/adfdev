<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
  <head>
  <link rel="stylesheet" href="/views/css/bootstrap.min_1.css">
  <link rel="stylesheet" href="/views/css/style.css">
    <title><tiles:getAsString name="title"/></title>
  </head>
  <body>
  <div class="container">
  <table>
      <tr>
        <td>
          <tiles:insertAttribute name="header" />
        </td>
      </tr>
      <tr>
        <td>
          <tiles:insertAttribute name="body" />
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <tiles:insertAttribute name="footer" />
        </td>
      </tr>
    </table>
  </div>
        
  </body>
</html>