<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />
<!DOCTYPE html>
<%@page import="com.sears.model.User"%>
<html>
    <head>
        <title>
            DEPT/DEXT
        </title>
        <link rel="stylesheet" href="views/css/font-awesome.min.css">
        <link rel="stylesheet" href="views/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="views/css/bootstrap.min_1.css">
        <link rel="stylesheet" href="views/css/style.css">
        <link rel="stylesheet" href="views/css/sweetalert.css">
        <script src="views/js/bootstrap.min.js"></script>

    </head>
<body>

<%
User user =  (User) pageContext.getAttribute("user");   
%>
        <!-- Header -->
        <nav class="navbar navbar-dark justify-content-center pt-0 pb-0 mb-2" id="top-nav">
            <span class="navbar-brand mb-0 h1" id="headerSpan">
                <h3>LOCATION DATABASE</h3>
            </span>
        </nav>
        <!-- Nav Bar  -->
        <nav class="d-flex flex-row justify-content-around navbar navbar-expand-lg navbar-light text-light pt-0 pb-0" id="menu-bar">
            <div class="justify-content-between font-weight-bold" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getLocationSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getLocationSegmentAccess().trim()))) {
                    %>
	                    <li class="nav-item">
	                        <a class="nav-link" href="locationpage" id="btnLOCN" style="font-size:11px;">LOCN/LEXT</a>
	                    </li>
                    <% } %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getDpidSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getDpidSegmentAccess().trim()))) {
                    %>
	                    <li class="nav-item">
	                        <a class="nav-link" href="dpidpage" id="btnDPID" style="font-size:11px;">DPID</a>
	                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getDptvSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getDptvSegmentAccess().trim()))) {
                    %>
	                    <li class="nav-item">
	                        <a class="nav-link" href="dptvpage" id="btnDPTV" style="font-size:11px;">DPTV</a>
	                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getDeptDextSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getDeptDextSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="deptpage" btn="btnDEPT" style="font-size:11px;">DEPT/DEXT</a>
                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getStatSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getStatSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="statpage" id="btnSTAT" style="font-size:11px;">STAT</a>
                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getRealSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getRealSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="realpage" id="btnREAL" style="font-size:11px;">REAL</a>
                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getBankSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getBankSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="bankpage" id="btnBANK" style="font-size:11px;">BANK</a>
                    </li>
                    <%} %>
                    <%if (user != null && ("FULL".equalsIgnoreCase(user.getKinsSegmentAccess().trim()) || "BROWSE".equalsIgnoreCase(user.getKinsSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="kinspage" id="btnKINS" style="font-size:11px;">KINS</a>
                    </li>
                    <%} %>
                    <%if (user != null && ("YES".equalsIgnoreCase(user.getAdminSegmentAccess().trim()))) {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="adminpage" btn="btnAdmin" style="font-size:11px;">ADMIN</a>
                    </li>
                    <%} %>
                    <li class="nav-item">
                        <a class="nav-link" href="logout" style="font-size:11px;">Logout
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
</body>
</html>