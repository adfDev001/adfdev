package com.sears.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtility {

	public static void main(String[] args) throws ParseException {
		String date = "05/24/2018";
		Integer output = getJulian7FromDate(date);
		System.out.println(output);
		String returnDate = getDateFromJulian7("52418");
		System.out.println("Return Date " + returnDate);
	}

	public static String getDateFromJulian7(String julianDate) throws ParseException {
		return new SimpleDateFormat("yyyyD").parse(julianDate).toString();
	}

	
	public static Integer getJulian7FromDate(String dateStr) {
		Date date = new Date(dateStr);
		StringBuilder sb = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return Integer.valueOf(sb.append(String.valueOf(cal.get(Calendar.YEAR))).append(String.format("%03d", cal.get(Calendar.DAY_OF_YEAR))).toString());
	}

}
