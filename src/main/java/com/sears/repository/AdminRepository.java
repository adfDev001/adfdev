package com.sears.repository;

import java.util.List;

import com.sears.model.User;

public interface AdminRepository {

	User getUser(String userId);
	User getUserById(Long userId);
	List<User> getAllUsers(Integer offset, Integer limit);
	User createUser(User user);
	User updateUser(User user);
	User deleteUser(User user);
}
