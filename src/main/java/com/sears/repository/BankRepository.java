package com.sears.repository;

import com.sears.model.Bank;

public interface BankRepository {

	Bank getBank(String locationNumber, Long bankId);

	Bank updateBank(Bank bank);
}
