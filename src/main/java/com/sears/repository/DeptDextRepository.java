package com.sears.repository;

import com.sears.model.DeptDext;

public interface DeptDextRepository {

	DeptDext getDeptDext(Integer departmentNumber);
	DeptDext updateDeptDext(DeptDext deptDext);
}
