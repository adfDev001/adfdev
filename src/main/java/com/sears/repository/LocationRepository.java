package com.sears.repository;

import java.util.List;

import com.sears.model.Location;
import com.sears.model.LocationVO;

public interface LocationRepository {
	
	List<LocationVO> getAllLocations(Integer offet, Integer limit);
	Location getLocation(Long occId);
	Location getLocationByLocation(Long locationNumber);
	Location createLocation(Location location);
	Location updateLocation(Location location);
}
