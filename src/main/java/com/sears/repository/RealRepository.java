package com.sears.repository;

import com.sears.model.Real;

public interface RealRepository {
	
	Real getReal(String locationNumber, String realId);
	
	Real createReal(Real real);
	
	Real updateReal(Real real);

}
