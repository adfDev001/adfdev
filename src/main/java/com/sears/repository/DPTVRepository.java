package com.sears.repository;

import java.util.List;

import com.sears.model.Dptv;

public interface DPTVRepository {

	Dptv getDPTV(String locationNumber, String departmentNumber, String openDate);
	Dptv getDPTV(Long dptvId);
	List<Dptv> getAllDPTV(Integer locationNumber, Integer departmentNumber);
	Dptv createDPTV(Dptv dptv);
	Dptv updateDPTV(Dptv dptv);

}
