package com.sears.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.KinsMapper;
import com.sears.model.Kins;
import com.sears.repository.KinsRepository;

@Repository
public class KinsRepositoryImpl implements KinsRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(KinsRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String GET_KINS= "select LOCN.OCC_ID, LOCN.LOCN_NUMBER, LOCN.LOCN_TERMINAL_CODE, LOCN.LOCN_TERMINAL_DATE \r\n" + 
			"from PLOCN2_LOCN LOCN where LOCN.LOCN_NUMBER = ?"; 
	
	private static final String UPDATE_KINS = "\r\n" + 
			"UPDATE PLOCN2_LOCN LOCN SET LOCN.LOCN_TERMINAL_CODE = ? , LOCN.LOCN_TERMINAL_DATE = ? \r\n" + 
			"WHERE LOCN.OCC_ID = ? AND LOCN.LOCN_NUMBER = ?"; 
	
	@Override
	public Kins getKins(String locationNumber) {
		return jdbcTemplate.queryForObject(GET_KINS, new KinsMapper(), locationNumber);
	}


	@Override
	public Kins updateKins(Kins kins) {
		Kins kinsUpdated = null;
		int status =0;
		try {
			status = jdbcTemplate.update(UPDATE_KINS, kins.getTerminalCode(), kins.getTerminalDate(), kins.getLocationId(), kins.getLocationNumber());
		}catch(Exception exp) {
			exp.printStackTrace();
			kinsUpdated = new Kins();
			kinsUpdated.setValidationMessages("Error Messages - " + exp.getCause());
		}
		if(status > 0) {
			LOGGER.debug("Kins record is updated successfully !!!");
			kinsUpdated = this.getKins(kins.getLocationNumber());
		}else {
			LOGGER.debug("Kins record is not updated!!!");
		}
		return kinsUpdated;
	}

}
