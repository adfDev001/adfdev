package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.DPTVMapper;
import com.sears.model.DPID;
import com.sears.model.Dptv;
import com.sears.repository.DPIDRepository;
import com.sears.repository.DPTVRepository;

@Repository
public class DPTVRepositoryImpl implements DPTVRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(DPTVRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private DPIDRepository dpidRepository;
	
	private static final String GET_ALL_DPTV = "select \r\n" + 
			"LOC.OCC_ID AS LOCID, LOC.LOCN_NUMBER,\r\n" + 
			"DPID.OCC_ID AS DPID, \r\n" + 
			"DPID.DPID_SP_DEPARTMENT_NUMBER, \r\n" + 
			"DPTV.OCC_ID AS DPTVID,\r\n" + 
			"DPTV.DPTV_VENDOR_OPEN_DATE, \r\n" + 
			"DPTV.DPTV_VENDOR_CLOSE_DATE\r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_DPID DPID, PLOCN2_DPTV DPTV\r\n" + 
			"where LOC.OCC_ID = DPID.PARENT_ID \r\n" + 
			"AND DPID.OCC_ID = DPTV.PARENT_ID"
			+ " AND LOC.LOCN_NUMBER=? "
			+ " AND DPID.DPID_SP_DEPARTMENT_NUMBER = ?";
	
	private static final String GET_DPTV_BY_LOC_DPID = "select\r\n" + 
			"LOCN.OCC_ID LOC_ID, \r\n" + 
			"LOCN.LOCN_NUMBER, \r\n" + 
			"DPID.OCC_ID DPID, DPID.DPID_SP_DEPARTMENT_NUMBER, DPID.DPID_OPEN_DATE,\r\n" + 
			"DPTV.OCC_ID DPTVID,\r\n" + 
			"DPTV.DPTV_CHANGE_INDICATOR, \r\n" + 
			"DPTV.DPTV_VENDOR_OPEN_DATE,\r\n" + 
			"DPTV.DPTV_VENDOR_CLOSE_DATE, \r\n" + 
			"DPTV.DPTV_VENDOR_DUNS_NUMBER,\r\n" + 
			"DPTV.DPTV_VENDOR_ACCT_NUMBER,\r\n" + 
			"DPTV.DPTV_COMBINED_FEE_PERCNT,\r\n" + 
			"DPTV.DPTV_SPLIT_FEE_PERCENT,\r\n" + 
			"DPTV.DPTV_PERCENT_OWNED_BY_KMART,\r\n" + 
			"DPTV.DPTV_ADVERTNG_FEE_PERCNT,\r\n" + 
			"DPTV.DPTV_WEEKLY_FLORIDA_TAX\r\n" + 
			"from PLOCN2_LOCN LOCN, PLOCN2_DPID DPID, PLOCN2_DPTV DPTV\r\n" + 
			"WHERE LOCN.OCC_ID = DPID.PARENT_ID\r\n" + 
			"AND DPID.OCC_ID = DPTV.PARENT_ID\r\n" + 
			"AND  LOCN.LOCN_NUMBER = ?\r\n" + 
			"AND  DPID.DPID_SP_DEPARTMENT_NUMBER = ? and DPTV.DPTV_VENDOR_OPEN_DATE = ?";
	
	private static final String GET_DPTV_BY_ID = "select\r\n" + 
			"LOCN.OCC_ID LOC_ID, \r\n" + 
			"LOCN.LOCN_NUMBER, \r\n" + 
			"DPID.OCC_ID DPID, DPID.DPID_SP_DEPARTMENT_NUMBER, \r\n" + 
			"DPTV.OCC_ID DPTVID,\r\n" + 
			"DPTV.DPTV_CHANGE_INDICATOR, \r\n" + 
			"DPTV.DPTV_VENDOR_OPEN_DATE,\r\n" + 
			"DPTV.DPTV_VENDOR_CLOSE_DATE, \r\n" + 
			"DPTV.DPTV_VENDOR_DUNS_NUMBER,\r\n" + 
			"DPTV.DPTV_VENDOR_ACCT_NUMBER,\r\n" + 
			"DPTV.DPTV_COMBINED_FEE_PERCNT,\r\n" + 
			"DPTV.DPTV_SPLIT_FEE_PERCENT,\r\n" + 
			"DPTV.DPTV_PERCENT_OWNED_BY_KMART,\r\n" + 
			"DPTV.DPTV_ADVERTNG_FEE_PERCNT,\r\n" + 
			"DPTV.DPTV_WEEKLY_FLORIDA_TAX\r\n" + 
			"from PLOCN2_LOCN LOCN, PLOCN2_DPID DPID, PLOCN2_DPTV DPTV\r\n" + 
			"WHERE LOCN.OCC_ID = DPID.PARENT_ID\r\n" + 
			"AND DPID.OCC_ID = DPTV.PARENT_ID\r\n" + 
			"AND  DPTV.OCC_ID = ?\r\n"; 
	
	private static final String GET_MAX_DPTV_ID = "select max(occ_id) as DPTV_ID from PLOCN2_DPTV";
	
	private static final String INSERT_DPTV = "INSERT INTO PLOCN2_DPTV \r\n" + 
			"(\r\n" + 
			"OCC_ID, \r\n" +
			"DPTV_VENDOR_OPEN_DATE, \r\n" +
			"DPTV_VENDOR_CLOSE_DATE, \r\n" +
			"DPTV_VENDOR_DUNS_NUMBER, \r\n" +
			"DPTV_COMBINED_FEE_PERCNT, \r\n" +
			"DPTV_ADVERTNG_FEE_PERCNT, \r\n" +
			"DPTV_WEEKLY_FLORIDA_TAX, \r\n" +
			"DPTV_VENDOR_ACCT_NUMBER, \r\n" +
			"DPTV_SPLIT_FEE_PERCENT, \r\n" +
			"DPTV_PERCENT_OWNED_BY_KMART, \r\n" +
			"DPTV_FILLER1, \r\n" +
			"DPTV_CHANGE_INDICATOR, \r\n" +
			"PARENT_ID, \r\n" +
			"PARENT_KEY \r\n" +
			")\r\n" + 
			"VALUES\r\n" + 
			"(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String UPDATE_DPTV = "UPDATE PLOCN2_DPTV SET \r\n" + 
			"DPTV_VENDOR_OPEN_DATE = ?, \r\n" +
			"DPTV_VENDOR_CLOSE_DATE = ?, \r\n" +
			"DPTV_VENDOR_DUNS_NUMBER = ?, \r\n" +
			"DPTV_COMBINED_FEE_PERCNT = ?, \r\n" +
			"DPTV_ADVERTNG_FEE_PERCNT = ?, \r\n" +
			"DPTV_WEEKLY_FLORIDA_TAX = ?, \r\n" +
			"DPTV_VENDOR_ACCT_NUMBER = ?, \r\n" +
			"DPTV_SPLIT_FEE_PERCENT = ?, \r\n" +
			"DPTV_PERCENT_OWNED_BY_KMART = ?, \r\n" +
			"DPTV_CHANGE_INDICATOR = ? \r\n" +
			"WHERE OCC_ID = ? ";

	
	@Override
	public Dptv getDPTV(String locationNumber, String departmentNumber, String openDate) {
		return jdbcTemplate.queryForObject(GET_DPTV_BY_LOC_DPID, new DPTVMapper(), locationNumber, departmentNumber, openDate);
	}

	@Override
	public Dptv getDPTV(Long dptvId) {
		LOGGER.debug("dptvId:::::::::::" + dptvId);
		return jdbcTemplate.queryForObject(GET_DPTV_BY_ID, new DPTVMapper(), dptvId);
	}

	@Override
	public List<Dptv> getAllDPTV(Integer locationNumber, Integer departmentNumber) {
		List<Dptv> dptvs = new ArrayList<Dptv>();
		jdbcTemplate.setMaxRows(1000);
		List<Map<String, Object>> retMap = jdbcTemplate.queryForList(GET_ALL_DPTV, locationNumber, departmentNumber);
		if(retMap != null && !retMap.isEmpty()) {
			for (Map<String, Object> map : retMap) {
				Dptv dptv = new Dptv();
				dptv.setLocationId(Long.valueOf(String.valueOf(map.get("LOCID"))));
				dptv.setLocationNumber(String.valueOf(map.get("LOCN_NUMBER")));
				dptv.setDpid(Long.valueOf(String.valueOf(map.get("DPID"))));
				dptv.setDepartmentNumber(String.valueOf(map.get("DPID_SP_DEPARTMENT_NUMBER")));
				dptv.setDptvId(Long.valueOf(String.valueOf(map.get("DPTVID"))));
				dptv.setVendorOpenDate(Integer.valueOf(String.valueOf(map.get("DPTV_VENDOR_OPEN_DATE"))));
				dptv.setVendorCloseDate(Integer.valueOf(String.valueOf(map.get("DPTV_VENDOR_CLOSE_DATE"))));
				dptv.setKeyControl(dptv.getVendorOpenDate()+dptv.getDepartmentNumber());
				dptvs.add(dptv);
			}
		}
		return dptvs;
	}
	
	@Override
	public Dptv createDPTV(Dptv dptv) {
		Dptv dptvCreated = null;
		int status =0;
		Map<String, Object> retMap = jdbcTemplate.queryForMap(GET_MAX_DPTV_ID);
		BigDecimal nextDPId = (BigDecimal) retMap.get("DPTV_ID");
		dptv.setDptvId(nextDPId.longValue()+1);
		DPID dpid = dpidRepository.getDepartment(dptv.getLocationNumber(), dptv.getDepartmentNumber());
		try {
			status = jdbcTemplate.update(INSERT_DPTV, 
					dptv.getDptvId(),
					dpid.getDepartmentOpenDate(),
					dptv.getVendorCloseDate(),
					dptv.getVendorDunsNumber(),
					dptv.getCombinedFeePercent(),
					dptv.getAdvertisingFeePercentage(),
					dptv.getWeeklyFloridaTax(),
					dptv.getVendorAccountNumber(),
					dptv.getSplitFeePercent(),
					dptv.getPercentageOwnedByKMART(),
					null,
					null,
					dpid.getDepartmentId(),
					null
					);
		}catch(DataIntegrityViolationException e) {
			e.printStackTrace();
			dptvCreated = new Dptv();
			dptvCreated.setValidationMessages("" + e.getCause());
		}catch(Exception exp) {
			dptvCreated = new Dptv();
			dptvCreated.setValidationMessages("" + exp.getCause());
		}
		if(status > 0) {
			LOGGER.debug("DPID record is inserted successfully.");
			dptvCreated = this.getDPTV(dptv.getDptvId());
		}
		return dptvCreated;
	}

	@Override
	public Dptv updateDPTV(Dptv dptv) {
		Dptv dptvUpdated = null;
		int status=0;
		try {
			status = jdbcTemplate.update(UPDATE_DPTV,
					dptv.getVendorOpenDate(),
					dptv.getVendorCloseDate(),
					dptv.getVendorDunsNumber(),
					dptv.getCombinedFeePercent(),
					dptv.getAdvertisingFeePercentage(),
					dptv.getWeeklyFloridaTax(),
					dptv.getVendorAccountNumber(),
					dptv.getSplitFeePercent(),
					dptv.getPercentageOwnedByKMART(),
					dptv.getChangeIndicator(),
					dptv.getDptvId()
					);
		}catch(DataIntegrityViolationException e) {
			e.printStackTrace();
			dptvUpdated = new Dptv();
			dptvUpdated.setValidationMessages("" + e.getCause());
		}catch(Exception exp) {
			dptvUpdated = new Dptv();
			dptvUpdated.setValidationMessages("" + exp.getCause());
		}
		if(status > 0) {
			LOGGER.debug("DPID record is updated successfully.");
			dptvUpdated = this.getDPTV(dptv.getDptvId());
		}
		return dptvUpdated;
	}
	
}
