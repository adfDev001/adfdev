package com.sears.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.DeptDextMapper;
import com.sears.model.DeptDext;
import com.sears.repository.DeptDextRepository;

@Repository
public class DeptDextRepositoryImpl implements DeptDextRepository{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeptDextRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate; 
	
	private static final String GET_DEPT_DEXT= "SELECT \r\n" + 
			"DEPT.OCC_ID DEPTID,\r\n" + 
			"DEXT.OCC_ID DEXTID,\r\n" + 
			"DEPT.DEPT_DEPARTMENT_NUMBER,\r\n" + 
			"DEPT.DEPT_DEPARTMENT_NAME,\r\n" + 
			"DEPT.DEPT_PRODUCT_CODE,\r\n" + 
			"DEPT.DEPT_DATE_ADDED_TO_DEPT,\r\n" + 
			"DEPT.DEPT_PHASE_OUT_DATE,\r\n" + 
			"DEPT.DEPT_TERM_ENTRY_LVL,\r\n" + 
			"DEPT.DEPT_KIH_RPT_LVL,\r\n" + 
			"DEPT.DEPT_CORP_SUMM_LVL,\r\n" + 
			"DEPT.DEPT_SALES_TERM_ENTRY_LVL,\r\n" + 
			"DEPT.DEPT_SALES_KIH_RPT_LVL,\r\n" + 
			"DEPT.DEPT_SALES_CORP_SUMM_LVL,\r\n" + 
			"DEPT.DEPT_VAL_STORE_USE_CODE,\r\n" + 
			"DEPT.DEPT_VAL_SSK_OPER_CODE,\r\n" + 
			"DEPT.DEPT_VAL_OTHER_OPER_CODE,\r\n" + 
			"DEPT.DEPT_VAL_ASSOC_OPER_CODE,\r\n" + 
			"DEPT.DEPT_VAL_SPECIALTY_CODE,\r\n" + 
			"DEPT.DEPT_VAL_CASH_SALES_CODE,\r\n" + 
			"DEPT.DEPT_VAL_PAYROLL_CODE,\r\n" + 
			"DEPT.DEPT_RETAIL_CODE,\r\n" + 
			"DEPT.DEPT_OWNER_CODE,\r\n" + 
			"DEPT.DEPT_VAL_OVERHEAD_CODE,\r\n" + 
			"DEPT.DEPT_INVENTORIED_CODE,\r\n" + 
			"DEPT.DEPT_VAL_AP_CODE,\r\n" + 
			"DEPT.DEPT_COST_ONLY_CODE,\r\n" + 
			"DEPT.DEPT_ACCT_REQUIRED_CODE,\r\n" + 
			"DEPT.DEPT_VAL_EMPLOYEE_DISCOUNT,\r\n" + 
			"DEPT.DEPT_VAL_FOOD_STAMPS,\r\n" + 
			"DEPT.DEPT_VAL_PUR_ORDER_CODE,\r\n" + 
			"DEXT.DEXT_GROUP_CODE,\r\n" + 
			"DEXT.DEXT_REPORTING_DEPT\r\n" + 
			"FROM PDEPT2_DEPT DEPT, PDEPT2_DEXT DEXT\r\n" + 
			"WHERE DEPT.OCC_ID = DEXT.PARENT_ID\r\n" + 
			"AND DEPT.DEPT_DEPARTMENT_NUMBER = ? \r\n";
	
	private static final String DEPT_UPDATE = "UPDATE PDEPT2_DEPT DEPT \r\n" + 
			"SET\r\n" + 
			"DEPT.DEPT_DEPARTMENT_NAME= ?,\r\n" + 
			"DEPT.DEPT_PRODUCT_CODE= ?,\r\n" + 
			"DEPT.DEPT_DATE_ADDED_TO_DEPT = ?,\r\n" + 
			"DEPT.DEPT_PHASE_OUT_DATE = ?,\r\n" + 
			"DEPT.DEPT_TERM_ENTRY_LVL = ?,\r\n" + 
			"DEPT.DEPT_KIH_RPT_LVL = ?,\r\n" + 
			"DEPT.DEPT_CORP_SUMM_LVL = ?,\r\n" + 
			"DEPT.DEPT_SALES_TERM_ENTRY_LVL= ?,\r\n" + 
			"DEPT.DEPT_SALES_KIH_RPT_LVL= ?,\r\n" + 
			"DEPT.DEPT_SALES_CORP_SUMM_LVL= ?,\r\n" + 
			"DEPT.DEPT_VAL_STORE_USE_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_SSK_OPER_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_OTHER_OPER_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_ASSOC_OPER_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_SPECIALTY_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_CASH_SALES_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_PAYROLL_CODE= ?,\r\n" + 
			"DEPT.DEPT_RETAIL_CODE= ?,\r\n" + 
			"DEPT.DEPT_OWNER_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_OVERHEAD_CODE= ?,\r\n" + 
			"DEPT.DEPT_INVENTORIED_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_AP_CODE= ?,\r\n" + 
			"DEPT.DEPT_COST_ONLY_CODE= ?,\r\n" + 
			"DEPT.DEPT_ACCT_REQUIRED_CODE= ?,\r\n" + 
			"DEPT.DEPT_VAL_EMPLOYEE_DISCOUNT= ?,\r\n" + 
			"DEPT.DEPT_VAL_FOOD_STAMPS= ?,\r\n" + 
			"DEPT.DEPT_VAL_PUR_ORDER_CODE= ?\r\n" + 
			"WHERE DEPT.OCC_ID = ? ";
	
	private static final String UPDATE_DEXT = "UPDATE PDEPT2_DEXT DEXT\r\n" + 
			"SET \r\n" + 
			"DEXT.DEXT_GROUP_CODE = ?,\r\n" + 
			"DEXT.DEXT_REPORTING_DEPT = ?\r\n" + 
			"WHERE DEXT.OCC_ID = ?\r\n";
	
	@Override
	public DeptDext getDeptDext(Integer departmentNumber) {
		return jdbcTemplate.queryForObject(GET_DEPT_DEXT, new DeptDextMapper(), departmentNumber);
	}


	@Override
	public DeptDext updateDeptDext(DeptDext deptDext) {
		DeptDext deptDextUpdated = null;
		int deptStatus = 0;
		int dextStatus = 0;
		
		LOGGER.debug("DEPT ID " + deptDext.getDeptId());
		LOGGER.debug("DEXT ID " + deptDext.getDextId());
		LOGGER.debug("DEXT ID " + deptDext.getDepartmentNumber());
		
		
		try {
			deptStatus = jdbcTemplate.update(DEPT_UPDATE, 
					deptDext.getDepartmentName(),
					deptDext.getDepartmentProductCode(),
					deptDext.getDateDepartmentAdded(),
					deptDext.getDepartmentPhaseOutDate(),
					deptDext.getTermEntryLevel(),
					deptDext.getShcReportLevel(),
					deptDext.getCorpSummLevel(),
					deptDext.getSalesTermEntryLevel(),
					deptDext.getSalesShcReportLevel(),
					deptDext.getSalesCorpSummLevel(),
					deptDext.getStoreUseCode(),
					deptDext.getShcOperCode(),
					deptDext.getOtherOperCode(),
					deptDext.getAssocOperCode(),
					deptDext.getSpecialityCode(),
					deptDext.getCashSalesCode(),
					deptDext.getPayrollCode(),
					deptDext.getRetailCode(),
					deptDext.getOwnerCode(),
					deptDext.getOverHeadCode(),
					deptDext.getInventoriedDepartment(),
					deptDext.getAccountPayableCode(),
					deptDext.getCostOnlyDept(),
					deptDext.getAccountRequired(),
					deptDext.getAssociateDiscount(),
					deptDext.getFoodStampsEbt(),
					deptDext.getPurchaseOrderCode(),
					deptDext.getDeptId());
		} catch(DataIntegrityViolationException e) {
			e.printStackTrace();
			deptDextUpdated = new DeptDext();
			deptDextUpdated.setValidationMessages("" + e.getCause());
		} catch(Exception exp) {
			deptDextUpdated = new DeptDext();
			deptDextUpdated.setValidationMessages("Error Messages - " + exp.getCause());
		}
		
				
		if(deptStatus > 0) {
			LOGGER.debug("DEPT Record is updated successfully.");
			try {
				dextStatus = jdbcTemplate.update(UPDATE_DEXT, deptDext.getGroupCode(), deptDext.getReportingDept(), deptDext.getDextId());
			}catch(DataIntegrityViolationException e) {
				e.printStackTrace();
				deptDextUpdated = new DeptDext();
				deptDextUpdated.setValidationMessages("" + e.getCause());
			} catch(Exception exp) {
				deptDextUpdated = new DeptDext();
				deptDextUpdated.setValidationMessages("Error Messages - " + exp.getCause());
			}
		}
		
		if(dextStatus > 0) {
			deptDextUpdated = this.getDeptDext(Integer.valueOf(deptDext.getDepartmentNumber()));
		}
		return deptDextUpdated;
	}

}
