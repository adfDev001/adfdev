package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.RealMapper;
import com.sears.model.Location;
import com.sears.model.Real;
import com.sears.repository.LocationRepository;
import com.sears.repository.RealRepository;

@Repository
public class RealRepositoryImpl implements RealRepository{

	private static final Logger LOGGER = LoggerFactory.getLogger(RealRepositoryImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private LocationRepository locationRepository;

	private static final String GET_REAL= "select \r\n" + 
			"RAL.OCC_ID, \r\n" + 
			"RAL.REAL_LEASED_OWNED_CODE, \r\n" + 
			"RAL.REAL_ORIGINAL_LEASE_DATE, \r\n" + 
			"RAL.REAL_LEASE_RENEWAL1, \r\n" + 
			"RAL.REAL_LEASE_RENEWAL2,\r\n" + 
			"RAL.REAL_LEASE_RENEWAL3 \r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_REAL RAL\r\n" + 
			"WHERE LOC.OCC_ID = RAL.PARENT_ID\r\n" + 
			"AND LOC.LOCN_NUMBER= ?";
	
	private static final String GET_MAX_REAL_ID = "select max(occ_id) as REAL_ID from PLOCN2_REAL";
	
	
	private static final String CREATE_REAL = "INSERT INTO PLOCN2_REAL \r\n" + 
			"(OCC_ID, \r\n" + 
			"PARENT_ID, \r\n" + 
			"REAL_LEASED_OWNED_CODE, \r\n" + 
			"REAL_ORIGINAL_LEASE_DATE, \r\n" + 
			"REAL_LEASE_RENEWAL1,\r\n" + 
			"REAL_LEASE_RENEWAL2,\r\n" + 
			"REAL_LEASE_RENEWAL3\r\n" + 
			")\r\n" + 
			"VALUES (?,?,?,?,?,?,?)";
	
	private static final String UPDATE_REAL = "UPDATE PLOCN2_REAL SET\r\n" + 
			"REAL_LEASED_OWNED_CODE =? ,\r\n" + 
			"REAL_ORIGINAL_LEASE_DATE=? , \r\n" + 
			"REAL_LEASE_RENEWAL1=?, \r\n" + 
			"REAL_LEASE_RENEWAL2=?, \r\n" + 
			"REAL_LEASE_RENEWAL3=?\r\n" + 
			"WHERE OCC_ID = ?";
	
	@Override
	public Real getReal(String locationNumber, String realId){
		Real real = null;
		try {
			real = jdbcTemplate.queryForObject(GET_REAL, new RealMapper(), locationNumber);
		}catch(EmptyResultDataAccessException exp) {
			LOGGER.error("Unique Record is not found", exp);
		}
		return real;
	}
	
	@Override
	public Real createReal(Real real) {
		Real realCreated = null;
		int status = 0;
		Map<String, Object> retMap = jdbcTemplate.queryForMap(GET_MAX_REAL_ID);
		BigDecimal nextRealId = (BigDecimal) retMap.get("REAL_ID");
		real.setRealId(nextRealId.longValue()+1);
		
		Location location  = locationRepository.getLocation(Long.valueOf(real.getLocationNumber()));
		try {
			status = jdbcTemplate.update(CREATE_REAL, real.getRealId(), location.getLocationId(), 
					real.getLeasedOwnedCode(), real.getOriginalLeasedDate(), real.getFirstLeaseRenewalDate(),
					real.getSecondLeaseRenewalDate(), real.getThirdLeaseRenewalDate());
		}catch(Exception exp) {
			exp.printStackTrace();
			realCreated = new Real();
			realCreated.setValidationMessages("Error Messages -"+ exp.getCause());
		}
		if(status > 0) {
			realCreated = this.getReal(real.getLocationNumber(), "");
		}
		return realCreated;
	}
	
	@Override
	public Real updateReal(Real real) {
		Real realUpdated = null;
		int status = 0;
		try {
			
		}catch(Exception exp) {
			exp.printStackTrace();
			realUpdated = new Real();
			realUpdated.setValidationMessages("Error Messages -"+ exp.getCause());
		}
		status = jdbcTemplate.update(UPDATE_REAL, real.getLeasedOwnedCode(), real.getOriginalLeasedDate(), real.getFirstLeaseRenewalDate(), 
				real.getSecondLeaseRenewalDate(), real.getThirdLeaseRenewalDate(), real.getRealId());
		if(status > 0) {
			LOGGER.debug("Updated Successfully ....!!!");
			realUpdated = this.getReal(real.getLocationNumber(), "");
		}else {
			LOGGER.debug("Updated is failed ....!!!");
		}
		return realUpdated;
	}
}
