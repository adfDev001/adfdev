package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.AdminMapper;
import com.sears.model.User;
import com.sears.repository.AdminRepository;

@Repository
public class AdminRepositoryImpl implements AdminRepository{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String INSERT_ADMIN_SQL= "INSERT INTO ADMIN1\r\n" + 
			"(\r\n" + 
			"OCC_ID, \r\n" + 
			"USERID,\r\n" + 
			"LOCNLEXTSEGMENTACCESS, \r\n" + 
			"DPIDSEGMENTACCESS, \r\n" + 
			"DPTVSEGMENTACCESS, \r\n" + 
			"KINSSEGMENTACCESS, \r\n" + 
			"STATSEGMENTACCESS, \r\n" + 
			"REALSEGMENTACCESS, \r\n" + 
			"BANKSEGMENTACCESS, \r\n" + 
			"DEPTDEXTSEGMENTACCESS, \r\n" + 
			"ADMINACCESS, CASHSEGMENTACCESS \r\n" + 
			")\r\n" + 
			"VALUES \r\n" + 
			"(?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String UPDATE_ADMIN_SQL = "UPDATE  ADMIN1 SET \r\n" + 
			"LOCNLEXTSEGMENTACCESS=?, \r\n" + 
			"DPIDSEGMENTACCESS=?, \r\n" + 
			"DPTVSEGMENTACCESS=?, \r\n" + 
			"KINSSEGMENTACCESS=?, \r\n" + 
			"STATSEGMENTACCESS=?, \r\n" + 
			"REALSEGMENTACCESS=?, \r\n" + 
			"BANKSEGMENTACCESS=?, \r\n" + 
			"DEPTDEXTSEGMENTACCESS=?, \r\n" + 
			"ADMINACCESS = ?\r\n" + 
			"WHERE OCC_ID = ? ";
	
	private static final String GET_ADMIN_BY_NAME = "SELECT * FROM ADMIN1 WHERE UPPER(trim(USERID)) = UPPER(trim(?))"; 
	private static final String GET_ADMIN_BY_ID = "SELECT * FROM ADMIN1 WHERE OCC_ID = ? "; 
	private static final String DEL_ADMIN_BY_ID = "DELETE FROM ADMIN1 WHERE OCC_ID = ? "; 
	
	private static final String DUPLICATE_USER_CHECK = "select count(*) as USER_COUNT from ADMIN1  where UPPER(trim(USERID)) = UPPER(trim(?))";

	private final static String GET_MAX_ID = "select max(OCC_ID) as ADMIN_ID from ADMIN1 ";
	
	private final static String GET_ALL_ADMIN = "SELECT * FROM ADMIN1 order by OCC_ID desc";
	
	@Override
	public User getUser(String userName) {
		LOGGER.debug("User Id - " + userName);
		return jdbcTemplate.queryForObject(GET_ADMIN_BY_NAME, new AdminMapper(),userName.trim());
	}

	@Override
	public User getUserById(Long userId) {
		LOGGER.debug("User Id - " + userId);
		return jdbcTemplate.queryForObject(GET_ADMIN_BY_ID, new AdminMapper(), userId);
	}
	
	@Override
	public List<User> getAllUsers(Integer offset, Integer limit) {
		jdbcTemplate.setMaxRows(1000);
		List<User> users = new ArrayList<User>();
		List<Map<String, Object>> resultMap = jdbcTemplate.queryForList(GET_ALL_ADMIN);
		if(resultMap != null && !resultMap.isEmpty()) {
			for (Map<String, Object> map : resultMap) {
				User user = new User();
				user.setUserId(((BigDecimal)map.get("OCC_ID")).longValue());
				user.setUserName(String.valueOf(map.get("USERID")).trim());
				user.setLocationSegmentAccess(String.valueOf(map.get("LOCNLEXTSEGMENTACCESS")).trim());
				user.setDpidSegmentAccess(String.valueOf(map.get("DPIDSEGMENTACCESS")).trim());
				user.setDptvSegmentAccess(String.valueOf(map.get("DPTVSEGMENTACCESS")).trim());
				user.setDeptDextSegmentAccess(String.valueOf(map.get("DEPTDEXTSEGMENTACCESS")).trim());
				user.setStatSegmentAccess(String.valueOf(map.get("STATSEGMENTACCESS")).trim());
				user.setRealSegmentAccess(String.valueOf(map.get("REALSEGMENTACCESS")).trim());
				user.setBankSegmentAccess(String.valueOf(map.get("BANKSEGMENTACCESS")).trim());
				user.setKinsSegmentAccess(String.valueOf(map.get("KINSSEGMENTACCESS")).trim());
				user.setAdminSegmentAccess(String.valueOf(map.get("ADMINACCESS")).trim());
				users.add(user);
			}
		}
		return users;
	}

	@Override
	public User createUser(User user) {
		int status = 0;
		Map<String, Object> userDuplicateMap = jdbcTemplate.queryForMap(DUPLICATE_USER_CHECK, user.getUserName().trim());
		System.out.println("userDuplicateMap:" + userDuplicateMap);
		BigDecimal duplicateCount = (BigDecimal) userDuplicateMap.get("USER_COUNT");
		System.out.println("duplicateCount:" + duplicateCount);
		Long userDuplicateCount = duplicateCount.longValue();
		System.out.println("userDuplicateCount:" + userDuplicateCount);
		if(userDuplicateCount > 0) {
			user = new User();
			user.setValidationMessages("This user already exists into system.");
			return user;
		}else {
			Map<String, Object> retMap = jdbcTemplate.queryForMap(GET_MAX_ID);
			System.out.println("Max ID call is done");
			BigDecimal nextMaxId = (BigDecimal) retMap.get("ADMIN_ID");
			System.out.println("Max ID call is done - > nextDepartmentId" + nextMaxId);
			user.setUserId(nextMaxId.longValue()+1);
			LOGGER.debug("OCC_id - " + user.getUserId());
			
			status = jdbcTemplate.update(INSERT_ADMIN_SQL, user.getUserId(), user.getUserName(), user.getLocationSegmentAccess(),
											user.getDpidSegmentAccess(), user.getDptvSegmentAccess(), user.getKinsSegmentAccess(),
											user.getStatSegmentAccess(), user.getRealSegmentAccess(), user.getBankSegmentAccess(),
											user.getDeptDextSegmentAccess(), user.getAdminSegmentAccess(), "NULL");
		}
				
		if(status > 0) {
			return user;
		} else {
			user = new User();
			user.setValidationMessages("Error occured while inserting admin record ");
		}
		return user;
	}

	@Override
	public User updateUser(User user) {
		LOGGER.debug("User Id :::::::"  + user.getUserId());
		int status = jdbcTemplate.update(UPDATE_ADMIN_SQL, user.getLocationSegmentAccess(),
				user.getDpidSegmentAccess(), user.getDptvSegmentAccess(), user.getKinsSegmentAccess(),
				user.getStatSegmentAccess(), user.getRealSegmentAccess(), user.getBankSegmentAccess(),
				user.getDeptDextSegmentAccess(), user.getAdminSegmentAccess(), user.getUserId());
		
		if(status > 0) {
			return user;
		}else {
			user = new User();
			user.setValidationMessages("Error occured while update");
		}
		return user;
	}

	@Override
	public User deleteUser(User user) {
		User userDeleted = null;
		int status = 0;
		try {
			status = jdbcTemplate.update(DEL_ADMIN_BY_ID, user.getUserId());
		}catch(Exception exp) {
			exp.printStackTrace();
			userDeleted = new User();
			userDeleted.setValidationMessages("Error Message - " + exp.getCause());
		}
		if(status > 0) {
			return user;
		}else {
			return userDeleted;
		}
	}
	
}
