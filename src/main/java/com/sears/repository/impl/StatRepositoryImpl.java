package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.StatMapper;
import com.sears.model.Stat;
import com.sears.model.StatVO;
import com.sears.repository.StatRepository;

@Repository
public class StatRepositoryImpl implements StatRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(StatRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final String GET_STAT = "select  \r\n" + 
						"STAT.OCC_ID, LOC.LOCN_NUMBER, \r\n" + 
			"STAT.STAT_FISCAL_YEAR, \r\n" + 
			"STAT.STAT_FISCAL_PERIOD, \r\n" + 
			"STAT.STAT_STATUS_CODE1, \r\n" + 
			"STAT.STAT_STATUS_CODE2, \r\n" + 
			"STAT.STAT_STATUS_CODE3, \r\n" + 
			"STAT.STAT_STATUS_CODE4, \r\n" + 
			"STAT.STAT_STATUS_CODE5, \r\n" + 
			"STAT.STAT_STATUS_CODE6, \r\n" + 
			"STAT.STAT_STATUS_CODE7, \r\n" + 
			"STAT.STAT_STATUS_CODE8, \r\n" + 
			"STAT.STAT_STATUS_CODE9, \r\n" + 
			"STAT.STAT_STATUS_CODE10,\r\n" + 
			"STAT.STAT_PREVIOUS_DAY_DATE, \r\n" + 
			"STAT.STAT_LINE19_PREV_BALANCE, \r\n" + 
			"STAT.STAT_UNBAL_SALES_CURR_WEEK, \r\n" + 
			"STAT.STAT_UNBAL_REG_FUNDS_CUR_WEEK, \r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE1, \r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE2, \r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE3, \r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE4, \r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE5, \r\n" + 
			"STAT.STAT_APAY_INVOICE_TRAN_DATE, \r\n" + 
			"STAT.STAT_APAY_605606_TRAN_DATE, \r\n" + 
			"STAT.STAT_APAY_USE_TAX_PURCH,  \r\n" + 
			"STAT.STAT_APAY_TAX_PAID,  \r\n" + 
			"STAT.STAT_APAY_MERCH_STORE_USE, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES1, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES2, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES3, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES4, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES5, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES6, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES7, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES8, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES9, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES10, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES11, \r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES12, \r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG1, \r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG2, \r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG3, \r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG4, \r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG5, \r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG1, \r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG2, \r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG3, \r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG4, \r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG5, \r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG1, \r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG2, \r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG3, \r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG4, \r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG5 \r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_STAT STAT  \r\n" + 
			"WHERE LOC.OCC_ID = STAT.PARENT_ID \r\n" + 
			"AND LOC.LOCN_NUMBER = ? \r\n" + 
			"AND STAT.STAT_FISCAL_YEAR || STAT.STAT_FISCAL_PERIOD  = ?";
	
	private static final String GET_STAT_BY_ID = "select \r\n" + 
			"STAT.OCC_ID, LOC.LOCN_NUMBER,\r\n" + 
			"STAT.STAT_FISCAL_YEAR,\r\n" + 
			"STAT.STAT_FISCAL_PERIOD,\r\n" + 
			"STAT.STAT_STATUS_CODE1,\r\n" + 
			"STAT.STAT_STATUS_CODE2,\r\n" + 
			"STAT.STAT_STATUS_CODE3,\r\n" + 
			"STAT.STAT_STATUS_CODE4,\r\n" + 
			"STAT.STAT_STATUS_CODE5,\r\n" + 
			"STAT.STAT_STATUS_CODE6,\r\n" + 
			"STAT.STAT_STATUS_CODE7,\r\n" + 
			"STAT.STAT_STATUS_CODE8,\r\n" + 
			"STAT.STAT_STATUS_CODE9,\r\n" + 
			"STAT.STAT_STATUS_CODE10,\r\n" + 
			"STAT.STAT_PREVIOUS_DAY_DATE, \r\n" + 
			"STAT.STAT_LINE19_PREV_BALANCE,\r\n" + 
			"STAT.STAT_UNBAL_SALES_CURR_WEEK, \r\n" + 
			"STAT.STAT_UNBAL_REG_FUNDS_CUR_WEEK,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE1,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE2,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE3,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE4,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE5,\r\n" + 
			"STAT.STAT_APAY_INVOICE_TRAN_DATE,\r\n" + 
			"STAT.STAT_APAY_605606_TRAN_DATE,\r\n" + 
			"STAT.STAT_APAY_USE_TAX_PURCH, \r\n" + 
			"STAT.STAT_APAY_TAX_PAID, \r\n" + 
			"STAT.STAT_APAY_MERCH_STORE_USE,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES1,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES2,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES3,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES4,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES5,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES6,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES7,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES8,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES9,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES10,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES11,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES12,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG5,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG5,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG5\r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_STAT STAT\r\n" + 
			"WHERE LOC.OCC_ID = STAT.PARENT_ID\r\n" + 
			"AND STAT.OCC_ID = ? "; 
	
	private static final String GET_STAT_BY_KEY_FIELD = "select \r\n" + 
			"STAT.OCC_ID, LOC.LOCN_NUMBER,\r\n" + 
			"STAT.STAT_FISCAL_YEAR,\r\n" + 
			"STAT.STAT_FISCAL_PERIOD,\r\n" + 
			"STAT.STAT_STATUS_CODE1,\r\n" + 
			"STAT.STAT_STATUS_CODE2,\r\n" + 
			"STAT.STAT_STATUS_CODE3,\r\n" + 
			"STAT.STAT_STATUS_CODE4,\r\n" + 
			"STAT.STAT_STATUS_CODE5,\r\n" + 
			"STAT.STAT_STATUS_CODE6,\r\n" + 
			"STAT.STAT_STATUS_CODE7,\r\n" + 
			"STAT.STAT_STATUS_CODE8,\r\n" + 
			"STAT.STAT_STATUS_CODE9,\r\n" + 
			"STAT.STAT_STATUS_CODE10,\r\n" + 
			"STAT.STAT_PREVIOUS_DAY_DATE, \r\n" + 
			"STAT.STAT_LINE19_PREV_BALANCE,\r\n" + 
			"STAT.STAT_UNBAL_SALES_CURR_WEEK, \r\n" + 
			"STAT.STAT_UNBAL_REG_FUNDS_CUR_WEEK,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE1,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE2,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE3,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE4,\r\n" + 
			"STAT.STAT_APAY_SP_ADJ_STAT_CODE5,\r\n" + 
			"STAT.STAT_APAY_INVOICE_TRAN_DATE,\r\n" + 
			"STAT.STAT_APAY_605606_TRAN_DATE,\r\n" + 
			"STAT.STAT_APAY_USE_TAX_PURCH, \r\n" + 
			"STAT.STAT_APAY_TAX_PAID, \r\n" + 
			"STAT.STAT_APAY_MERCH_STORE_USE,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES1,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES2,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES3,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES4,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES5,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES6,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES7,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES8,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES9,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES10,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES11,\r\n" + 
			"STAT.STAT_GENA_COMPLETE_CODES12,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_COMPLETED_FLAG5,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_PROCESS_FLAG5,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG1,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG2,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG3,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG4,\r\n" + 
			"STAT.STAT_WEEK_FORCE_FLAG5\r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_STAT STAT\r\n" + 
			"WHERE LOC.OCC_ID = STAT.PARENT_ID \r\n" + 
			" AND LOC.LOCN_NUMBER = ? AND STAT.STAT_FISCAL_YEAR || STAT.STAT_FISCAL_PERIOD  = ?  "; 


	
	private static final String GET_ALL_STAT = "SELECT \r\n" + 
			"LOCN.OCC_ID AS LOCN_ID,\r\n" + 
			"LOCN.LOCN_NUMBER,\r\n" + 
			"STAT.OCC_ID AS STAT_ID,\r\n" + 
			"STAT.STAT_FISCAL_YEAR || STAT.STAT_FISCAL_PERIOD AS KEYFIELDS,\r\n" + 
			"STAT.STAT_FISCAL_YEAR,\r\n" + 
			"STAT.STAT_FISCAL_PERIOD,\r\n" + 
			"STAT.STAT_PREVIOUS_DAY_DATE,\r\n" + 
			"STAT.STAT_LINE19_PREV_BALANCE,\r\n" + 
			"STAT.STAT_APAY_TAX_PAID,\r\n" + 
			"STAT.STAT_APAY_USE_TAX_PURCH\r\n" + 
			"FROM PLOCN2_STAT STAT, PLOCN2_LOCN LOCN\r\n" + 
			"WHERE LOCN.OCC_ID = STAT.PARENT_ID order by STAT_ID desc";
	
	private static final String GET_ALL_STAT_BY_LOC = "SELECT \r\n" + 
			"LOCN.OCC_ID AS LOCN_ID,\r\n" + 
			"LOCN.LOCN_NUMBER,\r\n" + 
			"STAT.OCC_ID AS STAT_ID,\r\n" + 
			"STAT.STAT_FISCAL_YEAR || STAT.STAT_FISCAL_PERIOD AS KEYFIELDS,\r\n" + 
			"STAT.STAT_FISCAL_YEAR,\r\n" + 
			"STAT.STAT_FISCAL_PERIOD,\r\n" + 
			"STAT.STAT_PREVIOUS_DAY_DATE,\r\n" + 
			"STAT.STAT_LINE19_PREV_BALANCE,\r\n" + 
			"STAT.STAT_APAY_TAX_PAID,\r\n" + 
			"STAT.STAT_APAY_USE_TAX_PURCH\r\n" + 
			"FROM PLOCN2_STAT STAT, PLOCN2_LOCN LOCN\r\n" + 
			"WHERE LOCN.OCC_ID = STAT.PARENT_ID "
			+ " And LOCN.LOCN_NUMBER = ? order by STAT_ID desc";
	
	
	@Override
	public Stat getStat(String locationNumber, Integer keyFields) {
		return jdbcTemplate.queryForObject(GET_STAT, new StatMapper(), Integer.valueOf(locationNumber), String.valueOf(keyFields));
	}

	@Override
	public Stat getStat(Long statId) {
		return jdbcTemplate.queryForObject(GET_STAT_BY_ID, new StatMapper(), statId);
	}
	
	@Override
	public List<StatVO> getAllStats(Integer offset, Integer limit) {
		List<StatVO> stats = new ArrayList<StatVO>();
		jdbcTemplate.setMaxRows(100);
		List<Map<String, Object>> statLists = jdbcTemplate.queryForList(GET_ALL_STAT);
		if(statLists != null && !statLists.isEmpty()) {
			for (Map<String, Object> map : statLists) {
				StatVO stat = new StatVO();
				stat.setLocationId(Long.valueOf(String.valueOf(map.get("LOCN_ID"))));
				stat.setLocationNumber(String.valueOf(map.get("LOCN_NUMBER")));
				stat.setStatId(Long.valueOf(String.valueOf(map.get("STAT_ID"))));
				stat.setKeyFields(Long.valueOf(String.valueOf(map.get("KEYFIELDS"))));
				stat.setFiscalYear(Integer.valueOf(String.valueOf(map.get("STAT_FISCAL_YEAR"))));
				stat.setPeriod(Integer.valueOf(String.valueOf(map.get("STAT_FISCAL_PERIOD"))));
				stat.setPreviousBalance(new BigDecimal(String.valueOf(map.get("STAT_LINE19_PREV_BALANCE"))));
				stat.setPreviousDayDate(Integer.valueOf(String.valueOf(map.get("STAT_PREVIOUS_DAY_DATE"))));
				stat.setUseTaxPaid(new BigDecimal(String.valueOf(map.get("STAT_APAY_TAX_PAID"))));
				stat.setUseTaxPurchase(new BigDecimal(String.valueOf(map.get("STAT_APAY_USE_TAX_PURCH"))));
				stats.add(stat);
			}
		}
		return stats;
	}

	@Override
	public List<StatVO> getAllStats(Integer locationNumber) {
		List<StatVO> stats = new ArrayList<StatVO>();
		jdbcTemplate.setMaxRows(100);
		List<Map<String, Object>> statLists = jdbcTemplate.queryForList(GET_ALL_STAT_BY_LOC, locationNumber);
		if(statLists != null && !statLists.isEmpty()) {
			for (Map<String, Object> map : statLists) {
				StatVO stat = new StatVO();
				stat.setLocationId(Long.valueOf(String.valueOf(map.get("LOCN_ID"))));
				stat.setLocationNumber(String.valueOf(map.get("LOCN_NUMBER")));
				stat.setStatId(Long.valueOf(String.valueOf(map.get("STAT_ID"))));
				stat.setKeyFields(Long.valueOf(String.valueOf(map.get("KEYFIELDS"))));
				stat.setFiscalYear(Integer.valueOf(String.valueOf(map.get("STAT_FISCAL_YEAR"))));
				stat.setPeriod(Integer.valueOf(String.valueOf(map.get("STAT_FISCAL_PERIOD"))));
				stat.setPreviousBalance(new BigDecimal(String.valueOf(map.get("STAT_LINE19_PREV_BALANCE"))));
				stat.setPreviousDayDate(Integer.valueOf(String.valueOf(map.get("STAT_PREVIOUS_DAY_DATE"))));
				stat.setUseTaxPaid(new BigDecimal(String.valueOf(map.get("STAT_APAY_TAX_PAID"))));
				stat.setUseTaxPurchase(new BigDecimal(String.valueOf(map.get("STAT_APAY_USE_TAX_PURCH"))));
				stats.add(stat);
			}
		}
		return stats;
	}

	@Override
	public Stat getStatByKey(Long keyFields,  String locationNumber) {
		return getStat(locationNumber, keyFields.intValue());

	}
	
}
