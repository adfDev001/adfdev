package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.LocationGridDataMapper;
import com.sears.mapper.LocationMapper;
import com.sears.model.Location;
import com.sears.model.LocationVO;
import com.sears.repository.LocationRepository;


@Repository
public class LocationRepositoryImpl implements LocationRepository {
	
	private Logger LOGGER = LoggerFactory.getLogger(LocationRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String GET_ALL_LOCATION = "SELECT * FROM PLOCN2_LOCN order by occ_id desc";
	
	private static final String GET_LOCATION = "select \r\n" + 
			"LOCN.OCC_ID LOCN_ID, \r\n" + 
			"LEXT.OCC_ID LEXT_ID,\r\n" + 
			"LOCN.LOCN_NUMBER,\r\n" + 
			"LOCN.LOCN_EFFECTIVE_DATE,\r\n" + 
			"LOCN.LOCN_TYPE_CODE,\r\n" + 
			"LOCN.LOCN_COMPANY_CODE,\r\n" + 
			"LOCN.LOCN_STREET_ADDRESS,\r\n" + 
			"LOCN.LOCN_SHOP_CNTR_NAME,\r\n" + 
			"LOCN.LOCN_CITY,\r\n" + 
			"LOCN.LOCN_STATE_ABBREV_CODE,\r\n" + 
			"LOCN.LOCN_ZIPCODE,\r\n" + 
			"LOCN.LOCN_ZIP_EXPANSION,\r\n" + 
			"LOCN.LOCN_COUNTY,\r\n" + 
			"LOCN.LOCN_OPEN_DATE,\r\n" + 
			"LOCN.LOCN_OPEN_DATE_CODE,\r\n" + 
			"LOCN.LOCN_CLOSE_DATE,\r\n" + 
			"LOCN.LOCN_REGION_CODE,\r\n" + 
			"LOCN.LOCN_DIVISION_CODE,\r\n" + 
			"LOCN.LOCN_SEARS_UNIT_NBR,\r\n" + 
			"LOCN.LOCN_DIST_CENTER,\r\n" + 
			"LOCN.LOCN_CLIMATIC_ZONE,\r\n" + 
			"LOCN.LOCN_PHONE_NUMBER,\r\n" + 
			"LOCN.LOCN_MANAGER_NAME,\r\n" + 
			"LOCN.LOCN_REGIONAL_MANAGER,\r\n" + 
			"LOCN.LOCN_ORIG_KMART_NBR,\r\n" + 
			"LOCN.LOCN_HIERARCHY_LEVEL1,\r\n" + 
			"LOCN.LOCN_HIERARCHY_LEVEL2,\r\n" + 
			"LOCN.LOCN_HIERARCHY_LEVEL3,\r\n" + 
			"LOCN.LOCN_ORIG_NBR,\r\n" + 
			"LOCN.LOCN_OVERHEAD_NAME,\r\n" + 
			"LOCN.LOCN_TERMINAL_CODE,\r\n" + 
			"LOCN.LOCN_TERMINAL_DATE,\r\n" + 
			"LOCN.LOCN_UPS_ZONE,\r\n" + 
			"LOCN.LOCN_REMOTE_STORE_CODE,\r\n" + 
			"LOCN.LOCN_TIME_ZONE,\r\n" + 
			"LOCN.LOCN_CURRENT_MDSE_ZONE,\r\n" + 
			"LOCN.LOCN_SCANNER_FLAG,\r\n" + 
			"LOCN.LOCN_APPAREL_AREA_CODE,\r\n" + 
			"LOCN.LOCN_ORIG_FACILITY_NBR,\r\n" + 
			"LOCN.LOCN_ORIG_NAI_NUMBER,\r\n" + 
			"LOCN.LOCN_FORMAT_TYPE,\r\n" + 
			"LOCN.LOCN_FORMAT_SUB_TYPE,\r\n" + 
			"LOCN.LOCN_FORMAT_MOD_CD,\r\n" + 
			"LOCN.LOCN_REPORTING_STORE,\r\n" + 
			"LOCN.LOCN_BT_FT_STORE,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE1,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE2,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE3,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE4,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE5,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE6,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE7,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE8,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE9,\r\n" + 
			"LOCN.LOCN_HOLIDAY_CLOSED_DATE10,\r\n" + 
			"LOCN.LOCN_SHRINK_SELL_VALUE_PCT,\r\n" + 
			"LOCN.LOCN_HORT_CLIMATIC_ZONE,\r\n" + 
			"LOCN.LOCN_HIGH_FREIGHT_AREA_CODE,\r\n" + 
			"LOCN.LOCN_COMPARABLE_STORE_NO,\r\n" + 
			"LOCN.LOCN_TEMP_CLOSE_DATE,\r\n" + 
			"LOCN.LOCN_REOPEN_DATE,\r\n" + 
			"LOCN.LOCN_SPECIAL_SHIPPING_ADDRESS,\r\n" + 
			"LOCN.LOCN_STATE_NO,\r\n" + 
			"LOCN.LOCN_COUNTY_NO,\r\n" + 
			"LOCN.LOCN_CITY_NO,\r\n" + 
			"LOCN.LOCN_TOTAL_SQ_FOOTAGE,\r\n" + 
			"LOCN.LOCN_SELLING_AREA_SQ_FOOTAGE,\r\n" + 
			"LOCN.LOCN_FOOD_SEAT_CAPACITY,\r\n" + 
			"LOCN.LOCN_FOOD_SERVICE_TYPE,\r\n" + 
			"LOCN.LOCN_AUTO_AREA_CODE,\r\n" + 
			"LEXT.LEXT_FUTURE_MDSE_ZONE,\r\n" + 
			"LEXT.LEXT_MDSE_ZONE_EFF_DATE,\r\n" + 
			"LEXT.LEXT_SHIPPING_STREET_ADDRESS,\r\n" + 
			"LEXT.LEXT_SHIPPING_CITY,\r\n" + 
			"LEXT.LEXT_SHIPPING_STATE_ABBREV_CD,\r\n" + 
			"LEXT.LEXT_ZIPCODE,\r\n" + 
			"LEXT.LEXT_ZIP_EXPANSION,\r\n" + 
			"LEXT.LEXT_RECEIVING_STORE,\r\n" + 
			"LEXT.LEXT_AREA_MERCH_COORDINATOR,\r\n" + 
			"LEXT.LEXT_PLANNED_CLOSED_DATE, LEXT.LEXT_FAX_PHONE, LEXT.LEXT_LONGITUDE, LEXT.LEXT_LATITUDE\r\n" + 
			"from PLOCN2_LOCN LOCN, PLOCN2_LEXT LEXT\r\n" + 
			"WHERE LOCN.OCC_ID = LEXT.PARENT_ID\r\n" + 
			"AND LOCN.LOCN_NUMBER = ?";
	
	private static final String GET_LOCATION_BY_LOCATION_NBR = "SELECT *  FROM PLOCN2_LOCN where LOCN_NUMBER = ?";
	
	private static final String GET_MAX_LOCN_ID = "select max(occ_id) as LOCN_ID from PLOCN2_LOCN";
	
	private static final String LOCATION_DUPLICATE_VALIDATION = "select count(*) as LOCN_COUNT from PLOCN2_LOCN where LOCN_NUMBER = ?";
	
	private static final String INSERT_LOCATION = "INSERT INTO  PLOCN2_LOCN \r\n" + 
			"(\r\n" + 
			"OCC_ID,\r\n" + 
			"LOCN_NUMBER,\r\n" + 
			"LOCN_EFFECTIVE_DATE,\r\n" + 
			"LOCN_TYPE_CODE,\r\n" + 
			"LOCN_COMPANY_CODE,\r\n" + 
			"LOCN_STREET_ADDRESS,\r\n" + 
			"LOCN_SHOP_CNTR_NAME,\r\n" + 
			"LOCN_CITY,\r\n" + 
			"LOCN_STATE_ABBREV_CODE,\r\n" + 
			"LOCN_ZIPCODE,\r\n" + 
			"LOCN_ZIP_EXPANSION,\r\n" + 
			"LOCN_COUNTY,\r\n" + 
			"LOCN_OPEN_DATE,\r\n" + 
			"LOCN_CLOSE_DATE,\r\n" + 
			"LOCN_REGION_CODE,\r\n" + 
			"LOCN_DIVISION_CODE,\r\n" + 
			"LOCN_SEARS_UNIT_NBR,\r\n" + 
			"LOCN_CLIMATIC_ZONE,\r\n" + 
			"LOCN_PHONE_NUMBER,\r\n" + 
			"LOCN_REGIONAL_MANAGER,\r\n" + 
			"LOCN_ORIG_KMART_NBR,\r\n" + 
			"LOCN_HIERARCHY_LEVEL1,\r\n" + 
			"LOCN_HIERARCHY_LEVEL2,\r\n" + 
			"LOCN_HIERARCHY_LEVEL3,\r\n" + 
			"LOCN_ORIG_NBR,\r\n" + 
			"LOCN_OVERHEAD_NAME,\r\n" + 
			"LOCN_UPS_ZONE,\r\n" + 
			"LOCN_TIME_ZONE,\r\n" + 
			"LOCN_CURRENT_MDSE_ZONE,\r\n" + 
			"LOCN_MANAGER_NAME,\r\n" + 
			"LOCN_ORIG_FACILITY_NBR,\r\n" + 
			"LOCN_ORIG_NAI_NUMBER,\r\n" + 
			"LOCN_FORMAT_TYPE,\r\n" + 
			"LOCN_FORMAT_SUB_TYPE,\r\n" + 
			"LOCN_REPORTING_STORE,\r\n" + 
			"LOCN_BT_FT_STORE,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE1,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE2,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE3,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE4,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE5,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE6,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE7,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE8,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE9,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE10,\r\n" + 
			"LOCN_TEMP_CLOSE_DATE,\r\n" + 
			"LOCN_REOPEN_DATE,\r\n" + 
			"LOCN_SPECIAL_SHIPPING_ADDRESS,\r\n" + 
			"LOCN_STATE_NO,\r\n" + 
			"LOCN_COUNTY_NO,\r\n" + 
			"LOCN_CITY_NO,\r\n" + 
			"LOCN_TOTAL_SQ_FOOTAGE,\r\n" + 
			"LOCN_SELLING_AREA_SQ_FOOTAGE,\r\n" + 
			"LOCN_FORMAT_MOD_CD)\r\n" + 
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,\r\n" + 
			"?,?,?,?,?,?,?)";
	
	private static final String GET_MAX_LEXT_ID = "select max(occ_id) as LEXT_ID from PLOCN2_LEXT";
	
	private static final String INSERT_LEXT = "INSERT INTO PLOCN2_LEXT \r\n" + 
			"(\r\n" + 
			"OCC_ID,\r\n" + 
			"PARENT_ID,\r\n" + 
			"LEXT_SHIPPING_STREET_ADDRESS,\r\n" + 
			"LEXT_SHIPPING_CITY,\r\n" + 
			"LEXT_SHIPPING_STATE_ABBREV_CD,\r\n" + 
			"LEXT_ZIPCODE,\r\n" + 
			"LEXT_ZIP_EXPANSION,\r\n" + 
			"LEXT_FAX_PHONE,\r\n" + 
			"LEXT_LATITUDE,\r\n" + 
			"LEXT_LONGITUDE, \r\n" + 
			"LEXT_AREA_MERCH_COORDINATOR, LEXT_PLANNED_CLOSED_DATE)\r\n" + 
			"VALUES \r\n" + 
			"(?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String UPDATE_LOCATION = "UPDATE PLOCN2_LOCN SET \r\n" + 
			"LOCN_NUMBER=?,\r\n" + 
			"LOCN_EFFECTIVE_DATE=?,\r\n" + 
			"LOCN_TYPE_CODE=?,\r\n" + 
			"LOCN_COMPANY_CODE=?,\r\n" + 
			"LOCN_STREET_ADDRESS=?,\r\n" + 
			"LOCN_SHOP_CNTR_NAME=?,\r\n" + 
			"LOCN_CITY=?,\r\n" + 
			"LOCN_STATE_ABBREV_CODE=?,\r\n" + 
			"LOCN_ZIPCODE=?,\r\n" + 
			"LOCN_ZIP_EXPANSION=?,\r\n" + 
			"LOCN_COUNTY=?,\r\n" + 
			"LOCN_OPEN_DATE=?,\r\n" + 
			"LOCN_CLOSE_DATE=?,\r\n" + 
			"LOCN_REGION_CODE=?,\r\n" + 
			"LOCN_DIVISION_CODE=?,\r\n" + 
			"LOCN_SEARS_UNIT_NBR=?,\r\n" + 
			"LOCN_CLIMATIC_ZONE=?,\r\n" + 
			"LOCN_PHONE_NUMBER=?,\r\n" + 
			"LOCN_REGIONAL_MANAGER=?,\r\n" + 
			"LOCN_ORIG_KMART_NBR=?,\r\n" + 
			"LOCN_HIERARCHY_LEVEL1=?,\r\n" + 
			"LOCN_HIERARCHY_LEVEL2=?,\r\n" + 
			"LOCN_HIERARCHY_LEVEL3=?,\r\n" + 
			"LOCN_ORIG_NBR=?,\r\n" + 
			"LOCN_OVERHEAD_NAME=?,\r\n" + 
			"LOCN_UPS_ZONE=?,\r\n" + 
			"LOCN_TIME_ZONE=?,\r\n" + 
			"LOCN_CURRENT_MDSE_ZONE=?,\r\n" + 
			"LOCN_MANAGER_NAME=?,\r\n" + 
			"LOCN_ORIG_FACILITY_NBR=?,\r\n" + 
			"LOCN_ORIG_NAI_NUMBER=?,\r\n" + 
			"LOCN_FORMAT_TYPE=?,\r\n" + 
			"LOCN_FORMAT_SUB_TYPE=?,\r\n" + 
			"LOCN_REPORTING_STORE=?,\r\n" + 
			"LOCN_BT_FT_STORE=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE1=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE2=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE3=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE4=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE5=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE6=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE7=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE8=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE9=?,\r\n" + 
			"LOCN_HOLIDAY_CLOSED_DATE10=?,\r\n" + 
			"LOCN_TEMP_CLOSE_DATE=?,\r\n" + 
			"LOCN_REOPEN_DATE=?,\r\n" + 
			"LOCN_SPECIAL_SHIPPING_ADDRESS=?,\r\n" + 
			"LOCN_STATE_NO=?,\r\n" + 
			"LOCN_COUNTY_NO=?,\r\n" + 
			"LOCN_CITY_NO=?,\r\n" + 
			"LOCN_TOTAL_SQ_FOOTAGE=?,\r\n" + 
			"LOCN_SELLING_AREA_SQ_FOOTAGE=?,\r\n" + 
			"LOCN_FORMAT_MOD_CD= ? \r\n" + 
			"WHERE OCC_ID = ?";
	
	private static final String UPDATE_LEXT = "UPDATE PLOCN2_LEXT SET \r\n" + 
			"LEXT_SHIPPING_STREET_ADDRESS = ?,\r\n" + 
			"LEXT_SHIPPING_CITY = ?,\r\n" + 
			"LEXT_SHIPPING_STATE_ABBREV_CD = ?,\r\n" + 
			"LEXT_ZIPCODE=?,\r\n" + 
			"LEXT_ZIP_EXPANSION=?,\r\n" + 
			"LEXT_FAX_PHONE=?,\r\n" + 
			"LEXT_LATITUDE=?,\r\n" + 
			"LEXT_LONGITUDE=?,\r\n" + 
			"LEXT_AREA_MERCH_COORDINATOR=?,\r\n" + 
			"LEXT_PLANNED_CLOSED_DATE=?\r\n" + 
			"WHERE OCC_ID = ?";
	
	
	@Override
	public List<LocationVO> getAllLocations(Integer offet, Integer limit) {
		jdbcTemplate.setMaxRows(100);
		return jdbcTemplate.query(GET_ALL_LOCATION, new LocationGridDataMapper());
	}

	@Override
	public Location getLocation(Long locationNumber) {
		LOGGER.debug("LOCATION NUMBER : "+ locationNumber);
		return jdbcTemplate.queryForObject(GET_LOCATION, new LocationMapper(), locationNumber);
	}
	
	@Override
	public Location getLocationByLocation(Long locationNumber) {
		LOGGER.debug("LOCATION NUMBER : "+ locationNumber);
		return jdbcTemplate.queryForObject(GET_LOCATION_BY_LOCATION_NBR, new LocationMapper(), locationNumber);
	}

	@Override
	public Location createLocation(Location location) {
		int locationStatus = 0;
		int lextStatus = 0;
		Location locationCreated = null;
		Map<String, Object> locationDuplicateMap = jdbcTemplate.queryForMap(LOCATION_DUPLICATE_VALIDATION, location.getLocationNumber());
		BigDecimal duplicateCount = (BigDecimal) locationDuplicateMap.get("LOCN_COUNT");
		Long locationCount = duplicateCount.longValue();
		
		if(locationCount > 0) {
			LOGGER.debug(" THERE IS ALREADY A RECORD FOR LOCATION NUMBER " + location.getLocationNumber());
			locationCreated = new Location();
			locationCreated.setValidationMessages(" THERE IS ALREADY A RECORD FOR LOCATION NUMBER " + location.getLocationNumber());
		}else {
			Map<String, Object> retMap = jdbcTemplate.queryForMap(GET_MAX_LOCN_ID);
			BigDecimal nextLocationId = (BigDecimal) retMap.get("LOCN_ID");
			LOGGER.debug("NEXT LOCATION OCC ID  : "+ nextLocationId);
			location.setLocationId(nextLocationId.longValue()+1);
			LOGGER.debug("INSERT STARTING .... FOR LOCATION NUMBER - " + location.getLocationNumber());
			try {
				locationStatus = jdbcTemplate.update(INSERT_LOCATION, 
						location.getLocationId(),
						location.getLocationNumber(),
						location.getPlanedOpenDate(),
						location.getTypeCode(),
						Integer.valueOf(location.getCompanyCode()),
						location.getStreetAddress(),
						location.getShoppingCenter(),
						location.getCity(),
						location.getState(),
						Integer.valueOf(location.getZipCode()),
						Integer.valueOf(location.getZipCodeExtension()),
						location.getCounty(),
						Integer.valueOf(location.getActualOpenDate()),
						Integer.valueOf(location.getCloseDate()),
						location.getRegionCode(),
						location.getDevisionCode(),
						Integer.valueOf(location.getSearUnitNumber()),
						Integer.valueOf(location.getClimaticZone()),
						Long.valueOf(location.getStorePhone()),
						Integer.valueOf(location.getRegionalManager()),
						Integer.valueOf(location.getOriginalKmartNumber()),
						Integer.valueOf(location.getDistrictLevel1()),
						Integer.valueOf(location.getDistrictLevel2()),
						Integer.valueOf(location.getSummaryLevel3()),
						Integer.valueOf(location.getOriginalNumber()),
						location.getOverHeadName(),
						Integer.valueOf(location.getUpsZone()),
						Integer.valueOf(location.getTimeZone()),
						Integer.valueOf(location.getMdseAreaRotoZone()),
						location.getManagerName(), 
						location.getOriginalFacility(),
						location.getOriginalNumber(),
						location.getFormatType(),
						location.getFormatSubType(),
						Integer.valueOf(location.getRecievingStoreNumber()),
						Integer.valueOf(location.getBtftLocationNumber()),
						Integer.valueOf(location.getHolidayCloseDate1()),
						Integer.valueOf(location.getHolidayCloseDate2()),
						Integer.valueOf(location.getHolidayCloseDate3()),
						Integer.valueOf(location.getHolidayCloseDate4()),
						Integer.valueOf(location.getHolidayCloseDate5()),
						Integer.valueOf(location.getHolidayCloseDate6()),
						Integer.valueOf(location.getHolidayCloseDate7()),
						Integer.valueOf(location.getHolidayCloseDate8()),
						Integer.valueOf(location.getHolidayCloseDate9()),
						Integer.valueOf(location.getHolidayCloseDate10()),
						Integer.valueOf(location.getCloseDate()),
						Integer.valueOf(location.getReOpenDate()),
						location.getShippingAddressFlag(),
						Integer.valueOf(location.getStateNumber()), 
						Integer.valueOf(location.getCountyNumber()),
						Integer.valueOf(location.getCityNumber()), 
						Integer.valueOf(location.getTotalSQFootage()),
						Integer.valueOf(location.getSellingAreaSqFoot()),
						location.getFormatModifier());
						LOGGER.debug("INSERT COMPLETED .... FOR LOCATION NUMBER - " + location.getLocationNumber());
			} catch(Exception e) {
				e.printStackTrace();
				locationCreated = new Location();
				locationCreated.setValidationMessages("Error Message -" + e.getCause());
			}
			System.out.println("locationStatus::::::::::::::::::::"+locationStatus);
			if(locationStatus > 0) {
				Map<String, Object> retMapLext = jdbcTemplate.queryForMap(GET_MAX_LEXT_ID);
				BigDecimal nextLextId = (BigDecimal) retMapLext.get("LEXT_ID");
				LOGGER.debug("NEXT LEXT ID - " + nextLextId+1);
				location.setLocationLextId(nextLextId.longValue()+1);
				LOGGER.debug("INSERT STARTING FOR LEXT.... FOR LOCATION NUMBER - " + location.getLocationNumber());
				
				try {
					lextStatus = jdbcTemplate.update(INSERT_LEXT, 
							location.getLocationLextId(),
							location.getLocationId(),
							location.getShippingStreetAddress(),
							location.getShipCity(),
							location.getShipState(),
							Integer.valueOf(location.getShipZipCode()),
							Integer.valueOf(location.getShipZipCodeExtension()),
							Integer.valueOf(location.getFaxNumber()),
							location.getLatitude(), 
							location.getLangitude(), 
							Integer.valueOf(location.getAreaMerchCoord()),
							Integer.valueOf(location.getPlannedCloseDate())
							);
					LOGGER.debug("INSERT COMPLETED FOR LEXT.... FOR LOCATION NUMBER - " + location.getLocationNumber());
				}catch (Exception e) {
					locationCreated = new Location();
					locationCreated.setValidationMessages("Error Message -" + e.getMessage());
					lextStatus = 0;
				}
				if(lextStatus > 0) {
					System.out.println("success");
					locationCreated = this.getLocation(location.getLocationNumber());
				}
			}
		}
		System.out.println("result ::::"+locationCreated );

		return locationCreated;
	}

	@Override
	public Location updateLocation(Location location) {
		int locationStatus = 0;
		int lextStatus = 0;
		Location locationUpdated = null;
		LOGGER.debug("UPDATE STARTING FOR LEXT.... FOR LOCATION NUMBER - " + location.getLocationNumber());
		
		try {
			locationStatus = jdbcTemplate.update(UPDATE_LOCATION, 
					location.getLocationNumber(),
					location.getPlanedOpenDate(),
					location.getTypeCode(),
					Integer.valueOf(location.getCompanyCode()),
					location.getStreetAddress(),
					location.getShoppingCenter(),
					location.getCity(),
					location.getState(),
					Integer.valueOf(location.getZipCode()),
					Integer.valueOf(location.getZipCodeExtension()),
					location.getCounty(),
					Integer.valueOf(location.getActualOpenDate()),
					Integer.valueOf(location.getCloseDate()),
					location.getRegionCode(),
					location.getDevisionCode(),
					Integer.valueOf(location.getSearUnitNumber()),
					Integer.valueOf(location.getClimaticZone()),
					Long.valueOf(location.getStorePhone()),
					Integer.valueOf(location.getRegionalManager()),
					Integer.valueOf(location.getOriginalKmartNumber()),
					Integer.valueOf(location.getDistrictLevel1()),
					Integer.valueOf(location.getDistrictLevel2()),
					Integer.valueOf(location.getSummaryLevel3()),
					Integer.valueOf(location.getOriginalNumber()),
					location.getOverHeadName(),
					Integer.valueOf(location.getUpsZone()),
					Integer.valueOf(location.getTimeZone()),
					Integer.valueOf(location.getMdseAreaRotoZone()),
					location.getManagerName(), 
					location.getOriginalFacility(),
					location.getNaiNumber(),
					location.getFormatType(),
					location.getFormatSubType(),
					Integer.valueOf(location.getRecievingStoreNumber()),
					Integer.valueOf(location.getBtftLocationNumber()),
					Integer.valueOf(location.getHolidayCloseDate1()),
					Integer.valueOf(location.getHolidayCloseDate2()),
					Integer.valueOf(location.getHolidayCloseDate3()),
					Integer.valueOf(location.getHolidayCloseDate4()),
					Integer.valueOf(location.getHolidayCloseDate5()),
					Integer.valueOf(location.getHolidayCloseDate6()),
					Integer.valueOf(location.getHolidayCloseDate7()),
					Integer.valueOf(location.getHolidayCloseDate8()),
					Integer.valueOf(location.getHolidayCloseDate9()),
					Integer.valueOf(location.getHolidayCloseDate10()),
					Integer.valueOf(location.getCloseDate()),
					Integer.valueOf(location.getReOpenDate()),
					location.getShippingAddressFlag(),
					Integer.valueOf(location.getStateNumber()), 
					Integer.valueOf(location.getCountyNumber()),
					Integer.valueOf(location.getCityNumber()), 
					Integer.valueOf(location.getTotalSQFootage()),
					Integer.valueOf(location.getSellingAreaSqFoot()),
					location.getFormatModifier(),
					location.getLocationId()
					);
					LOGGER.debug("UPDATE COMPLETED FOR LOCATION.... FOR LOCATION NUMBER - " + location.getLocationNumber());

		}catch(DataIntegrityViolationException e) {
			e.printStackTrace();
			locationUpdated = new Location();
			locationUpdated.setValidationMessages("Error Messages - " + e.getCause());
		}catch(Exception exp) {
			exp.printStackTrace();
			locationUpdated = new Location();
			locationUpdated.setValidationMessages("Error Messages - " + exp.getCause());
		}
		System.out.println("locationStatus::::::::"+ locationStatus);
		if(locationStatus > 0) {
			LOGGER.debug("UPDATE STARTING  FOR LEXT.... FOR LOCATION NUMBER - " + location.getLocationNumber());
			try {
				System.out.println("location.getLocationLextId()"+ location.getLocationLextId());

				lextStatus = jdbcTemplate.update(UPDATE_LEXT, 
						location.getShippingStreetAddress(),
						location.getShipCity(),
						location.getShipState(),
						location.getShipZipCode(),
						location.getShipZipCodeExtension(),
						location.getFaxNumber(),
						location.getLatitude(),
						location.getLangitude(),
						location.getAreaMerchCoord(),
						location.getPlannedCloseDate(),
						location.getLocationLextId());
				System.out.println("locationStatus::::::::"+ locationStatus);
				LOGGER.debug("UPDATE COMPLETED FOR LEXT.... FOR LOCATION NUMBER - " + location.getLocationNumber());
			}catch(DataIntegrityViolationException e) {
				e.printStackTrace();
				locationUpdated = new Location();
				locationUpdated.setValidationMessages("Error Messages - " + e.getCause());
			}catch(Exception exp) {
				exp.printStackTrace();
				locationUpdated = new Location();
				locationUpdated.setValidationMessages("Error Messages - " + exp.getCause());
			}
			
			if(lextStatus > 0) {
				locationUpdated = this.getLocation(location.getLocationNumber());
			}
		}
		return locationUpdated;
	}

}
