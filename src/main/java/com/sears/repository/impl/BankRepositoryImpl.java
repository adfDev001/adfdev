package com.sears.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.BankMapper;
import com.sears.model.Bank;
import com.sears.repository.BankRepository;

@Repository
public class BankRepositoryImpl implements BankRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BankRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String GET_BANK = "select BANK.OCC_ID, LOC.LOCN_NUMBER, BANK.BANK_MULTI_BANK_STORE_NO from PLOCN2_LOCN LOC, PLOCN2_BANK BANK\r\n" + 
			"WHERE LOC.OCC_ID = BANK.PARENT_ID\r\n" + 
			"AND LOC.LOCN_NUMBER = ?"; 
	
	private static final String UPDATE_BANK = "UPDATE PLOCN2_BANK SET BANK_MULTI_BANK_STORE_NO = ? WHERE OCC_ID = ?";
	
	@Override
	public Bank getBank(String locationNumber, Long bankId) {
		return jdbcTemplate.queryForObject(GET_BANK, new BankMapper(), locationNumber);
	}

	@Override
	public Bank updateBank(Bank bank) {
		Bank bankUpated = null;
		int status = 0;
		try {
			status = jdbcTemplate.update(UPDATE_BANK, bank.getMultibankStoreNumber(), bank.getBankId());
		}catch(Exception exp) {
			exp.printStackTrace();
			bankUpated = new Bank();
			bankUpated.setValidationMessages("Error Message - "+ exp.getClass());
		}
		if(status > 0 ) {
			LOGGER.debug("Bank record is updated successfully !!!!");
			bankUpated = this.getBank(bank.getLocationNumber(), 0L);
		}else {
			LOGGER.debug("Bank record is not updated!!!!");
		}
		return bankUpated;
	}

}
