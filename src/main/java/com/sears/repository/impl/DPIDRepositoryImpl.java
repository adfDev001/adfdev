package com.sears.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sears.mapper.DPIDMapper;
import com.sears.model.DPID;
import com.sears.model.DeptDext;
import com.sears.model.Location;
import com.sears.repository.DPIDRepository;
import com.sears.repository.DeptDextRepository;
import com.sears.repository.LocationRepository;

@Repository
public class DPIDRepositoryImpl implements DPIDRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DPIDRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private DeptDextRepository deptDextRepository;
	
	private static final String GET_DEPARTMENT = "select\r\n" + 
			"LOCN.OCC_ID LOC_ID, \r\n" + 
			"LOCN.LOCN_NUMBER, \r\n" + 
			"DPID.OCC_ID DPID,\r\n" + 
			"DPID.DPID_DEPT_TYPE,\r\n" + 
			"DPID.DPID_SP_DEPARTMENT_NUMBER,\r\n" + 
			"DPID.DPID_OPEN_DATE,\r\n" + 
			"DPID.DPID_CLOSED_DATE,\r\n" + 
			"DPID.DPID_SPECIAL_DIST_MGR_CODE,\r\n" + 
			"DPID.DPID_SPECIAL_RGN_MGR,\r\n" + 
			"DPID.DPID_DEPT_IN_SQ_FT\r\n" + 
			"from PLOCN2_LOCN LOCN, PLOCN2_DPID DPID \r\n" + 
			"WHERE LOCN.OCC_ID = DPID.PARENT_ID\r\n" + 
			"AND  LOCN.LOCN_NUMBER = ? \r\n" + 
			"AND  DPID.DPID_SP_DEPARTMENT_NUMBER = ?";
	
	
	private static final String GET_DEPARTMENT_BY_ID = "select\r\n" + 
			"LOCN.OCC_ID LOC_ID, \r\n" + 
			"LOCN.LOCN_NUMBER, \r\n" + 
			"DPID.OCC_ID DPID,\r\n" + 
			"DPID.DPID_DEPT_TYPE,\r\n" + 
			"DPID.DPID_SP_DEPARTMENT_NUMBER,\r\n" + 
			"DPID.DPID_OPEN_DATE,\r\n" + 
			"DPID.DPID_CLOSED_DATE,\r\n" + 
			"DPID.DPID_SPECIAL_DIST_MGR_CODE,\r\n" + 
			"DPID.DPID_SPECIAL_RGN_MGR,\r\n" + 
			"DPID.DPID_DEPT_IN_SQ_FT\r\n" + 
			"from PLOCN2_LOCN LOCN, PLOCN2_DPID DPID \r\n" + 
			"WHERE LOCN.OCC_ID = DPID.PARENT_ID\r\n" + 
			"AND  DPID.OCC_ID = ?";
	
	private static final String GET_ALL_DEPARTMENT = "select \r\n" + 
			"LOC.OCC_ID AS LOCID, LOC.LOCN_NUMBER,\r\n" + 
			"DPID.OCC_ID AS DPID, \r\n" + 
			"DPID.DPID_SP_DEPARTMENT_NUMBER, \r\n" + 
			"DPID.DPID_DEPT_TYPE, \r\n" + 
			"DPID.DPID_OPEN_DATE,\r\n" + 
			"DPID.DPID_CLOSED_DATE, \r\n" + 
			"DPID.DPID_SPECIAL_DIST_MGR_CODE, \r\n" + 
			"DPID.DPID_SPECIAL_RGN_MGR\r\n" + 
			"from PLOCN2_LOCN LOC, PLOCN2_DPID DPID\r\n" + 
			"where LOC.OCC_ID = DPID.PARENT_ID "
			+ " AND LOC.LOCN_NUMBER = ? "
			+ " order by DPID desc";
	
	private static final String GET_MAX_DPID_ID = "select max(occ_id) as DPID_ID from PLOCN2_DPID";
	
	private static final String INSERT_DPID="INSERT INTO PLOCN2_DPID \r\n" + 
			"(\r\n" + 
			"OCC_ID, \r\n" + 
			"PARENT_ID, \r\n" + 
			"DPID_SP_DEPARTMENT_NUMBER,\r\n" + 
			"DPID_DEPT_TYPE,\r\n" + 
			"DPID_OPEN_DATE,\r\n" + 
			"DPID_CLOSED_DATE,\r\n" + 
			"DPID_SPECIAL_DIST_MGR_CODE,\r\n" + 
			"DPID_SPECIAL_RGN_MGR,\r\n" + 
			"DPID_DEPT_IN_SQ_FT\r\n" + 
			")\r\n" + 
			"VALUES\r\n" + 
			"(?,?,?,?,?,?,?,?,?)";
	
	private static final String UPDATE_DPID = "UPDATE PLOCN2_DPID SET \r\n" + 
			"DPID_DEPT_TYPE = ?, \r\n" + 
			"DPID_OPEN_DATE = ?, \r\n" + 
			"DPID_CLOSED_DATE = ?, \r\n" + 
			"DPID_SPECIAL_DIST_MGR_CODE = ?, \r\n" + 
			"DPID_SPECIAL_RGN_MGR = ?, \r\n" + 
			"DPID_DEPT_IN_SQ_FT = ? \r\n" + 
			"WHERE OCC_ID = ?";
	
	
	@Override
	public DPID getDepartment(String locationNumber, String departmentNumber) {
		LOGGER.debug(" Location Number " + locationNumber +  "departmentNumber" + departmentNumber);
		return jdbcTemplate.queryForObject(GET_DEPARTMENT, new DPIDMapper(), locationNumber, departmentNumber);
	}

	@Override
	public DPID getDepartment(Long dpid) {
		LOGGER.debug("dpid" + dpid);
		return jdbcTemplate.queryForObject(GET_DEPARTMENT_BY_ID, new DPIDMapper(), dpid);
	}

	@Override
	public List<DPID> getAllDepartments(Integer locationNumber) {
		List<DPID> dpids = new ArrayList<>();
		jdbcTemplate.setMaxRows(1000);
		List<Map<String, Object>> resultMap = jdbcTemplate.queryForList(GET_ALL_DEPARTMENT, locationNumber);
		if(resultMap != null && !resultMap.isEmpty()) {
			for (Map<String, Object> map : resultMap) {
				DPID dpid = new DPID();
				dpid.setLocationId(String.valueOf(map.get("LOCID")));
				dpid.setLocationNumber(String.valueOf(map.get("LOCN_NUMBER")));
				dpid.setDepartmentId(Long.valueOf(String.valueOf(map.get("DPID"))));
				dpid.setDepartmentNumber(String.valueOf(map.get("DPID_SP_DEPARTMENT_NUMBER")));
				dpid.setDepartmentType(Integer.valueOf(String.valueOf(map.get("DPID_DEPT_TYPE"))));
				/*try {
					//dpid.setDepartmentOpenDate(DateUtility.getDateFromJulian7(String.valueOf(map.get("DPID_OPEN_DATE"))));
					//dpid.setDepartmentCloseDate(DateUtility.getDateFromJulian7(String.valueOf(map.get("DPID_CLOSED_DATE"))));
				} catch (ParseException e) {
					e.printStackTrace();
				}*/
				dpid.setSpecialDistrictMGRCode(String.valueOf(map.get("DPID_SPECIAL_DIST_MGR_CODE")));
				dpid.setSpecialRegionManager(String.valueOf(map.get("DPID_SPECIAL_RGN_MGR")));
				dpids.add(dpid);
			}
		}
		return dpids;
	}

	@Override
	public DPID createDpid(DPID dpid) {
		DPID dpidCreated = null;
		int status = 0;
		DeptDext  deptDext = null;
		Map<String, Object> retMap = jdbcTemplate.queryForMap(GET_MAX_DPID_ID);
		BigDecimal nextDepartmentId = (BigDecimal) retMap.get("DPID_ID");
		dpid.setDepartmentId(nextDepartmentId.longValue()+1);
		Location location  = locationRepository.getLocation(Long.valueOf(dpid.getLocationNumber()));
		
		//Integer departmentOpenDate = DateUtility.getJulian7FromDate(dpid.getDepartmentOpenDate());
		//Integer departmentCloseDate = DateUtility.getJulian7FromDate(dpid.getDepartmentCloseDate());
		try {
			deptDext = this.deptDextRepository.getDeptDext(Integer.valueOf(dpid.getDepartmentNumber()));
		}catch (EmptyResultDataAccessException exp) {
			LOGGER.error("No DEPT Record is found", exp);
			dpidCreated = new DPID();
			dpidCreated.setValidationMessages("Error Messages - " + "DEPARTMENT NUMBER ENTERED IS NOT ON THE DEPT FILE");
		}
		if(deptDext != null) {
			try {
				status = jdbcTemplate.update(INSERT_DPID, dpid.getDepartmentId(),
						location.getLocationId(),
						dpid.getDepartmentNumber(),
						dpid.getDepartmentType(),
						dpid.getDepartmentOpenDate(),
						dpid.getDepartmentCloseDate(),
						dpid.getSpecialDistrictMGRCode(),
						dpid.getSpecialRegionManager(),
						dpid.getDeptInSquareFeet()
						);
			}catch(DataIntegrityViolationException e) {
				e.printStackTrace();
				dpidCreated = new DPID();
				dpidCreated.setValidationMessages("Error Messages -" + e.getCause());
			}catch(Exception exp) {
				dpidCreated = new DPID();
				dpidCreated.setValidationMessages("Error Messages - " + exp.getCause());
			}
		}else {
			dpidCreated = new DPID();
			dpidCreated.setValidationMessages("Error Messages - " + "DEPARTMENT NUMBER ENTERED IS NOT ON THE DEPT FILE");
		}
		if(status > 0) {
			LOGGER.debug("DPID record is inserted successfully.");
			dpidCreated = this.getDepartment(dpid.getDepartmentId());
		}
		return dpidCreated;
	}

	@Override
	public DPID updateDpid(DPID dpid) {
		DPID dpidUpdated = null;
		int status = 0;
		//Integer departmentOpenDate = DateUtility.getJulian7FromDate(dpid.getDepartmentOpenDate());
		//Integer departmentCloseDate = DateUtility.getJulian7FromDate(dpid.getDepartmentCloseDate());
		
		try {
			status = jdbcTemplate.update(UPDATE_DPID,
					dpid.getDepartmentType(),
					dpid.getDepartmentOpenDate(),
					dpid.getDepartmentCloseDate(),
					dpid.getSpecialDistrictMGRCode(),
					dpid.getSpecialRegionManager(),
					dpid.getDeptInSquareFeet(),
					dpid.getDepartmentId()
					);
		}catch(DataIntegrityViolationException e) {
			e.printStackTrace();
			dpidUpdated = new DPID();
			dpidUpdated.setValidationMessages("" + e.getCause());
		}catch(Exception exp){
			exp.printStackTrace();
			dpidUpdated = new DPID();
			dpidUpdated.setValidationMessages("" + exp.getCause());
		}
		if(status > 0) {
			LOGGER.debug("DPID record is inserted successfully.");
			dpidUpdated = this.getDepartment(dpid.getDepartmentId());
		}
		return dpidUpdated;
	}

}
