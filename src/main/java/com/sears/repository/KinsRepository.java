package com.sears.repository;

import com.sears.model.Kins;

public interface KinsRepository {

	Kins getKins(String locationNumber);
	
	Kins updateKins(Kins kins);

}
