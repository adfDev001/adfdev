package com.sears.model;

public class Bank {

	private Long locationId;
	private String locationNumber;
	private Long bankId;
	private Long multibankStoreNumber;
	private String validationMessages="";

	public Bank() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Long getMultibankStoreNumber() {
		return multibankStoreNumber;
	}

	public void setMultibankStoreNumber(Long multibankStoreNumber) {
		this.multibankStoreNumber = multibankStoreNumber;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}
	
}
