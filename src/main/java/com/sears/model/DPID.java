package com.sears.model;

public class DPID {

	private String locationNumber;
	private String LocationId;
	private String departmentNumber;
	private Long departmentId;
	private Integer departmentType;
	private String departmentOpenDate;
	private String departmentCloseDate;
	private String specialDistrictMGRCode;
	private String specialRegionManager;
	private String deptInSquareFeet;
	private String validationMessages = "";

	public DPID() {
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public String getLocationId() {
		return LocationId;
	}

	public void setLocationId(String locationId) {
		LocationId = locationId;
	}

	public String getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Integer getDepartmentType() {
		return departmentType;
	}

	public void setDepartmentType(Integer departmentType) {
		this.departmentType = departmentType;
	}

	public String getDepartmentOpenDate() {
		return departmentOpenDate;
	}

	public void setDepartmentOpenDate(String departmentOpenDate) {
		this.departmentOpenDate = departmentOpenDate;
	}

	public String getDepartmentCloseDate() {
		return departmentCloseDate;
	}

	public void setDepartmentCloseDate(String departmentCloseDate) {
		this.departmentCloseDate = departmentCloseDate;
	}

	public String getSpecialDistrictMGRCode() {
		return specialDistrictMGRCode;
	}

	public void setSpecialDistrictMGRCode(String specialDistrictMGRCode) {
		this.specialDistrictMGRCode = specialDistrictMGRCode;
	}

	public String getSpecialRegionManager() {
		return specialRegionManager;
	}

	public void setSpecialRegionManager(String specialRegionManager) {
		this.specialRegionManager = specialRegionManager;
	}

	public String getDeptInSquareFeet() {
		return deptInSquareFeet;
	}

	public void setDeptInSquareFeet(String deptInSquareFeet) {
		this.deptInSquareFeet = deptInSquareFeet;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
