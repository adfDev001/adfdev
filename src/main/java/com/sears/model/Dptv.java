package com.sears.model;

import java.math.BigDecimal;

public class Dptv {

	private Long locationId;
	private String locationNumber;
	private Long dpid;
	private String departmentNumber;
	private String openDate;
	private Long dptvId;
	private String keyControl;
	private String changeIndicator;
	private Integer vendorDepartment;
	private Integer vendorOpenDate;
	private Integer vendorCloseDate;
	private Long vendorDunsNumber;
	private Long vendorAccountNumber;
	private BigDecimal combinedFeePercent;
	private BigDecimal splitFeePercent;
	private BigDecimal percentageOwnedByKMART;
	private BigDecimal advertisingFeePercentage;
	private BigDecimal weeklyFloridaTax;
	private String validationMessages="";

	public Dptv() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getDpid() {
		return dpid;
	}

	public void setDpid(Long dpid) {
		this.dpid = dpid;
	}

	public String getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public Long getDptvId() {
		return dptvId;
	}

	public void setDptvId(Long dptvId) {
		this.dptvId = dptvId;
	}

	public String getKeyControl() {
		return keyControl;
	}

	public void setKeyControl(String keyControl) {
		this.keyControl = keyControl;
	}

	public String getChangeIndicator() {
		return changeIndicator;
	}

	public void setChangeIndicator(String changeIndicator) {
		this.changeIndicator = changeIndicator;
	}

	public Integer getVendorDepartment() {
		return vendorDepartment;
	}

	public void setVendorDepartment(Integer vendorDepartment) {
		this.vendorDepartment = vendorDepartment;
	}

	public Integer getVendorOpenDate() {
		return vendorOpenDate;
	}

	public void setVendorOpenDate(Integer vendorOpenDate) {
		this.vendorOpenDate = vendorOpenDate;
	}

	public Integer getVendorCloseDate() {
		return vendorCloseDate;
	}

	public void setVendorCloseDate(Integer vendorCloseDate) {
		this.vendorCloseDate = vendorCloseDate;
	}

	public Long getVendorDunsNumber() {
		return vendorDunsNumber;
	}

	public void setVendorDunsNumber(Long vendorDunsNumber) {
		this.vendorDunsNumber = vendorDunsNumber;
	}

	public Long getVendorAccountNumber() {
		return vendorAccountNumber;
	}

	public void setVendorAccountNumber(Long vendorAccountNumber) {
		this.vendorAccountNumber = vendorAccountNumber;
	}

	public BigDecimal getCombinedFeePercent() {
		return combinedFeePercent;
	}

	public void setCombinedFeePercent(BigDecimal combinedFeePercent) {
		this.combinedFeePercent = combinedFeePercent;
	}

	public BigDecimal getSplitFeePercent() {
		return splitFeePercent;
	}

	public void setSplitFeePercent(BigDecimal splitFeePercent) {
		this.splitFeePercent = splitFeePercent;
	}

	public BigDecimal getPercentageOwnedByKMART() {
		return percentageOwnedByKMART;
	}

	public void setPercentageOwnedByKMART(BigDecimal percentageOwnedByKMART) {
		this.percentageOwnedByKMART = percentageOwnedByKMART;
	}

	public BigDecimal getAdvertisingFeePercentage() {
		return advertisingFeePercentage;
	}

	public void setAdvertisingFeePercentage(BigDecimal advertisingFeePercentage) {
		this.advertisingFeePercentage = advertisingFeePercentage;
	}

	public BigDecimal getWeeklyFloridaTax() {
		return weeklyFloridaTax;
	}

	public void setWeeklyFloridaTax(BigDecimal weeklyFloridaTax) {
		this.weeklyFloridaTax = weeklyFloridaTax;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
