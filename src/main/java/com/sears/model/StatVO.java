package com.sears.model;

import java.math.BigDecimal;

public class StatVO {

	private Long locationId;
	private String locationNumber;
	private Long statId;
	private Long keyFields;
	private Integer fiscalYear;
	private Integer period;
	private Integer previousDayDate;
	private BigDecimal previousBalance;
	private BigDecimal useTaxPurchase;
	private BigDecimal useTaxPaid;

	public StatVO() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getStatId() {
		return statId;
	}

	public void setStatId(Long statId) {
		this.statId = statId;
	}

	public Long getKeyFields() {
		return keyFields;
	}

	public void setKeyFields(Long keyFields) {
		this.keyFields = keyFields;
	}

	public Integer getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Integer getPreviousDayDate() {
		return previousDayDate;
	}

	public void setPreviousDayDate(Integer previousDayDate) {
		this.previousDayDate = previousDayDate;
	}

	public BigDecimal getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(BigDecimal previousBalance) {
		this.previousBalance = previousBalance;
	}

	public BigDecimal getUseTaxPurchase() {
		return useTaxPurchase;
	}

	public void setUseTaxPurchase(BigDecimal useTaxPurchase) {
		this.useTaxPurchase = useTaxPurchase;
	}

	public BigDecimal getUseTaxPaid() {
		return useTaxPaid;
	}

	public void setUseTaxPaid(BigDecimal useTaxPaid) {
		this.useTaxPaid = useTaxPaid;
	}

}
