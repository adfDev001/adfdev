package com.sears.model;

import java.math.BigDecimal;

public class Stat {

	private Long locationId;
	private String locationNumber;
	private Long statId;
	private String keyFields;
	private Integer fiscalYear;
	private Integer period;
	private String statusCode;
	private String statusCode1;
	private String statusCode2;
	private String statusCode3;
	private String statusCode4;
	private String statusCode5;
	private String statusCode6;
	private String statusCode7;
	private String statusCode8;
	private String statusCode9;
	private Integer previousDayDate;
	private BigDecimal previousBalance;
	private BigDecimal weeklyUnBalanceSales;
	private BigDecimal weeklyUnBalanceFunds;
	private String spAdjStatusCode1;
	private String spAdjStatusCode2;
	private String spAdjStatusCode3;
	private String spAdjStatusCode4;
	private String spAdjStatusCode5;
	private String invoiceTranDate;
	private Integer invoiceTranDate605_606;
	private BigDecimal useTaxPurchase;
	private BigDecimal useTaxPaid;
	private BigDecimal merchForStoreUse;
	private String gaCompleteCodes1;
	private String gaCompleteCodes2;
	private String gaCompleteCodes3;
	private String gaCompleteCodes4;
	private String gaCompleteCodes5;
	private String gaCompleteCodes6;
	private String gaCompleteCodes7;
	private String gaCompleteCodes8;
	private String gaCompleteCodes9;
	private String gaCompleteCodes10;
	private String gaCompletedCodes;
	private String weeklyCompletedFlag1;
	private String weeklyCompletedFlag2;
	private String weeklyCompletedFlag3;
	private String weeklyCompletedFlag4;
	private String weeklyCompletedFlag5;
	private String weeklyCompletedFlags;
	private String weeklyProcessFlag1;
	private String weeklyProcessFlag2;
	private String weeklyProcessFlag3;
	private String weeklyProcessFlag4;
	private String weeklyProcessFlag5;
	private String weeklyProcessFlag;
	private String weeklyForceFlag1;
	private String weeklyForceFlag2;
	private String weeklyForceFlag3;
	private String weeklyForceFlag4;
	private String weeklyForceFlag5;
	private String weeklyForceFlag;

	public Stat() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getStatId() {
		return statId;
	}

	public void setStatId(Long statId) {
		this.statId = statId;
	}

	public String getKeyFields() {
		return keyFields;
	}

	public void setKeyFields(String keyFields) {
		this.keyFields = keyFields;
	}

	public Integer getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCode1() {
		return statusCode1;
	}

	public void setStatusCode1(String statusCode1) {
		this.statusCode1 = statusCode1;
	}

	public String getStatusCode2() {
		return statusCode2;
	}

	public void setStatusCode2(String statusCode2) {
		this.statusCode2 = statusCode2;
	}

	public String getStatusCode3() {
		return statusCode3;
	}

	public void setStatusCode3(String statusCode3) {
		this.statusCode3 = statusCode3;
	}

	public String getStatusCode4() {
		return statusCode4;
	}

	public void setStatusCode4(String statusCode4) {
		this.statusCode4 = statusCode4;
	}

	public String getStatusCode5() {
		return statusCode5;
	}

	public void setStatusCode5(String statusCode5) {
		this.statusCode5 = statusCode5;
	}

	public String getStatusCode6() {
		return statusCode6;
	}

	public void setStatusCode6(String statusCode6) {
		this.statusCode6 = statusCode6;
	}

	public String getStatusCode7() {
		return statusCode7;
	}

	public void setStatusCode7(String statusCode7) {
		this.statusCode7 = statusCode7;
	}

	public String getStatusCode8() {
		return statusCode8;
	}

	public void setStatusCode8(String statusCode8) {
		this.statusCode8 = statusCode8;
	}

	public String getStatusCode9() {
		return statusCode9;
	}

	public void setStatusCode9(String statusCode9) {
		this.statusCode9 = statusCode9;
	}

	public Integer getPreviousDayDate() {
		return previousDayDate;
	}

	public void setPreviousDayDate(Integer previousDayDate) {
		this.previousDayDate = previousDayDate;
	}

	public BigDecimal getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(BigDecimal previousBalance) {
		this.previousBalance = previousBalance;
	}

	public BigDecimal getWeeklyUnBalanceSales() {
		return weeklyUnBalanceSales;
	}

	public void setWeeklyUnBalanceSales(BigDecimal weeklyUnBalanceSales) {
		this.weeklyUnBalanceSales = weeklyUnBalanceSales;
	}

	public BigDecimal getWeeklyUnBalanceFunds() {
		return weeklyUnBalanceFunds;
	}

	public void setWeeklyUnBalanceFunds(BigDecimal weeklyUnBalanceFunds) {
		this.weeklyUnBalanceFunds = weeklyUnBalanceFunds;
	}

	public String getSpAdjStatusCode1() {
		return spAdjStatusCode1;
	}

	public void setSpAdjStatusCode1(String spAdjStatusCode1) {
		this.spAdjStatusCode1 = spAdjStatusCode1;
	}

	public String getSpAdjStatusCode2() {
		return spAdjStatusCode2;
	}

	public void setSpAdjStatusCode2(String spAdjStatusCode2) {
		this.spAdjStatusCode2 = spAdjStatusCode2;
	}

	public String getSpAdjStatusCode3() {
		return spAdjStatusCode3;
	}

	public void setSpAdjStatusCode3(String spAdjStatusCode3) {
		this.spAdjStatusCode3 = spAdjStatusCode3;
	}

	public String getSpAdjStatusCode4() {
		return spAdjStatusCode4;
	}

	public void setSpAdjStatusCode4(String spAdjStatusCode4) {
		this.spAdjStatusCode4 = spAdjStatusCode4;
	}

	public String getSpAdjStatusCode5() {
		return spAdjStatusCode5;
	}

	public void setSpAdjStatusCode5(String spAdjStatusCode5) {
		this.spAdjStatusCode5 = spAdjStatusCode5;
	}

	public String getInvoiceTranDate() {
		return invoiceTranDate;
	}

	public void setInvoiceTranDate(String invoiceTranDate) {
		this.invoiceTranDate = invoiceTranDate;
	}

	public Integer getInvoiceTranDate605_606() {
		return invoiceTranDate605_606;
	}

	public void setInvoiceTranDate605_606(Integer invoiceTranDate605_606) {
		this.invoiceTranDate605_606 = invoiceTranDate605_606;
	}

	public BigDecimal getUseTaxPurchase() {
		return useTaxPurchase;
	}

	public void setUseTaxPurchase(BigDecimal useTaxPurchase) {
		this.useTaxPurchase = useTaxPurchase;
	}

	public BigDecimal getUseTaxPaid() {
		return useTaxPaid;
	}

	public void setUseTaxPaid(BigDecimal useTaxPaid) {
		this.useTaxPaid = useTaxPaid;
	}

	public BigDecimal getMerchForStoreUse() {
		return merchForStoreUse;
	}

	public void setMerchForStoreUse(BigDecimal merchForStoreUse) {
		this.merchForStoreUse = merchForStoreUse;
	}

	public String getGaCompleteCodes1() {
		return gaCompleteCodes1;
	}

	public void setGaCompleteCodes1(String gaCompleteCodes1) {
		this.gaCompleteCodes1 = gaCompleteCodes1;
	}

	public String getGaCompleteCodes2() {
		return gaCompleteCodes2;
	}

	public void setGaCompleteCodes2(String gaCompleteCodes2) {
		this.gaCompleteCodes2 = gaCompleteCodes2;
	}

	public String getGaCompleteCodes3() {
		return gaCompleteCodes3;
	}

	public void setGaCompleteCodes3(String gaCompleteCodes3) {
		this.gaCompleteCodes3 = gaCompleteCodes3;
	}

	public String getGaCompleteCodes4() {
		return gaCompleteCodes4;
	}

	public void setGaCompleteCodes4(String gaCompleteCodes4) {
		this.gaCompleteCodes4 = gaCompleteCodes4;
	}

	public String getGaCompleteCodes5() {
		return gaCompleteCodes5;
	}

	public void setGaCompleteCodes5(String gaCompleteCodes5) {
		this.gaCompleteCodes5 = gaCompleteCodes5;
	}

	public String getGaCompleteCodes6() {
		return gaCompleteCodes6;
	}

	public void setGaCompleteCodes6(String gaCompleteCodes6) {
		this.gaCompleteCodes6 = gaCompleteCodes6;
	}

	public String getGaCompleteCodes7() {
		return gaCompleteCodes7;
	}

	public void setGaCompleteCodes7(String gaCompleteCodes7) {
		this.gaCompleteCodes7 = gaCompleteCodes7;
	}

	public String getGaCompleteCodes8() {
		return gaCompleteCodes8;
	}

	public void setGaCompleteCodes8(String gaCompleteCodes8) {
		this.gaCompleteCodes8 = gaCompleteCodes8;
	}

	public String getGaCompleteCodes9() {
		return gaCompleteCodes9;
	}

	public void setGaCompleteCodes9(String gaCompleteCodes9) {
		this.gaCompleteCodes9 = gaCompleteCodes9;
	}

	public String getGaCompleteCodes10() {
		return gaCompleteCodes10;
	}

	public void setGaCompleteCodes10(String gaCompleteCodes10) {
		this.gaCompleteCodes10 = gaCompleteCodes10;
	}

	public String getGaCompletedCodes() {
		return gaCompletedCodes;
	}

	public void setGaCompletedCodes(String gaCompletedCodes) {
		this.gaCompletedCodes = gaCompletedCodes;
	}

	public String getWeeklyCompletedFlag1() {
		return weeklyCompletedFlag1;
	}

	public void setWeeklyCompletedFlag1(String weeklyCompletedFlag1) {
		this.weeklyCompletedFlag1 = weeklyCompletedFlag1;
	}

	public String getWeeklyCompletedFlag2() {
		return weeklyCompletedFlag2;
	}

	public void setWeeklyCompletedFlag2(String weeklyCompletedFlag2) {
		this.weeklyCompletedFlag2 = weeklyCompletedFlag2;
	}

	public String getWeeklyCompletedFlag3() {
		return weeklyCompletedFlag3;
	}

	public void setWeeklyCompletedFlag3(String weeklyCompletedFlag3) {
		this.weeklyCompletedFlag3 = weeklyCompletedFlag3;
	}

	public String getWeeklyCompletedFlag4() {
		return weeklyCompletedFlag4;
	}

	public void setWeeklyCompletedFlag4(String weeklyCompletedFlag4) {
		this.weeklyCompletedFlag4 = weeklyCompletedFlag4;
	}

	public String getWeeklyCompletedFlag5() {
		return weeklyCompletedFlag5;
	}

	public void setWeeklyCompletedFlag5(String weeklyCompletedFlag5) {
		this.weeklyCompletedFlag5 = weeklyCompletedFlag5;
	}

	public String getWeeklyCompletedFlags() {
		return weeklyCompletedFlags;
	}

	public void setWeeklyCompletedFlags(String weeklyCompletedFlags) {
		this.weeklyCompletedFlags = weeklyCompletedFlags;
	}

	public String getWeeklyProcessFlag1() {
		return weeklyProcessFlag1;
	}

	public void setWeeklyProcessFlag1(String weeklyProcessFlag1) {
		this.weeklyProcessFlag1 = weeklyProcessFlag1;
	}

	public String getWeeklyProcessFlag2() {
		return weeklyProcessFlag2;
	}

	public void setWeeklyProcessFlag2(String weeklyProcessFlag2) {
		this.weeklyProcessFlag2 = weeklyProcessFlag2;
	}

	public String getWeeklyProcessFlag3() {
		return weeklyProcessFlag3;
	}

	public void setWeeklyProcessFlag3(String weeklyProcessFlag3) {
		this.weeklyProcessFlag3 = weeklyProcessFlag3;
	}

	public String getWeeklyProcessFlag4() {
		return weeklyProcessFlag4;
	}

	public void setWeeklyProcessFlag4(String weeklyProcessFlag4) {
		this.weeklyProcessFlag4 = weeklyProcessFlag4;
	}

	public String getWeeklyProcessFlag5() {
		return weeklyProcessFlag5;
	}

	public void setWeeklyProcessFlag5(String weeklyProcessFlag5) {
		this.weeklyProcessFlag5 = weeklyProcessFlag5;
	}

	public String getWeeklyProcessFlag() {
		return weeklyProcessFlag;
	}

	public void setWeeklyProcessFlag(String weeklyProcessFlag) {
		this.weeklyProcessFlag = weeklyProcessFlag;
	}

	public String getWeeklyForceFlag1() {
		return weeklyForceFlag1;
	}

	public void setWeeklyForceFlag1(String weeklyForceFlag1) {
		this.weeklyForceFlag1 = weeklyForceFlag1;
	}

	public String getWeeklyForceFlag2() {
		return weeklyForceFlag2;
	}

	public void setWeeklyForceFlag2(String weeklyForceFlag2) {
		this.weeklyForceFlag2 = weeklyForceFlag2;
	}

	public String getWeeklyForceFlag3() {
		return weeklyForceFlag3;
	}

	public void setWeeklyForceFlag3(String weeklyForceFlag3) {
		this.weeklyForceFlag3 = weeklyForceFlag3;
	}

	public String getWeeklyForceFlag4() {
		return weeklyForceFlag4;
	}

	public void setWeeklyForceFlag4(String weeklyForceFlag4) {
		this.weeklyForceFlag4 = weeklyForceFlag4;
	}

	public String getWeeklyForceFlag5() {
		return weeklyForceFlag5;
	}

	public void setWeeklyForceFlag5(String weeklyForceFlag5) {
		this.weeklyForceFlag5 = weeklyForceFlag5;
	}

	public String getWeeklyForceFlag() {
		return weeklyForceFlag;
	}

	public void setWeeklyForceFlag(String weeklyForceFlag) {
		this.weeklyForceFlag = weeklyForceFlag;
	}

}
