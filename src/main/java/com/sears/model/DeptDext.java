package com.sears.model;

public class DeptDext {

	private Long departmentId;
	private Long deptId;
	private Long dextId;
	private Integer departmentNumber;
	private String departmentName;
	private String departmentProductCode;
	private String dateDepartmentAdded;
	private String departmentPhaseOutDate;
	private String termEntryLevel;
	private String shcReportLevel;
	private String corpSummLevel;
	private String salesTermEntryLevel;
	private String salesShcReportLevel;
	private String salesCorpSummLevel;
	private String storeUseCode;
	private String shcOperCode;
	private String otherOperCode;
	private String assocOperCode;
	private String specialityCode;
	private String cashSalesCode;
	private String payrollCode;
	private String retailCode;
	private String ownerCode;
	private String groupCode;
	private String overHeadCode;
	private String inventoriedDepartment;
	private String accountPayableCode;
	private String reportingDept;
	private String costOnlyDept;
	private String accountRequired;
	private String associateDiscount;
	private String foodStampsEbt;
	private String purchaseOrderCode;
	private String validationMessages="";

	public DeptDext() {
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public Long getDextId() {
		return dextId;
	}

	public void setDextId(Long dextId) {
		this.dextId = dextId;
	}

	public Integer getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(Integer departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentProductCode() {
		return departmentProductCode;
	}

	public void setDepartmentProductCode(String departmentProductCode) {
		this.departmentProductCode = departmentProductCode;
	}

	public String getDateDepartmentAdded() {
		return dateDepartmentAdded;
	}

	public void setDateDepartmentAdded(String dateDepartmentAdded) {
		this.dateDepartmentAdded = dateDepartmentAdded;
	}

	public String getDepartmentPhaseOutDate() {
		return departmentPhaseOutDate;
	}

	public void setDepartmentPhaseOutDate(String departmentPhaseOutDate) {
		this.departmentPhaseOutDate = departmentPhaseOutDate;
	}

	public String getTermEntryLevel() {
		return termEntryLevel;
	}

	public void setTermEntryLevel(String termEntryLevel) {
		this.termEntryLevel = termEntryLevel;
	}

	public String getShcReportLevel() {
		return shcReportLevel;
	}

	public void setShcReportLevel(String shcReportLevel) {
		this.shcReportLevel = shcReportLevel;
	}

	public String getCorpSummLevel() {
		return corpSummLevel;
	}

	public void setCorpSummLevel(String corpSummLevel) {
		this.corpSummLevel = corpSummLevel;
	}

	public String getSalesTermEntryLevel() {
		return salesTermEntryLevel;
	}

	public void setSalesTermEntryLevel(String salesTermEntryLevel) {
		this.salesTermEntryLevel = salesTermEntryLevel;
	}

	public String getSalesShcReportLevel() {
		return salesShcReportLevel;
	}

	public void setSalesShcReportLevel(String salesShcReportLevel) {
		this.salesShcReportLevel = salesShcReportLevel;
	}

	public String getSalesCorpSummLevel() {
		return salesCorpSummLevel;
	}

	public void setSalesCorpSummLevel(String salesCorpSummLevel) {
		this.salesCorpSummLevel = salesCorpSummLevel;
	}

	public String getStoreUseCode() {
		return storeUseCode;
	}

	public void setStoreUseCode(String storeUseCode) {
		this.storeUseCode = storeUseCode;
	}

	public String getShcOperCode() {
		return shcOperCode;
	}

	public void setShcOperCode(String shcOperCode) {
		this.shcOperCode = shcOperCode;
	}

	public String getOtherOperCode() {
		return otherOperCode;
	}

	public void setOtherOperCode(String otherOperCode) {
		this.otherOperCode = otherOperCode;
	}

	public String getAssocOperCode() {
		return assocOperCode;
	}

	public void setAssocOperCode(String assocOperCode) {
		this.assocOperCode = assocOperCode;
	}

	public String getSpecialityCode() {
		return specialityCode;
	}

	public void setSpecialityCode(String specialityCode) {
		this.specialityCode = specialityCode;
	}

	public String getCashSalesCode() {
		return cashSalesCode;
	}

	public void setCashSalesCode(String cashSalesCode) {
		this.cashSalesCode = cashSalesCode;
	}

	public String getPayrollCode() {
		return payrollCode;
	}

	public void setPayrollCode(String payrollCode) {
		this.payrollCode = payrollCode;
	}

	public String getRetailCode() {
		return retailCode;
	}

	public void setRetailCode(String retailCode) {
		this.retailCode = retailCode;
	}

	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getOverHeadCode() {
		return overHeadCode;
	}

	public void setOverHeadCode(String overHeadCode) {
		this.overHeadCode = overHeadCode;
	}

	public String getInventoriedDepartment() {
		return inventoriedDepartment;
	}

	public void setInventoriedDepartment(String inventoriedDepartment) {
		this.inventoriedDepartment = inventoriedDepartment;
	}

	public String getAccountPayableCode() {
		return accountPayableCode;
	}

	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	public String getReportingDept() {
		return reportingDept;
	}

	public void setReportingDept(String reportingDept) {
		this.reportingDept = reportingDept;
	}

	public String getCostOnlyDept() {
		return costOnlyDept;
	}

	public void setCostOnlyDept(String costOnlyDept) {
		this.costOnlyDept = costOnlyDept;
	}

	public String getAccountRequired() {
		return accountRequired;
	}

	public void setAccountRequired(String accountRequired) {
		this.accountRequired = accountRequired;
	}

	public String getAssociateDiscount() {
		return associateDiscount;
	}

	public void setAssociateDiscount(String associateDiscount) {
		this.associateDiscount = associateDiscount;
	}

	public String getFoodStampsEbt() {
		return foodStampsEbt;
	}

	public void setFoodStampsEbt(String foodStampsEbt) {
		this.foodStampsEbt = foodStampsEbt;
	}

	public String getPurchaseOrderCode() {
		return purchaseOrderCode;
	}

	public void setPurchaseOrderCode(String purchaseOrderCode) {
		this.purchaseOrderCode = purchaseOrderCode;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
