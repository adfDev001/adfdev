package com.sears.model;

public class Real {

	private Long locationId;
	private String locationNumber;
	private Long realId;
	private String leasedOwnedCode;
	private Integer originalLeasedDate;
	private Integer firstLeaseRenewalDate;
	private Integer secondLeaseRenewalDate;
	private Integer thirdLeaseRenewalDate;
	private String validationMessages="";

	public Real() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getRealId() {
		return realId;
	}

	public void setRealId(Long realId) {
		this.realId = realId;
	}

	public String getLeasedOwnedCode() {
		return leasedOwnedCode;
	}

	public void setLeasedOwnedCode(String leasedOwnedCode) {
		this.leasedOwnedCode = leasedOwnedCode;
	}

	public Integer getOriginalLeasedDate() {
		return originalLeasedDate;
	}

	public void setOriginalLeasedDate(Integer originalLeasedDate) {
		this.originalLeasedDate = originalLeasedDate;
	}

	public Integer getFirstLeaseRenewalDate() {
		return firstLeaseRenewalDate;
	}

	public void setFirstLeaseRenewalDate(Integer firstLeaseRenewalDate) {
		this.firstLeaseRenewalDate = firstLeaseRenewalDate;
	}

	public Integer getSecondLeaseRenewalDate() {
		return secondLeaseRenewalDate;
	}

	public void setSecondLeaseRenewalDate(Integer secondLeaseRenewalDate) {
		this.secondLeaseRenewalDate = secondLeaseRenewalDate;
	}

	public Integer getThirdLeaseRenewalDate() {
		return thirdLeaseRenewalDate;
	}

	public void setThirdLeaseRenewalDate(Integer thirdLeaseRenewalDate) {
		this.thirdLeaseRenewalDate = thirdLeaseRenewalDate;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
