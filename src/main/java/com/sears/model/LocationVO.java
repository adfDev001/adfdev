package com.sears.model;

public class LocationVO {

	private Long occId;

	private Long locationNumber;

	private Long actualOpenDate;

	private Long plannedOpenDate;

	private String streetAddress;

	private String city;

	private String state;

	private Integer closeDate;

	private Integer originalFacility;

	private Integer formatType;

	private String subType;

	private String timeZone;

	public Long getOccId() {
		return occId;
	}

	public void setOccId(Long occId) {
		this.occId = occId;
	}

	public Long getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(Long locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Long getActualOpenDate() {
		return actualOpenDate;
	}

	public void setActualOpenDate(Long actualOpenDate) {
		this.actualOpenDate = actualOpenDate;
	}

	public Long getPlannedOpenDate() {
		return plannedOpenDate;
	}

	public void setPlannedOpenDate(Long plannedOpenDate) {
		this.plannedOpenDate = plannedOpenDate;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Integer closeDate) {
		this.closeDate = closeDate;
	}

	public Integer getOriginalFacility() {
		return originalFacility;
	}

	public void setOriginalFacility(Integer originalFacility) {
		this.originalFacility = originalFacility;
	}

	public Integer getFormatType() {
		return formatType;
	}

	public void setFormatType(Integer formatType) {
		this.formatType = formatType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
