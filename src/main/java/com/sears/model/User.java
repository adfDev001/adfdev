package com.sears.model;

public class User {

	private Long userId;
	private String userName;
	private String locationSegmentAccess;
	private String dpidSegmentAccess;
	private String dptvSegmentAccess;
	private String deptDextSegmentAccess;
	private String statSegmentAccess;
	private String realSegmentAccess;
	private String bankSegmentAccess;
	private String kinsSegmentAccess;
	private String adminSegmentAccess;
	private String action;
	private String validationMessages="";

	public User() {
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLocationSegmentAccess() {
		return locationSegmentAccess;
	}

	public void setLocationSegmentAccess(String locationSegmentAccess) {
		this.locationSegmentAccess = locationSegmentAccess;
	}

	public String getDpidSegmentAccess() {
		return dpidSegmentAccess;
	}

	public void setDpidSegmentAccess(String dpidSegmentAccess) {
		this.dpidSegmentAccess = dpidSegmentAccess;
	}

	public String getDptvSegmentAccess() {
		return dptvSegmentAccess;
	}

	public void setDptvSegmentAccess(String dptvSegmentAccess) {
		this.dptvSegmentAccess = dptvSegmentAccess;
	}

	public String getDeptDextSegmentAccess() {
		return deptDextSegmentAccess;
	}

	public void setDeptDextSegmentAccess(String deptDextSegmentAccess) {
		this.deptDextSegmentAccess = deptDextSegmentAccess;
	}

	public String getStatSegmentAccess() {
		return statSegmentAccess;
	}

	public void setStatSegmentAccess(String statSegmentAccess) {
		this.statSegmentAccess = statSegmentAccess;
	}

	public String getRealSegmentAccess() {
		return realSegmentAccess;
	}

	public void setRealSegmentAccess(String realSegmentAccess) {
		this.realSegmentAccess = realSegmentAccess;
	}

	public String getBankSegmentAccess() {
		return bankSegmentAccess;
	}

	public void setBankSegmentAccess(String bankSegmentAccess) {
		this.bankSegmentAccess = bankSegmentAccess;
	}

	public String getKinsSegmentAccess() {
		return kinsSegmentAccess;
	}

	public void setKinsSegmentAccess(String kinsSegmentAccess) {
		this.kinsSegmentAccess = kinsSegmentAccess;
	}

	public String getAdminSegmentAccess() {
		return adminSegmentAccess;
	}

	public void setAdminSegmentAccess(String adminSegmentAccess) {
		this.adminSegmentAccess = adminSegmentAccess;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
