package com.sears.model;

public class Location {

	private Long locationId;

	private Long locationLextId;

	private Long locationNumber;

	private String streetAddress;

	private String city;

	private String state;

	private String zipCode;

	private String zipCodeExtension;

	private String storePhone;

	private String county;

	private String shoppingCenter;

	private String shippingAddressFlag;

	private String overHeadName;

	private String shippingStreetAddress;

	private String shipCity;

	private String shipState;

	private String shipZipCode;

	private String shipZipCodeExtension;

	private String faxNumber;

	private String timeZone;

	private String latitude;

	private String langitude;

	private String districtLevel1;

	private String districtLevel2;

	private String summaryLevel3;

	private String managerName;

	private String regionalManager;

	private String areaMerchCoord;

	private String originalNumber;

	private String naiNumber;

	private String searUnitNumber;

	private String originalKmartNumber;

	private String originalFacility;

	private String rptingLocationNumber;

	private String btftLocationNumber;

	private String recievingStoreNumber;

	private String devisionCode;

	private String typeCode;

	private String regionCode;

	private String formatType;

	private String formatSubType;

	private String formatModifier;

	private String companyCode;

	private String climaticZone;

	private String mdseAreaRotoZone;

	private String upsZone;

	private String stateNumber;

	private String cityNumber;

	private String countyNumber;

	private String planedOpenDate;

	private String actualOpenDate;

	private String closeDate;

	private String temporaryCloseDate;

	private String reOpenDate;

	private String plannedCloseDate;

	private String holidayCloseDate1;

	private String holidayCloseDate2;

	private String holidayCloseDate3;

	private String holidayCloseDate4;

	private String holidayCloseDate5;

	private String holidayCloseDate6;

	private String holidayCloseDate7;

	private String holidayCloseDate8;

	private String holidayCloseDate9;

	private String holidayCloseDate10;

	private String totalSQFootage;

	private String sellingAreaSqFoot;

	private String terminalCode;

	private String terminalDate;

	private String validationMessages="";

	public Location() {
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getLocationLextId() {
		return locationLextId;
	}

	public void setLocationLextId(Long locationLextId) {
		this.locationLextId = locationLextId;
	}

	public Long getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(Long locationNumber) {
		this.locationNumber = locationNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getZipCodeExtension() {
		return zipCodeExtension;
	}

	public void setZipCodeExtension(String zipCodeExtension) {
		this.zipCodeExtension = zipCodeExtension;
	}

	public String getStorePhone() {
		return storePhone;
	}

	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getShoppingCenter() {
		return shoppingCenter;
	}

	public void setShoppingCenter(String shoppingCenter) {
		this.shoppingCenter = shoppingCenter;
	}

	public String getShippingAddressFlag() {
		return shippingAddressFlag;
	}

	public void setShippingAddressFlag(String shippingAddressFlag) {
		this.shippingAddressFlag = shippingAddressFlag;
	}

	public String getOverHeadName() {
		return overHeadName;
	}

	public void setOverHeadName(String overHeadName) {
		this.overHeadName = overHeadName;
	}

	public String getShippingStreetAddress() {
		return shippingStreetAddress;
	}

	public void setShippingStreetAddress(String shippingStreetAddress) {
		this.shippingStreetAddress = shippingStreetAddress;
	}

	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipState() {
		return shipState;
	}

	public void setShipState(String shipState) {
		this.shipState = shipState;
	}

	public String getShipZipCode() {
		return shipZipCode;
	}

	public void setShipZipCode(String shipZipCode) {
		this.shipZipCode = shipZipCode;
	}

	public String getShipZipCodeExtension() {
		return shipZipCodeExtension;
	}

	public void setShipZipCodeExtension(String shipZipCodeExtension) {
		this.shipZipCodeExtension = shipZipCodeExtension;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLangitude() {
		return langitude;
	}

	public void setLangitude(String langitude) {
		this.langitude = langitude;
	}

	public String getDistrictLevel1() {
		return districtLevel1;
	}

	public void setDistrictLevel1(String districtLevel1) {
		this.districtLevel1 = districtLevel1;
	}

	public String getDistrictLevel2() {
		return districtLevel2;
	}

	public void setDistrictLevel2(String districtLevel2) {
		this.districtLevel2 = districtLevel2;
	}

	public String getSummaryLevel3() {
		return summaryLevel3;
	}

	public void setSummaryLevel3(String summaryLevel3) {
		this.summaryLevel3 = summaryLevel3;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getRegionalManager() {
		return regionalManager;
	}

	public void setRegionalManager(String regionalManager) {
		this.regionalManager = regionalManager;
	}

	public String getAreaMerchCoord() {
		return areaMerchCoord;
	}

	public void setAreaMerchCoord(String areaMerchCoord) {
		this.areaMerchCoord = areaMerchCoord;
	}

	public String getOriginalNumber() {
		return originalNumber;
	}

	public void setOriginalNumber(String originalNumber) {
		this.originalNumber = originalNumber;
	}

	public String getNaiNumber() {
		return naiNumber;
	}

	public void setNaiNumber(String naiNumber) {
		this.naiNumber = naiNumber;
	}

	public String getSearUnitNumber() {
		return searUnitNumber;
	}

	public void setSearUnitNumber(String searUnitNumber) {
		this.searUnitNumber = searUnitNumber;
	}

	public String getOriginalKmartNumber() {
		return originalKmartNumber;
	}

	public void setOriginalKmartNumber(String originalKmartNumber) {
		this.originalKmartNumber = originalKmartNumber;
	}

	public String getOriginalFacility() {
		return originalFacility;
	}

	public void setOriginalFacility(String originalFacility) {
		this.originalFacility = originalFacility;
	}

	public String getRptingLocationNumber() {
		return rptingLocationNumber;
	}

	public void setRptingLocationNumber(String rptingLocationNumber) {
		this.rptingLocationNumber = rptingLocationNumber;
	}

	public String getBtftLocationNumber() {
		return btftLocationNumber;
	}

	public void setBtftLocationNumber(String btftLocationNumber) {
		this.btftLocationNumber = btftLocationNumber;
	}

	public String getRecievingStoreNumber() {
		return recievingStoreNumber;
	}

	public void setRecievingStoreNumber(String recievingStoreNumber) {
		this.recievingStoreNumber = recievingStoreNumber;
	}

	public String getDevisionCode() {
		return devisionCode;
	}

	public void setDevisionCode(String devisionCode) {
		this.devisionCode = devisionCode;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	public String getFormatSubType() {
		return formatSubType;
	}

	public void setFormatSubType(String formatSubType) {
		this.formatSubType = formatSubType;
	}

	public String getFormatModifier() {
		return formatModifier;
	}

	public void setFormatModifier(String formatModifier) {
		this.formatModifier = formatModifier;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getClimaticZone() {
		return climaticZone;
	}

	public void setClimaticZone(String climaticZone) {
		this.climaticZone = climaticZone;
	}

	public String getMdseAreaRotoZone() {
		return mdseAreaRotoZone;
	}

	public void setMdseAreaRotoZone(String mdseAreaRotoZone) {
		this.mdseAreaRotoZone = mdseAreaRotoZone;
	}

	public String getUpsZone() {
		return upsZone;
	}

	public void setUpsZone(String upsZone) {
		this.upsZone = upsZone;
	}

	public String getStateNumber() {
		return stateNumber;
	}

	public void setStateNumber(String stateNumber) {
		this.stateNumber = stateNumber;
	}

	public String getCityNumber() {
		return cityNumber;
	}

	public void setCityNumber(String cityNumber) {
		this.cityNumber = cityNumber;
	}

	public String getCountyNumber() {
		return countyNumber;
	}

	public void setCountyNumber(String countyNumber) {
		this.countyNumber = countyNumber;
	}

	public String getPlanedOpenDate() {
		return planedOpenDate;
	}

	public void setPlanedOpenDate(String planedOpenDate) {
		this.planedOpenDate = planedOpenDate;
	}

	public String getActualOpenDate() {
		return actualOpenDate;
	}

	public void setActualOpenDate(String actualOpenDate) {
		this.actualOpenDate = actualOpenDate;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public String getTemporaryCloseDate() {
		return temporaryCloseDate;
	}

	public void setTemporaryCloseDate(String temporaryCloseDate) {
		this.temporaryCloseDate = temporaryCloseDate;
	}

	public String getReOpenDate() {
		return reOpenDate;
	}

	public void setReOpenDate(String reOpenDate) {
		this.reOpenDate = reOpenDate;
	}

	public String getPlannedCloseDate() {
		return plannedCloseDate;
	}

	public void setPlannedCloseDate(String plannedCloseDate) {
		this.plannedCloseDate = plannedCloseDate;
	}

	public String getHolidayCloseDate1() {
		return holidayCloseDate1;
	}

	public void setHolidayCloseDate1(String holidayCloseDate1) {
		this.holidayCloseDate1 = holidayCloseDate1;
	}

	public String getHolidayCloseDate2() {
		return holidayCloseDate2;
	}

	public void setHolidayCloseDate2(String holidayCloseDate2) {
		this.holidayCloseDate2 = holidayCloseDate2;
	}

	public String getHolidayCloseDate3() {
		return holidayCloseDate3;
	}

	public void setHolidayCloseDate3(String holidayCloseDate3) {
		this.holidayCloseDate3 = holidayCloseDate3;
	}

	public String getHolidayCloseDate4() {
		return holidayCloseDate4;
	}

	public void setHolidayCloseDate4(String holidayCloseDate4) {
		this.holidayCloseDate4 = holidayCloseDate4;
	}

	public String getHolidayCloseDate5() {
		return holidayCloseDate5;
	}

	public void setHolidayCloseDate5(String holidayCloseDate5) {
		this.holidayCloseDate5 = holidayCloseDate5;
	}

	public String getHolidayCloseDate6() {
		return holidayCloseDate6;
	}

	public void setHolidayCloseDate6(String holidayCloseDate6) {
		this.holidayCloseDate6 = holidayCloseDate6;
	}

	public String getHolidayCloseDate7() {
		return holidayCloseDate7;
	}

	public void setHolidayCloseDate7(String holidayCloseDate7) {
		this.holidayCloseDate7 = holidayCloseDate7;
	}

	public String getHolidayCloseDate8() {
		return holidayCloseDate8;
	}

	public void setHolidayCloseDate8(String holidayCloseDate8) {
		this.holidayCloseDate8 = holidayCloseDate8;
	}

	public String getHolidayCloseDate9() {
		return holidayCloseDate9;
	}

	public void setHolidayCloseDate9(String holidayCloseDate9) {
		this.holidayCloseDate9 = holidayCloseDate9;
	}

	public String getHolidayCloseDate10() {
		return holidayCloseDate10;
	}

	public void setHolidayCloseDate10(String holidayCloseDate10) {
		this.holidayCloseDate10 = holidayCloseDate10;
	}

	public String getTotalSQFootage() {
		return totalSQFootage;
	}

	public void setTotalSQFootage(String totalSQFootage) {
		this.totalSQFootage = totalSQFootage;
	}

	public String getSellingAreaSqFoot() {
		return sellingAreaSqFoot;
	}

	public void setSellingAreaSqFoot(String sellingAreaSqFoot) {
		this.sellingAreaSqFoot = sellingAreaSqFoot;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getTerminalDate() {
		return terminalDate;
	}

	public void setTerminalDate(String terminalDate) {
		this.terminalDate = terminalDate;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}

}
