package com.sears.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.User;
import com.sears.services.AdminService;

@Controller
public class AdminController {
	
	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/adminpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("adminpage");
    	return mv;
    }
	
	@RequestMapping(value = "/admincreate", method = RequestMethod.GET)
    public ModelAndView adminCreate() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("admincreate");
    	return mv;
    }
	
	@RequestMapping(value = "/adminupdate", method = RequestMethod.GET)
    public ModelAndView adminUpdate(String userName) {
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("USER_VIEW", getUserDetails(userName));
    	mv.setViewName("adminupdate");
    	return mv;
    }
	
	private User getUserDetails(String userName) {
		return adminService.getUser(userName);
	}
	
	@RequestMapping(value = "/data/admin/getAllUsers/{offset}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<List<User>> getAllUser(@PathVariable Integer offset, @PathVariable Integer limit){
		List<User> users= adminService.getAllUsers(offset, limit);
    	return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/admin/getUser/{userId}", method = RequestMethod.GET)
	public ResponseEntity<User> getUserById(@PathVariable Long userId){
		User user= adminService.getUserById(userId);
    	return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/admin/createUser", method = RequestMethod.POST)
	public ResponseEntity<User> createUser(@RequestBody User user){
		User userCreated = adminService.createUser(user); 
    	return new ResponseEntity<User>(userCreated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/admin/updateUser", method = RequestMethod.POST)
	public ResponseEntity<User> updateUser(@RequestBody User user){
		User userUpdated = adminService.updateUser(user); 
    	return new ResponseEntity<User>(userUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/admin/deleteUser", method = RequestMethod.POST)
	public ResponseEntity<User> deleteUser(@RequestBody User user){
		User userDeleted = adminService.deleteUser(user);
    	return new ResponseEntity<User>(userDeleted, HttpStatus.OK);
	}
}
