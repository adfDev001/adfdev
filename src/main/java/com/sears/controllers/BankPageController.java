package com.sears.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Bank;
import com.sears.services.BankService;

@Controller
public class BankPageController {
	
	@Autowired
	private BankService bankService;
	
	@RequestMapping(value = "/bankpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("bankpage");
    	return mv;
    }
	
	@RequestMapping("/data/bank/getBank/{locationNumber}")
	public ResponseEntity<Bank> getBank(@PathVariable String locationNumber) {
		Bank bank = bankService.getBank(locationNumber, 0L);
		return new ResponseEntity<Bank>(bank, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/bank/updateBank", method=RequestMethod.POST)
	public ResponseEntity<Bank> updateBank(@RequestBody Bank bank) {
		Bank bankUpdated = bankService.updateBank(bank);
		return new ResponseEntity<Bank>(bankUpdated, HttpStatus.OK);
	}
}
