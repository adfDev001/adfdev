package com.sears.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Real;
import com.sears.services.RealService;

@Controller
public class RealPageController {

	@RequestMapping(value = "/realpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("realpage");
    	return mv;
    }
	
	@Autowired
	private RealService realService;
	
	@RequestMapping("/data/real/getReal/{locationNumber}")
	public ResponseEntity<Real> getReal(@PathVariable String locationNumber) {
		Real real = realService.getReal(locationNumber, "");
		return new ResponseEntity<Real>(real, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/real/createReal", method=RequestMethod.POST)
	public ResponseEntity<Real> createReal(@RequestBody Real real) {
		Real realCreated = realService.createReal(real);
		return new ResponseEntity<Real>(realCreated, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/real/updateReal", method=RequestMethod.POST)
	public ResponseEntity<Real> updateReal(@RequestBody Real real) {
		Real realUpdated = realService.updateReal(real);
		return new ResponseEntity<Real>(realUpdated, HttpStatus.OK);
	}
	
}
