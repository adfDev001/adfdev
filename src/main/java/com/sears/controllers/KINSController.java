package com.sears.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Kins;
import com.sears.services.KinsService;

@Controller
public class KINSController {

	@Autowired
	private KinsService kinsService;

	@RequestMapping(value = "/kinspage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("users", "Welcome User");
    	mv.setViewName("kinspage");
    	return mv;
    }
	
	@RequestMapping("/data/kins/getKins/{locationNumber}")
	public ResponseEntity<Kins> getKins(@PathVariable String locationNumber) {
		Kins kins = kinsService.getKins(locationNumber);
		return new ResponseEntity<Kins>(kins, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/kins/updateKins", method=RequestMethod.POST)
	public ResponseEntity<Kins> updateKins(@RequestBody Kins kins) {
		Kins kinsUpdated = kinsService.updateKins(kins);
		return new ResponseEntity<Kins>(kinsUpdated, HttpStatus.OK);
	}
}
