package com.sears.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Stat;
import com.sears.model.StatVO;
import com.sears.services.StatService;

@Controller
public class StatController {

	@Autowired
	private StatService statService;
	
	@RequestMapping(value = "/statpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("statpage");
    	return mv;
    }
	
	@RequestMapping("/data/stat/getStat/{locationNumber}/{keyFields}")
	public ResponseEntity<Stat> getStat(@PathVariable String locationNumber, @PathVariable Integer keyFields) {
		Stat stat = statService.getStat(locationNumber, keyFields);
		return new ResponseEntity<Stat>(stat, HttpStatus.OK);
	}
	
	@RequestMapping("/data/stat/getStat/{statId}")
	public ResponseEntity<Stat> getStat(@PathVariable Long statId) {
		Stat stat = statService.getStat(statId);
		return new ResponseEntity<Stat>(stat, HttpStatus.OK);
	}
	
	@RequestMapping("/data/stat/getStatByKey/{keyFields}/{locationNumber}")
	public ResponseEntity<Stat> getStatByKey(@PathVariable Long keyFields, String locationNumber) {
		Stat stat = statService.getStatByKey(keyFields, locationNumber);
		return new ResponseEntity<Stat>(stat, HttpStatus.OK);
	}
	
	@RequestMapping("/data/stat/getAllStat/{locationNumber}")
	public ResponseEntity<List<StatVO>> getAllStats(@PathVariable Integer locationNumber){
		List<StatVO> statVOs = statService.getAllStats(locationNumber);
		return new ResponseEntity<List<StatVO>>(statVOs, HttpStatus.OK);
	}
	
}
