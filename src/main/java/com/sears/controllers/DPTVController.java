package com.sears.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Dptv;
import com.sears.services.DPTVService;

@Controller
public class DPTVController {

	@Autowired
	private DPTVService dPTVService;
	
	@RequestMapping(value = "/dptvpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("users", "Welcome User");
    	mv.setViewName("dptvpage");
    	return mv;
    }
	
	@RequestMapping("/data/dptv/getAllDptv/{locationNumber}/{departmentNumber}")
	public ResponseEntity<List<Dptv>> getAllDptvs(@PathVariable Integer locationNumber, @PathVariable Integer departmentNumber) {
		List<Dptv> dptvs  = dPTVService.getAllDPTV(locationNumber, departmentNumber);
		return new ResponseEntity<List<Dptv>>(dptvs, HttpStatus.OK);
	}
	
	
	@RequestMapping("/data/dptv/getDptv/{locationNumber}/{departmentNumber}/{openDate}")
	public ResponseEntity<Dptv> getDptv(@PathVariable String locationNumber, @PathVariable String departmentNumber, @PathVariable String openDate) {
		Dptv dptv = dPTVService.getDPTV(locationNumber, departmentNumber, openDate);
		return new ResponseEntity<Dptv>(dptv, HttpStatus.OK);
	}
	
	
	@RequestMapping("/data/dptv/getDptv/{dtpvId}")
	public ResponseEntity<Dptv> getDptv(@PathVariable Long dtpvId) {
		Dptv dptv = dPTVService.getDPTV(dtpvId);
		return new ResponseEntity<Dptv>(dptv, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/dptv/createDptv", method=RequestMethod.POST)
	public ResponseEntity<Dptv> createDPTV(@RequestBody Dptv dptv) {
		Dptv dptvCreated = dPTVService.createDPTV(dptv);
		return new ResponseEntity<Dptv>(dptvCreated, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/dptv/updateDptv", method=RequestMethod.POST)
	public ResponseEntity<Dptv> updateDPTV(@RequestBody Dptv dptv) {
		Dptv dptvUpdated = dPTVService.updateDPTV(dptv);
		return new ResponseEntity<Dptv>(dptvUpdated, HttpStatus.OK);
	}
	
}
