package com.sears.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.DPID;
import com.sears.services.DPIDService;

@Controller
public class DPIDController {

	@Autowired
	private DPIDService dPIDService; 
	
	@RequestMapping(value = "/dpidpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("dpidpage");
    	return mv;
    }
	
	@RequestMapping(value = "/data/dpid/getAllDepartments/{locationNumber}", method = RequestMethod.GET)
	public ResponseEntity<List<DPID>> getAllDepartments(@PathVariable Integer locationNumber){
		List<DPID> dpids= dPIDService.getAllDepartments(locationNumber);
    	return new ResponseEntity<List<DPID>>(dpids, HttpStatus.OK);
	}

	
	@RequestMapping("/data/dpid/getDepartment/{locationNumber}/{departmentNumber}")
	public ResponseEntity<DPID> getDepartment(@PathVariable String locationNumber, @PathVariable String departmentNumber) {
		DPID dpid = dPIDService.getDepartment(locationNumber, departmentNumber);
    	return new ResponseEntity<DPID>(dpid, HttpStatus.OK);
	}
	
	@RequestMapping("/data/dpid/getDepartment/{departmentId}")
	public ResponseEntity<DPID> getDepartment(@PathVariable Long departmentId) {
		DPID dpid=dPIDService.getDepartment(departmentId);
    	return new ResponseEntity<DPID>(dpid, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/dpid/createDpid", method=RequestMethod.POST)
	public ResponseEntity<DPID> createDpid(@RequestBody DPID dpid) {
		DPID dpidCreated = dPIDService.createDpid(dpid);
    	return new ResponseEntity<DPID>(dpidCreated, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/dpid/updateDpid", method=RequestMethod.POST)
	public ResponseEntity<DPID> updateDpid(@RequestBody DPID dpid) {
		DPID dpidUpdated = dPIDService.updateDpid(dpid);
    	return new ResponseEntity<DPID>(dpidUpdated, HttpStatus.OK);
	}
}
