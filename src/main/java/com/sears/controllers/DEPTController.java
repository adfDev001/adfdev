package com.sears.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.DeptDext;
import com.sears.services.DeptDextService;

@Controller
public class DEPTController {
	
	@Autowired
	private DeptDextService deptDextService;

	@RequestMapping(value = "/deptpage", method = RequestMethod.GET)
    public ModelAndView dashboard() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("deptpage");
    	return mv;
    }
	
	@RequestMapping("/data/deptdext/getDeptDext/{departmentNumber}")
	public ResponseEntity<DeptDext> getDeptDext(@PathVariable Integer departmentNumber) {
		DeptDext deptDext = deptDextService.getDeptDext(departmentNumber);
		return new ResponseEntity<DeptDext>(deptDext, HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/deptdext/updateDeptDext", method=RequestMethod.POST)
	public ResponseEntity<DeptDext> updateDeptDext(@RequestBody DeptDext deptDext) {
		DeptDext deptDextUpdated = deptDextService.updateDeptDext(deptDext);
		return new ResponseEntity<DeptDext>(deptDextUpdated, HttpStatus.OK);
	}
	
}
