package com.sears.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sears.model.Location;
import com.sears.model.LocationVO;
import com.sears.model.User;
import com.sears.services.LocationServices;

@Controller
public class LocationController {
	
	@Autowired
	private LocationServices locationServices;
	

	@RequestMapping(value = "/locationpage", method = RequestMethod.GET)
    public ModelAndView login(@AuthenticationPrincipal User user) {
    	ModelAndView mv = new ModelAndView();
    	System.out.println("Location Access - " + user.getLocationSegmentAccess());
    	mv.setViewName("locationpage");
    	return mv;
    }

	@RequestMapping(value = "/data/location/getAllLocations/{offset}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<List<LocationVO>> getAllLocatoina(@PathVariable Integer offset, @PathVariable Integer limit){
		List<LocationVO> locations= locationServices.getAllLocations(offset, limit); 
    	return new ResponseEntity<List<LocationVO>>(locations, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/location/getLocation/{locationNumber}", method = RequestMethod.GET)
	public ResponseEntity<Location> getAllLocatoina(@PathVariable Long locationNumber){
		Location location = locationServices.getLocation(locationNumber); 
    	return new ResponseEntity<Location>(location, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/location/createLocation", method = RequestMethod.POST) 
	public ResponseEntity<Location> createLocatoina(@RequestBody Location location){
		System.out.println("createLocatoina::::::::::::::::::::");
		Location locationCreated = locationServices.createLocation(location); 
    	return new ResponseEntity<Location>(locationCreated, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/data/location/updateLocation", method = RequestMethod.POST)
	public ResponseEntity<Location> updateLocatoina(@RequestBody Location location){
		Location locationUpdated = locationServices.updateLocation(location); 
    	return new ResponseEntity<Location>(locationUpdated, HttpStatus.OK);
	}
	
}
