package com.sears.config;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import com.sears.model.User;
import com.sears.services.AdminService;


@Component
public class CustomSuccessHandler implements AuthenticationSuccessHandler
{
	@Autowired
	private AdminService adminService;
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,HttpServletResponse response, Authentication authentication)	throws IOException, ServletException 
	{

		User user = new User();		
		
		
		String userName = authentication.getName().toString();
	//String password = authentication.getCredentials().toString();
		user = adminService.getUser(userName);
		/*user.setUserName("testlocation");
		user.setLocationSegmentAccess("FULL");
		user.setBankSegmentAccess("FULL");
		user.setDeptDextSegmentAccess("FULL");
		user.setDptvSegmentAccess("FULL");
		user.setStatSegmentAccess("FULL");
		user.setRealSegmentAccess("FULL");
		user.setKinsSegmentAccess("FULL");
		user.setAdminSegmentAccess("FULL");	*/	
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, AuthorityUtils.NO_AUTHORITIES));		
		redirectStrategy.sendRedirect(request, response, "/homepage");
		
	}	

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) 
	{
		this.redirectStrategy = redirectStrategy;
	}
	protected RedirectStrategy getRedirectStrategy() 
	{
		return redirectStrategy;
	}

}
