package com.sears.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import com.sears.model.User;
import com.sears.services.AdminService;

@Component
public class LocationUserAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private AdminService adminService;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String userName = auth.getPrincipal().toString();
		String password = auth.getCredentials().toString();
		User user = adminService.getUser(userName);
		return new UsernamePasswordAuthenticationToken(user, password, AuthorityUtils.NO_AUTHORITIES);
	}
	
	@Override
	public boolean supports(Class<?> arg0) {
		return arg0.equals(UsernamePasswordAuthenticationToken.class);
	}

}
