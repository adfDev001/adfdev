package com.sears.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import com.sears.model.User;

@Configuration
@EnableWebSecurity
public class LocationSecurityConfiguration extends WebSecurityConfigurerAdapter {

	/*@Autowired
	private LocationUserAuthenticationProvider locationUserAuthenticationProvider;
	
	@Autowired
	private AdminService adminService;

	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(locationUserAuthenticationProvider);
    }*/
	
	@Autowired
	private CustomSuccessHandler customSuccessHandler;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/login").usernameParameter("username")
		.passwordParameter("password").successHandler(customSuccessHandler);
		http.antMatcher("/**").authorizeRequests().antMatchers("/login", "/views/css/**", "/views/js/**").permitAll().anyRequest().authenticated().and().csrf().disable();
		
		}
	
	@Configuration
	protected static class AuthenticationConfig extends GlobalAuthenticationConfigurerAdapter
	{
		@Value("${ldap.userSearchFilter}")
		private String userSearchFilter;
		
		@Value("${ldap.userSearchBase}")
		private String userSearchBase;
		
		@Value("${ldap.userDnPatterns}")
		private String userDnPatterns;
		
		@Value("${ldap.url}")
		private String url;
		
		@Value("${ldap.base}")
		private String base;
		
		
		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception
		{
			auth.ldapAuthentication().userSearchFilter(userSearchFilter)
			.userSearchBase(userSearchBase)
			.userDnPatterns(userDnPatterns)
			.contextSource()
			.url(url+"/"+base)
			.and();
			//.ldapAuthoritiesPopulator(ldapAuthoritiesPopulator);
		}
	}
	
}
