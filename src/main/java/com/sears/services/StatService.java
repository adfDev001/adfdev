package com.sears.services;

import java.util.List;

import com.sears.model.Stat;
import com.sears.model.StatVO;

public interface StatService {

	Stat getStat(String locationNumber, Integer keyFields);
	Stat getStat(Long statId);
	Stat getStatByKey(Long keyFields, String locationNumber);
	List<StatVO> getAllStats(Integer offset, Integer limit);
	List<StatVO> getAllStats(Integer locationNumber);
	
}
