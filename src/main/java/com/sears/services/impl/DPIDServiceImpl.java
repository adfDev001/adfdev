package com.sears.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.DPID;
import com.sears.repository.DPIDRepository;
import com.sears.services.DPIDService;

@Service
public class DPIDServiceImpl implements DPIDService {
	
	@Autowired
	private DPIDRepository dpidRepository;

	@Override
	public DPID getDepartment(String locationNumber, String departmentNumber) {
		return dpidRepository.getDepartment(locationNumber, departmentNumber);
	}

	@Override
	public DPID getDepartment(Long dpid) {
		return dpidRepository.getDepartment(dpid);
	}

	@Override
	public List<DPID> getAllDepartments(Integer locationNumber) {
		return dpidRepository.getAllDepartments(locationNumber);
	}

	@Override
	public DPID createDpid(DPID dpid) {
		return dpidRepository.createDpid(dpid);
	}

	@Override
	public DPID updateDpid(DPID dpid) {
		return dpidRepository.updateDpid(dpid);
	}

}
