package com.sears.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Real;
import com.sears.repository.RealRepository;
import com.sears.services.RealService;

@Service
public class RealServiceImpl implements RealService{

	@Autowired
	private RealRepository realRepository;
	
	@Override
	public Real getReal(String locationNumber, String realId) {
		return realRepository.getReal(locationNumber, realId);
	}

	@Override
	public Real createReal(Real real) {
		return realRepository.createReal(real);
	}

	@Override
	public Real updateReal(Real real) {
		return realRepository.updateReal(real);
	}

}
