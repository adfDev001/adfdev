package com.sears.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Stat;
import com.sears.model.StatVO;
import com.sears.repository.StatRepository;
import com.sears.services.StatService;

@Service
public class StatServiceImpl implements StatService {

	@Autowired
	private StatRepository statRepository;
	
	@Override
	public Stat getStat(String locationNumber, Integer keyFields) {
		return statRepository.getStat(locationNumber, keyFields);
	}

	@Override
	public List<StatVO> getAllStats(Integer offset, Integer limit) {
		return statRepository.getAllStats(offset, limit);
	}

	@Override
	public Stat getStat(Long statId) {
		return statRepository.getStat(statId);
	}

	@Override
	public List<StatVO> getAllStats(Integer locationNumber) {
		return statRepository.getAllStats(locationNumber);
	}

	@Override
	public Stat getStatByKey(Long keyFields,  String locationNumber) {
		return statRepository.getStatByKey(keyFields, locationNumber);
	}

}
