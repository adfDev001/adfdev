package com.sears.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Kins;
import com.sears.repository.KinsRepository;
import com.sears.services.KinsService;

@Service
public class KinsServiceImpl implements KinsService{

	@Autowired
	private KinsRepository kinsRepository;
	
	@Override
	public Kins getKins(String locationNumber) {
		return kinsRepository.getKins(locationNumber);
	}

	@Override
	public Kins updateKins(Kins kins) {
		return kinsRepository.updateKins(kins);
	}

}
