package com.sears.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.User;
import com.sears.repository.AdminRepository;
import com.sears.services.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminRepository  adminRepository;
	
	@Override
	public User getUser(String userId) {
		System.out.println("User Id - " + userId);
		return adminRepository.getUser(userId);
	}

	@Override
	public List<User> getAllUsers(Integer offset, Integer limit) {
		return adminRepository.getAllUsers(offset, limit);
	}

	@Override
	public User createUser(User admin) {
		return adminRepository.createUser(admin);
	}

	@Override
	public User updateUser(User admin) {
		return adminRepository.updateUser(admin);
	}

	@Override
	public User deleteUser(User admin) {
		return adminRepository.deleteUser(admin);
	}

	@Override
	public User getUserById(Long userId) {
		return adminRepository.getUserById(userId);
	}

}
