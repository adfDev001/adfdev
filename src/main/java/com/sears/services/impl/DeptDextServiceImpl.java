package com.sears.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.DeptDext;
import com.sears.repository.DeptDextRepository;
import com.sears.services.DeptDextService;

@Service
public class DeptDextServiceImpl implements DeptDextService{

	@Autowired
	private DeptDextRepository deptDextRepository;
	
	
	@Override
	public DeptDext getDeptDext(Integer deparmentNumber) {
		return deptDextRepository.getDeptDext(deparmentNumber);
	}

	@Override
	public DeptDext updateDeptDext(DeptDext deptDext) {
		return deptDextRepository.updateDeptDext(deptDext);
	}

}
