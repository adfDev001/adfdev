package com.sears.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Bank;
import com.sears.repository.BankRepository;
import com.sears.services.BankService;

@Service
public class BankServiceImpl implements BankService{

	@Autowired
	private BankRepository bankRepository;
	
	@Override
	public Bank getBank(String locationNumber, Long bankId) {
		return bankRepository.getBank(locationNumber, bankId);
	}

	@Override
	public Bank updateBank(Bank bank) {
		return bankRepository.updateBank(bank);
	}

}
