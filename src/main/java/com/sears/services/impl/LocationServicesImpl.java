package com.sears.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Location;
import com.sears.model.LocationVO;
import com.sears.repository.LocationRepository;
import com.sears.services.LocationServices;

@Service
public class LocationServicesImpl implements LocationServices {
	
	@Autowired
	private LocationRepository locationRepository;

	@Override
	public List<LocationVO> getAllLocations(Integer offet, Integer limit) {
		return locationRepository.getAllLocations(offet, limit);
	}

	@Override
	public Location getLocation(Long locationNumber) {
		return locationRepository.getLocation(locationNumber);
	}

	@Override
	public Location createLocation(Location location) {
		return locationRepository.createLocation(location);
	}

	@Override
	public Location updateLocation(Location location) {
		return locationRepository.updateLocation(location);
	}

}
