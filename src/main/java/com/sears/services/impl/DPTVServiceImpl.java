package com.sears.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sears.model.Dptv;
import com.sears.repository.DPTVRepository;
import com.sears.services.DPTVService;

@Service
public class DPTVServiceImpl implements DPTVService{

	@Autowired
	private DPTVRepository dPTVRepository;
	
	@Override
	public Dptv getDPTV(String locationNumber, String departmentNumber, String openDate) {
		return dPTVRepository.getDPTV(locationNumber, departmentNumber, openDate);
	}

	@Override
	public Dptv getDPTV(Long dptvId) {
		return dPTVRepository.getDPTV(dptvId);
	}

	@Override
	public List<Dptv> getAllDPTV(Integer locationNumber, Integer departmentNumber) {
		return dPTVRepository.getAllDPTV(locationNumber, departmentNumber);
	}
	
	@Override
	public Dptv createDPTV(Dptv dptv) {
		return dPTVRepository.createDPTV(dptv);
	}

	@Override
	public Dptv updateDPTV(Dptv dptv) {
		return dPTVRepository.updateDPTV(dptv);
	}

}
