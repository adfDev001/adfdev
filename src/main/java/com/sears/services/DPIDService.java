package com.sears.services;

import java.util.List;

import com.sears.model.DPID;

public interface DPIDService {

	DPID getDepartment(String locationNumber, String departmentNumber);
	DPID getDepartment(Long dpid);
	List<DPID> getAllDepartments(Integer locationNumber);
	DPID createDpid(DPID dpid);
	DPID updateDpid(DPID dpid);
}
