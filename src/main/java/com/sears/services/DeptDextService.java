package com.sears.services;

import com.sears.model.DeptDext;

public interface DeptDextService {

	DeptDext getDeptDext(Integer deparmentNumber);
	DeptDext updateDeptDext(DeptDext deptDext);
	
}
