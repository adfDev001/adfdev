package com.sears.services;

import com.sears.model.Kins;

public interface KinsService {

	Kins getKins(String locationNumber);
	
	Kins updateKins(Kins kins);
	
}
