package com.sears.services;

import com.sears.model.Real;

public interface RealService {

	Real getReal(String locationNumber, String realId);
	
	Real createReal(Real real);
	
	Real updateReal(Real real);

}
