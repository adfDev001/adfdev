package com.sears.services;

import java.util.List;

import com.sears.model.Location;
import com.sears.model.LocationVO;

public interface LocationServices {

	List<LocationVO> getAllLocations(Integer offet, Integer limit);
	Location getLocation(Long locationNumber);
	Location createLocation(Location location);
	Location updateLocation(Location location);
}
