package com.sears.services;

import java.util.List;

import com.sears.model.User;

public interface AdminService {
	
	User getUser(String userId);
	User getUserById(Long userId);
	List<User> getAllUsers(Integer offset, Integer limit);
	User createUser(User admin);
	User updateUser(User admin);
	User deleteUser(User admin);

}
