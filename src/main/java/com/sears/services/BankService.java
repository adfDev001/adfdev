package com.sears.services;

import com.sears.model.Bank;

public interface BankService {

	Bank getBank(String locationNumber, Long bankId);
	
	Bank updateBank(Bank bank);
}
