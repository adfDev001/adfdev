package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.User;

public class AdminMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int row) throws SQLException {
		User user = new User();
		user.setUserId(rs.getLong("OCC_ID"));
		user.setUserName(rs.getString("USERID").trim());
		user.setLocationSegmentAccess(rs.getString("LOCNLEXTSEGMENTACCESS").trim());
		user.setDpidSegmentAccess(rs.getString("DPIDSEGMENTACCESS").trim());
		user.setDptvSegmentAccess(rs.getString("DPTVSEGMENTACCESS").trim());
		user.setDeptDextSegmentAccess(rs.getString("DEPTDEXTSEGMENTACCESS").trim());
		user.setStatSegmentAccess(rs.getString("STATSEGMENTACCESS").trim());
		user.setRealSegmentAccess(rs.getString("REALSEGMENTACCESS").trim());
		user.setBankSegmentAccess(rs.getString("BANKSEGMENTACCESS").trim());
		user.setKinsSegmentAccess(rs.getString("KINSSEGMENTACCESS").trim());
		user.setAdminSegmentAccess(rs.getString("ADMINACCESS").trim());
		user.setAction("Delete");
		return user;
	}

}
