package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Kins;

public class KinsMapper implements RowMapper<Kins>{

	@Override
	public Kins mapRow(ResultSet rs, int row) throws SQLException {
		Kins kins = new Kins();
		kins.setLocationId(rs.getLong("OCC_ID"));
		kins.setLocationNumber(rs.getString("LOCN_NUMBER"));
		kins.setTerminalCode(rs.getString("LOCN_TERMINAL_CODE"));
		kins.setTerminalDate(String.valueOf(rs.getInt("LOCN_TERMINAL_DATE")));
		return kins;
	}

}
