package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Stat;

public class StatMapper implements RowMapper<Stat> {

	@Override
	public Stat mapRow(ResultSet rs, int row) throws SQLException {
		Stat stat = new Stat();
		stat.setStatId(rs.getLong("OCC_ID"));
		stat.setLocationNumber(rs.getString("LOCN_NUMBER"));
		stat.setFiscalYear(rs.getInt("STAT_FISCAL_YEAR"));
		stat.setPeriod(rs.getInt("STAT_FISCAL_PERIOD"));
		stat.setStatusCode(rs.getString("STAT_STATUS_CODE1"));
		stat.setStatusCode1(rs.getString("STAT_STATUS_CODE2"));
		stat.setStatusCode2(rs.getString("STAT_STATUS_CODE3"));
		stat.setStatusCode3(rs.getString("STAT_STATUS_CODE4"));
		stat.setStatusCode4(rs.getString("STAT_STATUS_CODE5"));
		stat.setStatusCode5(rs.getString("STAT_STATUS_CODE6"));
		stat.setStatusCode6(rs.getString("STAT_STATUS_CODE7"));
		stat.setStatusCode7(rs.getString("STAT_STATUS_CODE8"));
		stat.setStatusCode8(rs.getString("STAT_STATUS_CODE9"));
		stat.setStatusCode9(rs.getString("STAT_STATUS_CODE10"));
		stat.setPreviousDayDate(rs.getInt("STAT_PREVIOUS_DAY_DATE"));
		stat.setPreviousBalance(rs.getBigDecimal("STAT_LINE19_PREV_BALANCE"));
		stat.setWeeklyUnBalanceSales(rs.getBigDecimal("STAT_UNBAL_SALES_CURR_WEEK"));
		stat.setWeeklyUnBalanceFunds(rs.getBigDecimal("STAT_UNBAL_REG_FUNDS_CUR_WEEK"));
		stat.setSpAdjStatusCode1(rs.getString("STAT_APAY_SP_ADJ_STAT_CODE1"));
		stat.setSpAdjStatusCode2(rs.getString("STAT_APAY_SP_ADJ_STAT_CODE2"));
		stat.setSpAdjStatusCode3(rs.getString("STAT_APAY_SP_ADJ_STAT_CODE3"));
		stat.setSpAdjStatusCode4(rs.getString("STAT_APAY_SP_ADJ_STAT_CODE4"));
		stat.setSpAdjStatusCode5(rs.getString("STAT_APAY_SP_ADJ_STAT_CODE5"));
		stat.setInvoiceTranDate(rs.getString("STAT_APAY_INVOICE_TRAN_DATE"));
		stat.setInvoiceTranDate605_606(rs.getInt("STAT_APAY_605606_TRAN_DATE"));
		stat.setUseTaxPurchase(rs.getBigDecimal("STAT_APAY_USE_TAX_PURCH"));
		stat.setUseTaxPaid(rs.getBigDecimal("STAT_APAY_TAX_PAID"));
		stat.setMerchForStoreUse(rs.getBigDecimal("STAT_APAY_MERCH_STORE_USE"));
		stat.setGaCompleteCodes1(rs.getString("STAT_GENA_COMPLETE_CODES1"));
		stat.setGaCompleteCodes2(rs.getString("STAT_GENA_COMPLETE_CODES2"));
		stat.setGaCompleteCodes3(rs.getString("STAT_GENA_COMPLETE_CODES3"));
		stat.setGaCompleteCodes4(rs.getString("STAT_GENA_COMPLETE_CODES4"));
		stat.setGaCompleteCodes5(rs.getString("STAT_GENA_COMPLETE_CODES5"));
		stat.setWeeklyCompletedFlag1(rs.getString("STAT_WEEK_COMPLETED_FLAG1"));
		stat.setWeeklyCompletedFlag2(rs.getString("STAT_WEEK_COMPLETED_FLAG2"));
		stat.setWeeklyCompletedFlag3(rs.getString("STAT_WEEK_COMPLETED_FLAG3"));
		stat.setWeeklyCompletedFlag4(rs.getString("STAT_WEEK_COMPLETED_FLAG4"));
		stat.setWeeklyCompletedFlag5(rs.getString("STAT_WEEK_COMPLETED_FLAG5"));
		stat.setWeeklyProcessFlag1(rs.getString("STAT_WEEK_PROCESS_FLAG1"));
		stat.setWeeklyProcessFlag2(rs.getString("STAT_WEEK_PROCESS_FLAG2"));
		stat.setWeeklyProcessFlag3(rs.getString("STAT_WEEK_PROCESS_FLAG3"));
		stat.setWeeklyProcessFlag4(rs.getString("STAT_WEEK_PROCESS_FLAG4"));
		stat.setWeeklyProcessFlag5(rs.getString("STAT_WEEK_PROCESS_FLAG5"));
		stat.setWeeklyForceFlag1(rs.getString("STAT_WEEK_FORCE_FLAG1"));
		stat.setWeeklyForceFlag1(rs.getString("STAT_WEEK_FORCE_FLAG2"));
		stat.setWeeklyForceFlag1(rs.getString("STAT_WEEK_FORCE_FLAG3"));
		stat.setWeeklyForceFlag1(rs.getString("STAT_WEEK_FORCE_FLAG4"));
		stat.setWeeklyForceFlag1(rs.getString("STAT_WEEK_FORCE_FLAG5"));
		stat.setKeyFields(stat.getFiscalYear().toString()+stat.getPeriod().toString());
		stat.setGaCompletedCodes(getFieldValue(stat.getGaCompleteCodes1())+getFieldValue(stat.getGaCompleteCodes2())+getFieldValue(stat.getGaCompleteCodes3())+getFieldValue(stat.getGaCompleteCodes4())+getFieldValue(stat.getGaCompleteCodes5()));
		stat.setWeeklyForceFlag(getFieldValue(stat.getWeeklyForceFlag1())+getFieldValue(stat.getWeeklyForceFlag2())+getFieldValue(stat.getWeeklyForceFlag3())+getFieldValue(stat.getWeeklyForceFlag4())+getFieldValue(stat.getWeeklyForceFlag5()));
		stat.setWeeklyProcessFlag(getFieldValue(stat.getWeeklyProcessFlag1())+getFieldValue(stat.getWeeklyProcessFlag2())+getFieldValue(stat.getWeeklyProcessFlag3())+getFieldValue(stat.getWeeklyProcessFlag4())+getFieldValue(stat.getWeeklyProcessFlag5()));
		stat.setWeeklyCompletedFlags(getFieldValue(stat.getWeeklyCompletedFlag1())+ getFieldValue(stat.getWeeklyCompletedFlag2())+ getFieldValue(stat.getWeeklyCompletedFlag3())+getFieldValue(stat.getWeeklyCompletedFlag4())+getFieldValue(stat.getWeeklyCompletedFlag5()));
		return stat;
	}
	
	private String getFieldValue(String fieldValue) {
		return fieldValue != null ? fieldValue : "";
	}

}
