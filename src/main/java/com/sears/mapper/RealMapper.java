package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Real;

public class RealMapper implements RowMapper<Real>{

	@Override
	public Real mapRow(ResultSet rs, int row) throws SQLException {
		Real real = new Real();
		real.setRealId(rs.getLong("OCC_ID"));
		real.setLeasedOwnedCode(rs.getString("REAL_LEASED_OWNED_CODE"));
		real.setOriginalLeasedDate(rs.getInt("REAL_ORIGINAL_LEASE_DATE"));
		real.setFirstLeaseRenewalDate(rs.getInt("REAL_LEASE_RENEWAL1"));
		real.setSecondLeaseRenewalDate(rs.getInt("REAL_LEASE_RENEWAL2"));
		real.setThirdLeaseRenewalDate(rs.getInt("REAL_LEASE_RENEWAL3"));
		return real;
	}

}
