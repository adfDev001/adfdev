package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Location;

public class LocationMapper implements RowMapper<Location>{

	@Override
	public Location mapRow(ResultSet rs, int row) throws SQLException {
		Location location = new Location();
		location.setLocationId(rs.getLong("LOCN_ID"));
		location.setLocationLextId(rs.getLong("LEXT_ID"));
		location.setLocationNumber(rs.getLong("LOCN_NUMBER"));
		location.setStreetAddress(rs.getString("LOCN_STREET_ADDRESS"));
		location.setCity(rs.getString("LOCN_CITY"));
		location.setState(rs.getString("LOCN_STATE_ABBREV_CODE"));
		location.setZipCode(String.valueOf(rs.getInt("LOCN_ZIPCODE")));
		location.setZipCodeExtension(String.valueOf(rs.getInt("LOCN_ZIP_EXPANSION")));
		location.setCounty(rs.getString("LOCN_COUNTY"));
		location.setStorePhone(rs.getString("LOCN_PHONE_NUMBER"));
		location.setShoppingCenter(rs.getString("LOCN_SHOP_CNTR_NAME"));
		location.setShippingAddressFlag(rs.getString("LOCN_SPECIAL_SHIPPING_ADDRESS"));
		location.setOverHeadName(rs.getString("LOCN_OVERHEAD_NAME"));
		location.setDistrictLevel1(String.valueOf(rs.getInt("LOCN_HIERARCHY_LEVEL1")));
		location.setDistrictLevel2(String.valueOf(rs.getInt("LOCN_HIERARCHY_LEVEL2")));
		location.setSummaryLevel3(String.valueOf(rs.getInt("LOCN_HIERARCHY_LEVEL3")));
		location.setManagerName(rs.getString("LOCN_MANAGER_NAME"));
		location.setRegionalManager(String.valueOf(rs.getInt("LOCN_REGIONAL_MANAGER")));
		location.setAreaMerchCoord(rs.getString("LEXT_AREA_MERCH_COORDINATOR"));
		location.setNaiNumber((String.valueOf(rs.getString("LOCN_ORIG_NAI_NUMBER"))));
		location.setOriginalNumber(rs.getString("LOCN_ORIG_NBR"));
		location.setSearUnitNumber(String.valueOf(rs.getInt("LOCN_SEARS_UNIT_NBR")));
		location.setOriginalKmartNumber(rs.getString("LOCN_ORIG_KMART_NBR"));
		location.setOriginalFacility(String.valueOf(rs.getString("LOCN_ORIG_FACILITY_NBR")));
		location.setRptingLocationNumber(String.valueOf(rs.getLong("LOCN_REPORTING_STORE")));
		location.setBtftLocationNumber(String.valueOf(rs.getInt("LOCN_BT_FT_STORE")));
		location.setRecievingStoreNumber(String.valueOf(rs.getInt("LEXT_RECEIVING_STORE")));
		location.setDevisionCode(String.valueOf(rs.getString("LOCN_DIVISION_CODE")));
		location.setTypeCode(String.valueOf(rs.getString("LOCN_TYPE_CODE")));
		location.setRegionCode(String.valueOf(rs.getString("LOCN_REGION_CODE")));
		location.setFormatType(rs.getString("LOCN_FORMAT_TYPE"));
		location.setFormatSubType(rs.getString("LOCN_FORMAT_SUB_TYPE"));
		location.setFormatModifier(rs.getString("LOCN_FORMAT_MOD_CD"));
		location.setCompanyCode(rs.getString("LOCN_COMPANY_CODE"));
		location.setClimaticZone(rs.getString("LOCN_CLIMATIC_ZONE"));
		location.setMdseAreaRotoZone(rs.getString("LOCN_CURRENT_MDSE_ZONE"));
		location.setUpsZone(rs.getString("LOCN_UPS_ZONE"));
		location.setShippingStreetAddress(rs.getString("LEXT_SHIPPING_STREET_ADDRESS"));
		location.setShipCity(rs.getString("LEXT_SHIPPING_CITY"));
		location.setShipState(rs.getString("LEXT_SHIPPING_STATE_ABBREV_CD"));
		location.setShipZipCode(rs.getString("LEXT_ZIPCODE"));
		location.setShipZipCodeExtension(rs.getString("LEXT_ZIP_EXPANSION"));
		location.setFaxNumber(rs.getString("LEXT_FAX_PHONE"));
		location.setTimeZone(rs.getString("LOCN_TIME_ZONE"));
		location.setLangitude(rs.getString("LEXT_LONGITUDE"));
		location.setLatitude(rs.getString("LEXT_LATITUDE"));
		location.setStateNumber(rs.getString("LOCN_STATE_NO"));
		location.setCityNumber(rs.getString("LOCN_CITY_NO"));
		location.setCountyNumber(rs.getString("LOCN_COUNTY_NO"));
		location.setPlanedOpenDate(rs.getString("LOCN_EFFECTIVE_DATE"));
		location.setActualOpenDate(rs.getString("LOCN_OPEN_DATE"));
		location.setCloseDate(rs.getString("LOCN_CLOSE_DATE"));
		location.setTemporaryCloseDate(rs.getString("LOCN_TEMP_CLOSE_DATE"));
		location.setReOpenDate(rs.getString("LOCN_REOPEN_DATE"));
		location.setPlannedCloseDate(rs.getString("LEXT_PLANNED_CLOSED_DATE"));
		location.setHolidayCloseDate1(rs.getString("LOCN_HOLIDAY_CLOSED_DATE1"));
		location.setHolidayCloseDate2(rs.getString("LOCN_HOLIDAY_CLOSED_DATE2"));
		location.setHolidayCloseDate3(rs.getString("LOCN_HOLIDAY_CLOSED_DATE3"));
		location.setHolidayCloseDate4(rs.getString("LOCN_HOLIDAY_CLOSED_DATE4"));
		location.setHolidayCloseDate5(rs.getString("LOCN_HOLIDAY_CLOSED_DATE5"));
		location.setHolidayCloseDate6(rs.getString("LOCN_HOLIDAY_CLOSED_DATE6"));
		location.setHolidayCloseDate7(rs.getString("LOCN_HOLIDAY_CLOSED_DATE7"));
		location.setHolidayCloseDate8(rs.getString("LOCN_HOLIDAY_CLOSED_DATE8"));
		location.setHolidayCloseDate9(rs.getString("LOCN_HOLIDAY_CLOSED_DATE9"));
		location.setHolidayCloseDate10(rs.getString("LOCN_HOLIDAY_CLOSED_DATE10"));
		location.setTotalSQFootage(rs.getString("LOCN_TOTAL_SQ_FOOTAGE"));
		location.setSellingAreaSqFoot(rs.getString("LOCN_SELLING_AREA_SQ_FOOTAGE"));
		location.setTerminalCode(rs.getString("LOCN_TERMINAL_CODE"));
		location.setTerminalDate(rs.getString("LOCN_TERMINAL_DATE"));
		return location;
	}

}
