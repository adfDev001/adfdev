package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.LocationVO;

public class LocationGridDataMapper implements RowMapper<LocationVO>{

	@Override
	public LocationVO mapRow(ResultSet rs, int row) throws SQLException {
		LocationVO locationVO = new LocationVO();
		locationVO.setOccId(rs.getLong("OCC_ID"));
		locationVO.setLocationNumber(rs.getLong("LOCN_NUMBER"));
		locationVO.setPlannedOpenDate(rs.getLong("LOCN_OPEN_DATE")); // need to check the correct field.
		locationVO.setCity(rs.getString("LOCN_CITY"));
		locationVO.setState(rs.getString("LOCN_STATE_ABBREV_CODE"));
		locationVO.setCloseDate(rs.getInt("LOCN_CLOSE_DATE"));
		locationVO.setOriginalFacility(rs.getInt("LOCN_ORIG_FACILITY_NBR"));
		locationVO.setFormatType(rs.getInt("LOCN_FORMAT_TYPE"));
		locationVO.setSubType(rs.getString("LOCN_FORMAT_SUB_TYPE"));
		locationVO.setTimeZone(rs.getString("LOCN_TIME_ZONE"));
		return locationVO;
	}

}
