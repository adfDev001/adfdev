package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.DPID;

public class DPIDMapper implements RowMapper<DPID> {

	@Override
	public DPID mapRow(ResultSet rs, int row) throws SQLException {
		DPID dpid = new DPID();
		dpid.setLocationId(rs.getString("LOC_ID"));
		dpid.setLocationNumber(rs.getString("LOCN_NUMBER"));
		dpid.setDepartmentId(rs.getLong("DPID"));
		dpid.setDepartmentNumber(rs.getString("DPID_SP_DEPARTMENT_NUMBER"));
		dpid.setDepartmentType(rs.getInt("DPID_DEPT_TYPE"));
		dpid.setDepartmentOpenDate(rs.getString("DPID_OPEN_DATE"));
		dpid.setDepartmentCloseDate(rs.getString("DPID_CLOSED_DATE"));
		dpid.setSpecialDistrictMGRCode(rs.getString("DPID_SPECIAL_DIST_MGR_CODE"));
		dpid.setSpecialRegionManager(rs.getString("DPID_SPECIAL_RGN_MGR"));
		dpid.setDeptInSquareFeet(rs.getString("DPID_DEPT_IN_SQ_FT"));
		return dpid;
	}

}
