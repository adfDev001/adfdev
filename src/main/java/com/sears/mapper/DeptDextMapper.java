package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.DeptDext;

public class DeptDextMapper  implements RowMapper<DeptDext>{

	@Override
	public DeptDext mapRow(ResultSet rs, int row) throws SQLException {
		DeptDext deptDext = new DeptDext();
		deptDext.setDeptId(rs.getLong("DEPTID"));
		deptDext.setDextId(rs.getLong("DEXTID"));
		deptDext.setDepartmentProductCode(rs.getString("DEPT_PRODUCT_CODE"));
		deptDext.setDepartmentName(rs.getString("DEPT_DEPARTMENT_NAME"));
		deptDext.setDateDepartmentAdded(rs.getString("DEPT_DATE_ADDED_TO_DEPT"));
		deptDext.setDepartmentPhaseOutDate(rs.getString("DEPT_PHASE_OUT_DATE"));
		deptDext.setTermEntryLevel(rs.getString("DEPT_TERM_ENTRY_LVL"));
		deptDext.setShcReportLevel(rs.getString("DEPT_KIH_RPT_LVL"));
		deptDext.setCorpSummLevel(rs.getString("DEPT_CORP_SUMM_LVL"));
		deptDext.setSalesTermEntryLevel(rs.getString("DEPT_SALES_TERM_ENTRY_LVL"));
		deptDext.setSalesShcReportLevel(rs.getString("DEPT_SALES_KIH_RPT_LVL"));
		deptDext.setSalesCorpSummLevel(rs.getString("DEPT_SALES_CORP_SUMM_LVL"));
		deptDext.setStoreUseCode(rs.getString("DEPT_VAL_STORE_USE_CODE"));
		deptDext.setShcOperCode(rs.getString("DEPT_VAL_SSK_OPER_CODE"));
		deptDext.setOtherOperCode(rs.getString("DEPT_VAL_OTHER_OPER_CODE"));
		deptDext.setAssocOperCode(rs.getString("DEPT_VAL_ASSOC_OPER_CODE"));
		deptDext.setSpecialityCode(rs.getString("DEPT_VAL_SPECIALTY_CODE"));
		deptDext.setCashSalesCode(rs.getString("DEPT_VAL_CASH_SALES_CODE"));
		deptDext.setPayrollCode(rs.getString("DEPT_VAL_PAYROLL_CODE"));
		deptDext.setRetailCode(rs.getString("DEPT_RETAIL_CODE"));
		deptDext.setOwnerCode(rs.getString("DEPT_OWNER_CODE"));
		deptDext.setGroupCode(rs.getString("DEXT_GROUP_CODE"));
		deptDext.setOverHeadCode(rs.getString("DEPT_VAL_OVERHEAD_CODE"));
		deptDext.setInventoriedDepartment(rs.getString("DEPT_INVENTORIED_CODE"));
		deptDext.setAccountPayableCode(rs.getString("DEPT_VAL_AP_CODE"));
		deptDext.setReportingDept(rs.getString("DEXT_REPORTING_DEPT"));
		deptDext.setCostOnlyDept(rs.getString("DEPT_COST_ONLY_CODE"));
		deptDext.setAccountRequired(rs.getString("DEPT_ACCT_REQUIRED_CODE"));
		deptDext.setAssociateDiscount(rs.getString("DEPT_VAL_EMPLOYEE_DISCOUNT"));
		deptDext.setFoodStampsEbt(rs.getString("DEPT_VAL_FOOD_STAMPS"));
		deptDext.setPurchaseOrderCode(rs.getString("DEPT_VAL_PUR_ORDER_CODE"));
		return deptDext;
	}

}
