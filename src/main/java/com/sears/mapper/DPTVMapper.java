package com.sears.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Dptv;

public class DPTVMapper implements RowMapper<Dptv>{

	@Override
	public Dptv mapRow(ResultSet rs, int row) throws SQLException {
		Dptv dptv = new Dptv();
		dptv.setLocationId(rs.getLong("LOC_ID"));
		dptv.setLocationNumber(rs.getString("LOCN_NUMBER"));
		dptv.setDepartmentNumber(rs.getString("DPID_SP_DEPARTMENT_NUMBER"));
		dptv.setOpenDate(rs.getString("DPTV_VENDOR_OPEN_DATE"));
		dptv.setDpid(rs.getLong("DPID"));
		dptv.setDptvId(rs.getLong("DPTVID"));
		dptv.setVendorDepartment(Integer.valueOf(rs.getString("DPID_SP_DEPARTMENT_NUMBER")));
		dptv.setChangeIndicator(rs.getString("DPTV_CHANGE_INDICATOR"));
		dptv.setVendorOpenDate(rs.getInt("DPTV_VENDOR_OPEN_DATE"));
		dptv.setVendorCloseDate(rs.getInt("DPTV_VENDOR_CLOSE_DATE"));
		dptv.setVendorDunsNumber(rs.getLong("DPTV_VENDOR_DUNS_NUMBER"));
		dptv.setVendorAccountNumber(rs.getLong("DPTV_VENDOR_ACCT_NUMBER"));
		dptv.setCombinedFeePercent(new BigDecimal(String.valueOf(rs.getString("DPTV_COMBINED_FEE_PERCNT"))));
		dptv.setSplitFeePercent(new BigDecimal(String.valueOf(rs.getString("DPTV_SPLIT_FEE_PERCENT"))));
		dptv.setPercentageOwnedByKMART(new BigDecimal(String.valueOf(rs.getString("DPTV_PERCENT_OWNED_BY_KMART"))));
		dptv.setAdvertisingFeePercentage(new BigDecimal(String.valueOf(rs.getString("DPTV_ADVERTNG_FEE_PERCNT"))));
		dptv.setWeeklyFloridaTax(new BigDecimal(String.valueOf(rs.getString("DPTV_WEEKLY_FLORIDA_TAX"))));
		return dptv;
	}

}
