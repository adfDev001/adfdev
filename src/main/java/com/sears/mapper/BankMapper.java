package com.sears.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sears.model.Bank;

public class BankMapper implements RowMapper<Bank> {

	@Override
	public Bank mapRow(ResultSet rs, int row) throws SQLException {
		Bank bank = new Bank();
		bank.setBankId(rs.getLong("OCC_ID"));
		bank.setLocationNumber(rs.getString("LOCN_NUMBER"));
		bank.setMultibankStoreNumber(rs.getLong("BANK_MULTI_BANK_STORE_NO"));
		return bank;
	}

}
